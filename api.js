(function() {
  
  function _request(key,ajax_args) {
    return {
      key: key,
      when: this.cache_map.hasOwnProperty(key)
        ? this.cache_map[key].status!='ajax'
          ? $.Deferred().resolve(this.cache_map[key].value)
          : this.cache_map[key].defer
        : (this.cache_map[key]={status: 'ajax', defer: $.Deferred()},
            
            $.ajax(ajax_args)
              .done((function() {
                this.cache_map[key].value=arguments[0];
                this.cache_map[key].status='resolved';
                this.cache_map[key].defer.resolve(this.cache_map[key].value);
                delete this.cache_map[key].defer;
              }).bind(this))
              .fail((function() {
                this.cache_map[key].defer.reject();
                delete this.cache_map[key];
              }).bind(this))
            
          , this.cache_map[key]).defer
    };
  }
  
  function ApiManager() {
    this.cache_map={};
  }
  
  ApiManager.prototype.requestSchemaTree=(function() {
    return $.ajax({
      type: 'GET',
      url: 'api/schema/tree',
      dataType: 'json',
      cache: true
    });
  });
  
  ApiManager.prototype.requestSchema=(function(schema_id) {
    var rq=_request.call(this,'schema-'+schema_id,{
      type: 'GET',
      url: 'api/schema/'+schema_id,
      dataType: 'json',
      cache: true
    });
    rq.when.done(function(schema) {
      if (!db.struct_inf.hasOwnProperty(schema_id)) {
        db.struct_inf[schema_id]=schema;
        $.extend(db.param_inf,schema.param_inf);
        delete schema.param_inf;
        
        // project-dom.js
        db.struct_inf[schema_id].kit_map={};
        for (var i=0; i<db.struct_inf[schema_id].kit_functions.length; i++) {
          db.struct_inf[schema_id].kit_map[db.struct_inf[schema_id].kit_functions[i].id]={affecting_hnd: 1, name: db.struct_inf[schema_id].kit_functions[i].name};
        }
        
      }
    })
    return rq;
  });
  
  var hits_id_autoinc=0;
  ApiManager.prototype.requestHits=(function(schema_id,condition_expr) {
    return _request.call(this,'hits-of['+schema_id+']#'+(hits_id_autoinc++),{
      type: 'POST',
      url: 'api/schema/'+schema_id+'/query',
      dataType: 'json',
      cache: true,
      data: condition_expr ? condition_expr : undefined,
      headers: {'Content-Type': 'application/json'}
    });
  });
  
  ApiManager.prototype.fromCache=(function(key) {
    return this.cache_map.hasOwnProperty(key) ? this.cache_map[key].value : undefined;
  });
  
  window.db={struct_inf: {}, param_inf: {}};
  window.mnApi=new ApiManager();
  
  // model instancing functions
  
  function parseRawProduct(raw_data) {
    var schema_id=raw_data.id.slice(0,40);
    
    var props=$.map(Object.keys(raw_data.props_raw),function(prop_id) {
      var PProp_crobj=raw_data.props_raw[prop_id];
      return new PProp(prop_id,schema_id,PProp_crobj.value,undefined,undefined,undefined,undefined,undefined,PProp_crobj.concat_pattern,undefined);
    });
    
    var instrs=$.map(raw_data.instrs_raw,function(PInstr_crobj) {
      return new PInstr(PInstr_crobj.idx,PInstr_crobj.instr);
    });
    
    var param_override_map=new Object();
    $.each(Object.keys(raw_data.param_override_raw),function(index,param_hash) {
      param_override_map[param_hash]=$.map(raw_data.param_override_raw[param_hash],function(ParamOverride_crobj) {
        return new ParamOverride2(
          ParamOverride_crobj.type=='prop' ? $.extend({schema_id: schema_id},ParamOverride_crobj) :
          ParamOverride_crobj.type=='instr' ? $.extend({id: ParamOverride_crobj.idx, def_visible: true, target_visible: true},ParamOverride_crobj) :
          ParamOverride_crobj
        );
      });
    });
    
    var p=new PDContainer(
      raw_data.id,
      schema_id,
      undefined,
      raw_data.label,
      [raw_data.desc],
      null,
      props,
      [],
      instrs,
      undefined,
      raw_data.link,
      raw_data.doc_href,
      raw_data.kit,
      {schema_ext: {}, value_map: {}},
      param_override_map,
      'minedit-static'
    );
    
    if (p.isKit()) {
      p._kf=raw_data.kf_id;
      
      var refp_pool_keys=Object.keys(raw_data.refp_pool_raw);
      if (refp_pool_keys.length>0) {
        for (var i=0; i<refp_pool_keys.length; i++) {
          var RefP_crobj=raw_data.refp_pool_raw[refp_pool_keys[i]];
          p._refp_pool[RefP_crobj.id]=RefP_crobj.hasOwnProperty('found_in')
            ? RefP_crobj
            : parseRawProduct(RefP_crobj);
        }
      }
      
    }
    
    return p;
  }
  
  function resolvePReferences(hits) {
    hits.all(function(p) {
      $.each(p._refp_pool,function(refp_id,refp) {
        if (!(refp instanceof PDContainer)) {
          p._refp_pool[refp_id]=hits.get(refp.found_in)._refp_pool[refp.id];
        }
      });
    });
  }
  
  window.mnParseHitsArray=(function(raw_data_arr) {
    this.clear();
    
    for (var i=0; i<raw_data_arr.length; i++) {
      this.append(parseRawProduct(raw_data_arr[i]));
    }
    resolvePReferences(this);
    
  });
  
})();
