function deep_copy(obj) {
  return owl.deepCopy(obj);
}

var stack=new DList();

function has_stack(p) {
  var parse=p.parseId();
  return stack.has(parse.schema+'.'+parse.db_id);
}

function get_stack(p) {
  var parse=p.parseId();
  if (!has_stack(p)) {
    stack.append({getId: function() {return parse.schema+'.'+parse.db_id}});
  }
  return stack.get(parse.schema+'.'+parse.db_id);
}

function clear_stack() {
  stack.clear();
}

var SORT_INDEX_AUTO_APPEND=3435973836;

// -- class abbr: str

// -- class schema: int

// -- class PProp (implements DList.T)

// PProp(int prop_id, int schema_id, str value, str unit, str pvalue, str punit, str nvalue, str nunit, str concat_pattern, str std, optional bool custom=false)
function PProp(prop_id,schema_id,value,unit,pvalue,punit,nvalue,nunit,concat_pattern,std,custom) {
  this.prop_id=prop_id+'';
  this.schema_id=schema_id;
  this.value=value || '';
  this.unit=unit || '';
  this.pvalue=pvalue || '';
  this.punit=punit || '';
  this.nvalue=nvalue || '';
  this.nunit=nunit || '';
  this.concat_pattern=concat_pattern || '[v]';
  this.std=std || '';
  this.custom=custom || false;
}

// @interface(DList.T)
// K PProp.getId()
PProp.prototype.getId=function() {
  return this.prop_id;
}

// abbr PProp.getPropId()
PProp.prototype.getPropId=PProp.prototype.getId;

// schema PProp.getSchemaId()
PProp.prototype.getSchemaId=function() {
  return this.schema_id;
}

// str PProp.toString()
PProp.prototype.toString=function() {
  return this.concat_pattern
    .replace(/\[v\]/g   ,   this.value)
    .replace(/\[u\]/g   ,   this.unit)
    .replace(/\[pv\]/g  ,   this.pvalue)
    .replace(/\[pu\]/g  ,   this.punit)
    .replace(/\[nv\]/g  ,   this.nvalue)
    .replace(/\[nu\]/g  ,   this.nunit);
}

// str PProp.getStd()
PProp.prototype.getStd=function() {
  return this.std;
}

// bool PProp.equals(PProp prop)
PProp.prototype.equals=function(prop) {
  return (
    this.prop_id==prop.prop_id &&
    this.schema_id==prop.schema_id &&
    this.value==prop.value &&
    this.unit==prop.unit &&
    this.pvalue==prop.pvalue &&
    this.punit==prop.punit &&
    this.nvalue==prop.nvalue &&
    this.nunit==prop.nunit &&
    this.concat_pattern==prop.concat_pattern &&
    this.std==prop.std
  );
}

// bool PProp.isCustom()
PProp.prototype.isCustom=function() {
  return this.custom;
}

// -- class EmptyPProp : PProp

// EmptyPProp(int prop_id, int schema_id)
function EmptyPProp(prop_id,schema_id) {
  PProp.call(this,prop_id,schema_id,null,null,null,null,null,null,null,null);
}

EmptyPProp.prototype=Object.create(PProp.prototype);
EmptyPProp.prototype.constructor=EmptyPProp;

// -- class PExt (implements DList.T)

// PExt(int id, str label, str desc, str instr, int tag)
function PExt(id,label,desc,instr,tag) {
  this.id=id;
  this.label=label;
  this.desc=desc;
  this.instr=instr;
  this.tag=tag;
  this.alt=false;
}

// @interface(DList.T)
// K PExt.getId()
//
// int PExt.getId()
PExt.prototype.getId=function() {
  return this.id;
}

// str PExt.toString()
PExt.prototype.toString=function() {
  return this.tag!=-1 ? (this.alt ? this.desc : this.label+(this.tag==0 ? ' '+this.desc : ''))+(this.instr!='' ? ' '+this.instr : '') : this.instr;
}

// bool PExt.equals(PExt ext)
PExt.prototype.equals=function(ext) {
  return (
    this.id==ext.id &&
    this.label==ext.label &&
    this.desc==ext.desc &&
    this.instr==ext.instr &&
    this.tag==ext.tag &&
    this.alt==ext.alt
  );
}

// -- class PInstr (implements DList.T)

// PInstr(id, str instr)
function PInstr(id,instr) {
  this.id=id;
  this.instr=instr;
}

// @interface(DList.T)
// K PInstr.getId()
//
// int PInstr.getId()
PInstr.prototype.getId=function() {
  return this.id;
}

// str PInstr.toString()
PInstr.prototype.toString=function() {
  return this.instr;
}

// bool PInstr.equals(PInstr instr)
PInstr.prototype.equals=function(instr) {
  return (
    this.id==instr.id &&
    this.instr==instr.instr
  );
}

// -- class ParamStruct

function ParamStruct() {}

// ParamStruct_sha1 ParamStruct.toString(P* for_p)
ParamStruct.prototype.toString=function(for_p) {
  var _this=this;
  var for_schema_p=for_p.parseId().schema+'.'+for_p.parseId().db_id;
  return CryptoJS.SHA1($.map($.grep(Object.keys(this),function(key) {
    return !(ParamStruct.prototype.hasOwnProperty(key) || _this[key]==undefined)
  }).sort(function(a,b) {
    return db.param_inf[for_schema_p].params_order.indexOf(a)-db.param_inf[for_schema_p].params_order.indexOf(b);
  }),function(param_id) {
    return param_id+'='+_this[param_id];
  }).join(';')).toString();
}

// str ParamStruct.toParamStr(P* for_p)
ParamStruct.prototype.toParamStr=function(for_p) {
  var _this=this;
  var for_schema_p=for_p.parseId().schema+'.'+for_p.parseId().db_id;
  var param_str=$.map($.grep(Object.keys(this),function(key) {
    return !(ParamStruct.prototype.hasOwnProperty(key) || _this[key]==undefined);
  }).sort(function(a,b) {
    return db.param_inf[for_schema_p].params_order.indexOf(a)-db.param_inf[for_schema_p].params_order.indexOf(b);
  }),function(param_id) {
    var param_rec=db.param_inf[for_schema_p].params[param_id];
    return param_rec.str_format.replace(/\[v\]/g,(function(paramopt_rec) {
      return paramopt_rec[paramopt_rec.hasOwnProperty('value_in_param_str') ? 'value_in_param_str' : 'value'];
    })($.grep(param_rec.options,function(paramopt_rec) {
      return paramopt_rec.paramopt_id==_this[param_id];
    })[0]));
  }).join(', ');
  return param_str ? param_str : undefined;
}

// static arr<arr<int>> ParamStruct.getCombinations()
ParamStruct.getCombinations=function(n_arr,k) {
  if (k==0) {
    return new Array(new Array());
  } else if (k>0 && k<=n_arr.length) {
    var ret=[];
    var recur_combinations=ParamStruct.getCombinations(n_arr,k-1);
    for (var recur_i in recur_combinations) {
      var recur=recur_combinations[recur_i];
      var recur_last_index=recur.length>0 ? n_arr.indexOf(recur[recur.length-1]) : -1;
      for (var add_i=recur_last_index+1; add_i<n_arr.length; add_i++) {
        ret.push(recur.concat([n_arr[add_i]]));
      }
    }
    return ret;
  } else {
    throw 'Invalid k param for ParamStruct.getCombinations()';
  }
}

// -- class ParamStruct_sha1: str

// -- class ParamOverride

// ParamOverride(str target, str ovalue, int otype)
function ParamOverride(target,ovalue,otype) {
  var key_arr=new Array();
  $.each(target.split('.'),function(i,target_part) {
    key_arr.push(target_part);
  });
  this.key_arr=key_arr;
  this.type=otype;
  this.value=
    (otype=='NUMERIC' ? Number(ovalue) :
    //(otype=='STRING' ? ovalue :
    (otype=='BOOLEAN' ? ovalue=='true' :
    (otype=='OBJECT_PROTOTYPE' ? Object.create(window[ovalue].prototype) : 
    (otype=='UNDEFINED' || otype=='OPERATION_REMOVE' || otype=='INSERT_INTO_ARRAY_AT_INDEX' ? undefined :
    ovalue))));
}

// void ParamOverride.apply(P* p)
ParamOverride.prototype.apply=function(p) {
  var key=p;
  $.each(this.key_arr.slice(0,-1),function(i,key_part) {
    key=key[key_part];
  });
  if (this.type=='OPERATION_REMOVE') {
    if (key instanceof Array) {
      key.splice(this.key_arr.slice(-1)[0],1);
    } else {
      delete key[this.key_arr.slice(-1)[0]];
    }
  } else if (this.type=='INSERT_INTO_ARRAY_AT_INDEX') {
    key.splice(this.key_arr.slice(-1)[0],0,this.value);
  } else {
    key[this.key_arr.slice(-1)[0]]=this.value;
  }
}

// bool ParamOverride.keyEquals(ParamOverride param_override)
ParamOverride.prototype.keyEquals=function(param_override) {
  return param_override instanceof ParamOverride && this.key_arr.join('.')==param_override.key_arr.join('.');
}

// static void ParamOverride.blockOverwrite(arr<ParamOverride*> overrider, arr<ParanOverride*> to_override)
ParamOverride.blockOverwrite=function(overrider,to_override) {
  $.each(overrider,function(overrider_index,overrider_p) {
    var overriden=false;
    $.each(to_override,function(to_override_index,to_override_p) {
      if (overrider_p.keyEquals(to_override_p)) {
        to_override[to_override_index]=overrider_p;
        overriden=true;
        return false;
      }
    });
    if (!overriden) {
      to_override.push(overrider_p);
    }
  });
}

// -- class ParamOverride2

// ParamOverride2([...])
function ParamOverride2(data) {
  this.operation=data.operation;
  switch (data.type) {
  case 'prop':
    this.item=new PProp(data.prop_id,data.schema_id,data.value,data.unit,data.pvalue,data.punit,data.nvalue,data.nunit,data.concat_pattern,data.std);
    break;
  case 'ext':
    this.item=new PExt(data.id,data.label,data.desc,data.instr,data.tag);
    break;
  case 'instr':
    this.item=new PInstr(data.id,data.instr);
    break;
  case 'kit':
    this.item={f: data.function, refp_id: data.refp_id};
    break;
  case 'add_fvalue':
    this.item={f: data.field_id, value: data.value};
    break;
  }
  if (data.type=='ext' || data.type=='instr') {
    this.target_visible=data.target_visible;
    this.def_visible=this.operation=='REPLACE_TEXT' ? this.target_visible : data.def_visible; // (conditional) REPLACE_TEXT data overwrite
  }
}

// bool ParamOverride2.ei__need_delayed_apply()
ParamOverride2.prototype.ei__need_delayed_apply=function() {
  if (this.hasOwnProperty('_old')) {
    return false;
  }
  return !this.def_visible;
}

// void ParamOverride2.ei__pre_delayed_apply(P* p)
ParamOverride2.prototype.ei__pre_delayed_apply=function(p) {
  if (this.target_visible && !this.def_visible) {
    var target_dlist=this.item instanceof PExt ? p.exts : p.instrs;
    var key=this.item.getId();
    target_dlist.remove(key);
  }
}

// bool ParamOverride2.ei__validate_apply()
ParamOverride2.prototype.ei__validate_apply=function() { // returns true if .apply() makes modification
  if (this.hasOwnProperty('_old')) {
    return false;
  }
  return !(((this.item.toString()=='' ? this.operation=='AUTO' : false) || this.operation=='SHOW_OR_HIDE') && this.target_visible==this.def_visible);
}

// void ParamOverride2.apply(P* p)
ParamOverride2.prototype.apply=function(p) {
  if (this.item instanceof PProp) { // P
    var target_dlist=p.props;
    var key=this.item.getId();
    if (target_dlist.has(key)) {
      if (this.operation=='REMOVE' || (this.operation=='AUTO' && this.item.toString()=='')) { // remove
        target_dlist.remove(key);
      } else if (this.operation=='ADD_OR_REPLACE' || (this.operation=='AUTO' && this.item.toString()!='')) { // replace
        target_dlist.replace(key,deep_copy(this.item));
      }
    } else if (this.operation=='ADD_OR_REPLACE' || (this.operation=='AUTO' && this.item.toString()!='')) { // add
      target_dlist.append(deep_copy(this.item));
    }
  } else if (this.item instanceof PExt || this.item instanceof PInstr) { // E/I
    var target_dlist=this.item instanceof PExt ? p.exts : p.instrs;
    var key=this.item.getId();
    if (this.hasOwnProperty('_old')) { // 2. gen
      if (target_dlist.has(key)) {
        if (this.operation=='REMOVE' || (this.operation=='AUTO' && this.item.toString()=='' && this.item instanceof PInstr)) { // remove
          target_dlist.remove(key);
        } else if (this.operation=='ADD_OR_REPLACE') { // replace
          target_dlist.replace(key,deep_copy(this.item));
        } else if (this.operation=='INSERT' || (this.operation=='AUTO' && this.item.toString()!='')) { // insert
          var replace_dlist=new DList();
          replace_dlist.load_array((function(ins_key) {
            var arr=target_dlist.map(function(item,key) {
              if (key>=ins_key) {
                item.id=Number(item.id)+1+'';
              }
              return item;
            });
            arr.splice(ins_key,0,deep_copy(this.item));
            return arr;
          })(key));
          p[target_field]=replace_dlist;
        }
      } else if (this.operation=='ADD_OR_REPLACE' || (this.operation=='AUTO' && this.item.toString()!='' && key>=SORT_INDEX_AUTO_APPEND)) { // add
        var item=deep_copy(this.item);
        if (key>=SORT_INDEX_AUTO_APPEND) { // SORT_INDEX_AUTO_APPEND id align
          item.id=Number(target_dlist.at(target_dlist.count()-1).getId())+1+'';
        }
        target_dlist.append(item);
      }
    }
    if (this.ei__validate_apply()) { // 2.1. gen
      if (this.target_visible && this.def_visible) { // transformed into replace
        target_dlist.replace(key,deep_copy(this.item));
      } else { // transformed into insert/add
        var insert_before=target_dlist.test(function(ext_or_instr) {
          return ext_or_instr.getId()<key;
        });
        if (insert_before==target_dlist.count()) {
          target_dlist.append(deep_copy(this.item));
        } else {
          target_dlist.insert_before(target_dlist.at(insert_before).getId(),deep_copy(this.item));
        }
      }
    }
  } else if (this.item.hasOwnProperty('f') && this.item.hasOwnProperty('refp_id')) { // K
    var kit=p.getKit();
    if (this.item.refp_id.length!=81) { // <sha1>.<sha1>
      if (kit.hasOwnProperty(this.item.f)) { // remove
        delete kit[this.item.f];
      }
    } else { // add, replace
      kit[this.item.f]=this.item.refp_id;
    }
  } else if (this.item.hasOwnProperty('f') && this.item.hasOwnProperty('value')) { // V
    var key=this.item.f;
    var has_key=p.hasAdditionalValue(key);
    var is_empty=this.item.value=='';
    if (has_key && is_empty) { // remove
      p.removeAdditionalValue(key);
    } else if (!(!has_key && is_empty)) { // add, replace
      p.setAdditionalValue(key,this.item.value);
    }
  } else { // unknown
    return;
  }
}

// bool ParamOverride2.keyEquals(ParamOverride2 param_override)
ParamOverride2.prototype.keyEquals=function(param_override) {
  return param_override instanceof ParamOverride2 && (
    ((this.item instanceof PProp && param_override.item instanceof PProp) || (this.item instanceof PExt && param_override.item instanceof PExt) || (this.item instanceof PInstr && param_override.item instanceof PInstr))
      ? this.item.getId()==param_override.item.getId()
      : ((this.item.hasOwnProperty('f') && param_override.item.hasOwnProperty('f') && this.item.hasOwnProperty('refp_id')==param_override.item.hasOwnProperty('refp_id')) ? this.item.f==param_override.item.f : false)
  );
}

// -- interface P* (extends DList.T)

// -- class PDContainer : P*

// PDContainer(str id, int schema, str pict_href, str label, arr<str> descs, str param_str, arr<PProp> props, arr<PExt> exts, arr<PInstr> instrs, str vendor_href, str site_href, str doc_href, map<ParamStruct_sha1 -> arr<ParamOverride>> param_override_map, str db_last_mod)
function PDContainer(id,schema,pict_href,label,descs,param_str,props,exts,instrs,vendor_href,site_href,doc_href,kit,additional,param_override_map,db_last_mod) {
  this._id=id;
  this._schema=schema;
  this.pict_href=pict_href;
  this.label=label;
  this.descs=descs;
  this.param_str=param_str;
  this.props=new DList();
  this.props.load_array(props);
  this.exts=new DList();
  this.exts.load_array(exts);
  this.instrs=new DList();
  this.instrs.load_array(instrs);
  this.vendor_href=vendor_href;
  this.site_href=site_href;
  this.doc_href=doc_href;
  this.kit=kit;
  this.additional_value_map=additional.value_map;
  this._param_override_map=param_override_map;
  this._db_last_mod=db_last_mod;
  this._refp_pool=new Object();
  this._kf=undefined; // set in parse_product
  this._additional_schema_ext=additional.schema_ext;
}

// {schema schema, str db_id, str subkey} PDContainer.parseId()
PDContainer.prototype.parseId=function() {
  var ret_obj=new Object();
  var str=this.getId();
  $.each(new Array(
    {ch: '.', obj_key: 'schema'},
    {ch: '#', obj_key: 'db_id'},
    {obj_key: 'subkey'}
  ),function(i,part_rec) {
    var pos=part_rec.hasOwnProperty('ch') ? str.indexOf(part_rec.ch) : -1;
    ret_obj[part_rec.obj_key]=str.substr(0,pos!=-1 ? pos : undefined);
    str=str.substr(pos!=-1 ? pos+1 : str.length);
  });
  ret_obj.is_R275_product=ret_obj.schema.substr(0,5)=='R275_';
  return ret_obj;
}

// @interface(DList.T)
// K PDContainer.getId()
//
// str PDContainer.getId()
PDContainer.prototype.getId=function() {
  return this._id;
}

// str PDContainer.getPictHref()
PDContainer.prototype.getPictHref=function() {
  return this.pict_href;
}

// str PDContainer.getLabel()
PDContainer.prototype.getLabel=function() {
  return this.label;
}

// str PDContainer.getAltLabel()
PDContainer.prototype.getAltLabel=function() {
  return this.descs.slice(-1)[0]
}

// str PDContainer.getDesc()
PDContainer.prototype.getDesc=function() {
  return this.descs[0];
}

// bool PDContainer.hasParamStr()
PDContainer.prototype.hasParamStr=function() {
  return this.getParamStr()!=null;
}

// str PDContainer.getParamStr()
PDContainer.prototype.getParamStr=function() {
  return this.param_str;
}

// bool/str PDContainer.getAdditionalParamStrId()
// P compatible
PDContainer.prototype.getAdditionalParamStrId=function() {
  var id=(function(pdc) {
    return pdc._additional_schema_ext.hasOwnProperty('REPLACING_PARAM_STR') && pdc._additional_schema_ext['REPLACING_PARAM_STR'].length>0 ? pdc._additional_schema_ext['REPLACING_PARAM_STR'][0].id : false;
  })(this instanceof P ? this._original : this);
  return (id && this.hasAdditionalValue(id)) ? id : false;
}

// DList PDContainer.getProps()
PDContainer.prototype.getProps=function() {
  return this.props;
}

// DList PDContainer.getExts()
PDContainer.prototype.getExts=function() {
  return this.exts;
}

// DList PDContainer.getInstrs()
PDContainer.prototype.getInstrs=function() {
  return this.instrs;
}

// str PDContainer.getVendorHref()
PDContainer.prototype.getVendorHref=function() {
  return this.vendor_href ? this.vendor_href : '';
}

// str PDContainer.getSiteHref()
PDContainer.prototype.getSiteHref=function() {
  return this.site_href ? this.site_href : '';
}

// str PDContainer.getDocHref()
PDContainer.prototype.getDocHref=function() {
  return this.doc_href ? this.doc_href : '';
}

// bool PDContainer.hasAnyParamWDefault()
PDContainer.prototype.hasAnyParamWDefault=function() {
  var param_inf_key=this.parseId().schema+'.'+this.parseId().db_id;
  return db.param_inf.hasOwnProperty(param_inf_key) && $.grep(db.param_inf[param_inf_key].params_order,function(param_id_str,param_index) {
    return db.param_inf[param_inf_key].params[param_id_str].default_opt!=null;
  }).length>0;
}

// str PDContainer.getDefaultParamStruct()
PDContainer.prototype.getDefaultParamStruct=function() {
  var param_struct=new ParamStruct();
  if (this.hasAnyParamWDefault()) {
    var param_inf_key=this.parseId().schema+'.'+this.parseId().db_id;
    $.each($.grep(db.param_inf[param_inf_key].params_order,function(param_id_str,param_index) {
      return db.param_inf[param_inf_key].params[param_id_str].default_opt!=null;
    }),function(param_index,param_id_str) {
      var param_struct_part=new Object();
      param_struct_part[param_id_str]=db.param_inf[param_inf_key].params[param_id_str].default_opt;
      $.extend(param_struct,param_struct_part);
    });
  }
  return param_struct;
}

// {str <kit_func>*} PDContainer.getKit()
PDContainer.prototype.getKit=function() {
  return this.kit;
}

// bool PDContainer.isKit()
PDContainer.prototype.isKit=function() {
  return Object.keys(this.getKit()).length>0;
}

// bool PDContainer.hasAdditionalValue(FieldID field)
PDContainer.prototype.hasAdditionalValue=function(field) {
  return this.additional_value_map.hasOwnProperty(field);
}

// <any> PDContainer.getAdditionalValue(FieldID field)
PDContainer.prototype.getAdditionalValue=function(field) {
  return this.hasAdditionalValue(field) ? this.additional_value_map[field] : '';
}

// void PDContainer.setAdditionalValue(FieldID field, <any> value)
PDContainer.prototype.setAdditionalValue=function(field,value) {
  this.additional_value_map[field]=value;
}

// void PDContainer.removeAdditionalValue(FieldID field)
PDContainer.prototype.removeAdditionalValue=function(field) {
  delete this.additional_value_map[field];
}

// PDContainer PDContainer.resolveRefP(str id)
PDContainer.prototype.resolveRefP=function(id) {
  return this._refp_pool[id];
}

// void PDContainer.all(? func(P*,bool/str) func)
PDContainer.prototype.all=function(func) {
  var kit_content=[this];
  var _this=this;
  if (this.isKit()) {
    var kit_keys=Object.keys(this.getKit());
    var kit_keys_sort_as=Object.keys(db.struct_inf[this.parseId().schema].kit_map);
    kit_keys.sort(function(a,b) {
      return kit_keys_sort_as.indexOf(a)-kit_keys_sort_as.indexOf(b);
    });
    $.merge(kit_content,$.map(kit_keys,function(kit_function) {
      return _this.getKit()[kit_function];
    }));
  }
  $.each(kit_content,function(index,kcp) {
    var child=index>0;
    func(child ? _this.resolveRefP(kcp) : kcp,child ? kit_keys[index-1] : false);
  });
}

// str PDContainer.getKf()
PDContainer.prototype.getKf=function() {
  return this._kf;
}

// -- class P : PDContainer

var psubkey_len=5;
var psubkey_base=36;

// P(PDContainer p, ParamStruct param)
function P(p,param) {
  this._original=p;
  this._param=param || new ParamStruct();
  this._subkey=(function(subkey_intstr) {
    while (subkey_intstr.length<psubkey_len) {
      subkey_intstr='0'+subkey_intstr;
    }
    return subkey_intstr;
  })(Math.floor(Math.random()*Math.pow(psubkey_base,psubkey_len)).toString(psubkey_base));
  var delayed_param_overrides=this.loadParamBuild();
  this.resetAll();
  this._desc_index=0;
  (function(parambuild) {
    $.each(delayed_param_overrides,function(index,param_override) {
      param_override.apply(parambuild);
    });
  })(this._parambuild);
}

P.prototype=Object.create(PDContainer.prototype);
P.prototype.constructor=P;

// static P P.createCustom(str label, str desc, str param_str, arr<PProp> props, str func(int) prop_name_func, arr<PExt> exts, arr<PInstr> instrs)
P.createCustom=function(label,desc,param_str,props,prop_name_func,exts,instrs,additional) {
  var schema='empty_schema';
  var prop_object=new Object();
  var pdc=new P(new PDContainer(
    schema+'._',
    schema,
    null,
    label,
    [desc],
    param_str,
    $.map(props,function(prop) {
      prop_object[prop.getId()]={
        name: prop_name_func(prop.getId()),
        priority: 1,
        values: new Array()
      };
      return prop;
    }),
    exts,
    instrs,
    null,
    null,
    null,
    new Object(),
    additional ? additional : {schema_ext: new Object(), value_map: new Object()},
    {_hidden_ei: new Array()},
    '_customp_'
  ));
  pdc._parambuild.param_str=param_str;
  pdc.param_str=param_str;
  pdc._indp_props=prop_object;
  return pdc;
}

var customp_schema='empty_schema';

// void P.setParam(str param_key, str param_value)
P.prototype.setParam=function(param_key,param_value) {
  if (param_value) {
    this._param[param_key]=param_value;
  } else {
    delete this._param[param_key];
  }
}

// void P.loadParamBuild()
P.prototype.loadParamBuild=function() {
  var parambuild=Object.create(PDContainer.prototype);
  parambuild.pict_href=this._original.pict_href;
  parambuild.label=this._original.label;
  parambuild.descs=deep_copy(this._original.descs);
  if (this.isNaturallyParameterized()) {
    parambuild.param_str=this._param.toParamStr(this);
  }
  parambuild.props=deep_copy(this._original.props);
  parambuild.exts=deep_copy(this._original.exts);
  parambuild.instrs=deep_copy(this._original.instrs);
  parambuild.vendor_href=this._original.vendor_href;
  parambuild.site_href=this._original.site_href;
  parambuild.doc_href=this._original.doc_href;
  parambuild.kit=deep_copy(this._original.kit);
  parambuild.additional_value_map=deep_copy(this._original.additional_value_map)
  var delayed_param_overrides=new Array();
  var ei__applied_map=new Object();
  var apply_core_f=(function(param_override,ei) {
    if (ei && param_override.ei__need_delayed_apply()) {
      param_override.ei__pre_delayed_apply(parambuild);
      delayed_param_overrides.push(param_override);
    } else {
      param_override.apply(parambuild);
    }
  });
  $.each(this.isNaturallyParameterized() ? (function(param_override_block) {
    var param=this._param;
    var rules_gen_block=new Array();
    var _this=this;
    var selected_param_ids=(function() {
      var for_schema_p=_this.parseId().schema+'.'+_this.parseId().db_id;
      return $.grep(Object.keys(param),function(key) {
        return !(ParamStruct.prototype.hasOwnProperty(key) || param[key]==undefined)
      }).sort(function(a,b) {
        return db.param_inf[for_schema_p].params_order.indexOf(a)-db.param_inf[for_schema_p].params_order.indexOf(b);
      });
    })();
    selected_param_ids.reverse();
    for (var i=0; i<=selected_param_ids.length; i++) {
      var std_paramstructs=$.map(ParamStruct.getCombinations(selected_param_ids,i),function(param_ids_to_add) {
        var t=new ParamStruct();
        $.each(param_ids_to_add,function() {
          t[this]=param[this];
        });
        return t;
      });
      var hint_keys={'AD_': new Array(), 'FD_': new Array()};
      for (var any_c=i; any_c>0; any_c--) {
        $.each(hint_keys,function(hint_key_postfix,array_to_merge_in) {
          var hint_key=i+''+any_c+hint_key_postfix;
          if ($.grep(Object.keys(_this._original._param_override_map),function(key) {
            return key.substr(0,hint_key.length)==hint_key;
          }).length>0) {
            $.merge(array_to_merge_in,$.map(std_paramstructs,function(t) {
              var this_backup=deep_copy(t);
              return $.map(ParamStruct.getCombinations(Object.keys(t),any_c),function(what_to_set_to_any) {
                $.each(what_to_set_to_any,function() {
                  t[this]=':any:';
                });
                var hash=t.toString(_this);
                $.each(what_to_set_to_any,function() {
                  t[this]=this_backup[this];
                });
                return hint_key+hash;
              });
            }));
          }
        });
      }
      $.each($.merge($.merge(hint_keys['AD_'],$.map(std_paramstructs,function(t) {
        return 'RULE_'+t.toString(_this);
      })),hint_keys['FD_']),function() {
        if (_this._original._param_override_map.hasOwnProperty(this)) {
          ParamOverride.blockOverwrite(_this._original._param_override_map[this],rules_gen_block);
        }
      });
    }
    if (param_override_block) {
      ParamOverride.blockOverwrite(param_override_block,rules_gen_block);
    }
    return rules_gen_block;
  }).call(this,this._original._param_override_map[this._param.toString(this)]) : this._param,function(index,param_override) {
    var ei=this.item instanceof PExt || this.item instanceof PInstr;
    if (ei) {
      if (!(param_override.hasOwnProperty('target_visible') && param_override.hasOwnProperty('def_visible'))) {
        param_override.target_visible=true;
        param_override.def_visible=true;
        param_override._old=true;
      }
      if (param_override.ei__validate_apply()) {
        ei__applied_map[param_override.item.getId()]=true;
      }
    }
    apply_core_f(param_override,ei);
  });
  $.each(this._original._param_override_map['_hidden_ei'],function(index,param_override) {
    if (!ei__applied_map.hasOwnProperty(param_override.item.getId())) {
      apply_core_f(param_override,true);
    }
  });
  if (this._original.isKit()) {
    var new_kit=new Object();
    $.each(db.struct_inf[this.parseId().schema].kit_map,function(kit_function,ref_p) { // forced way to resort map keys... :'(
      if (parambuild.kit.hasOwnProperty(kit_function)) {
        new_kit[kit_function]=parambuild.kit[kit_function];
      }
    });
    parambuild.kit=new_kit;
  }
  this._parambuild=parambuild;
  this._defvisible_ei_dlo=$.merge($.merge(new Array(),parambuild.exts.order),parambuild.instrs.order);
  return delayed_param_overrides;
}

// void P.resetAll()
P.prototype.resetAll=function() {
  this.pict_href=this._parambuild.pict_href;
  this.label=this._parambuild.label;
  this.descs=deep_copy(this._parambuild.descs);
  this.param_str=this._parambuild.param_str;
  this.props=deep_copy(this._parambuild.props);
  this.exts=deep_copy(this._parambuild.exts);
  this.instrs=deep_copy(this._parambuild.instrs);
  this.vendor_href=this._parambuild.vendor_href;
  this.site_href=this._parambuild.site_href;
  this.doc_href=this._parambuild.doc_href;
  this.kit=deep_copy(this._parambuild.kit);
  this.additional_value_map=deep_copy(this._parambuild.additional_value_map)
}

// bool P.isNaturallyParameterized()
P.prototype.isNaturallyParameterized=function () {
  return this._param instanceof ParamStruct;
}

// @interface(DList.T)
// K P.getId()
//
// @override
// str P.getId()
P.prototype.getId=function() {
  return this._original._id+'#'+this._subkey;
}

// @override
// str P.getAltLabel()
P.prototype.getAltLabel=function() {
  return this._parambuild.descs.slice(-1)[0]
}

// bool P.isAltLabelSet()
P.prototype.isAltLabelSet=function() {
  return this.getLabel()==this.getAltLabel();
}

// void P.overrideLabel(str label)
P.prototype.overrideLabel=function(label) {
  this.label=label;
}

// str P.getResetLabel()
P.prototype.getResetLabel=function() {
  return ((this.isKitChild() ? my_pool.get(this.kit_parent).arePropsReset() : true) && this.arePropsReset()) ? this._parambuild.label : this.getAltLabel();
}

// bool P.isLabelOverridden()
P.prototype.isLabelOverridden=function() {
  return !this.parseId().is_R275_product ? this.label!=this.getResetLabel() : false;
}

// bool P.isLabelOverridden_original()
P.prototype.isLabelOverridden_original=function() {
  return this.label!=this._parambuild.label;
}

// bool P.isLabelOverridden_stillauto()
P.prototype.isLabelOverridden_stillauto=function() {
  return !my_pool.label_oam_isset(this.getId());
}

// @override
// str P.getDesc()
P.prototype.getDesc=function() {
  return this.descs[this._desc_index];
}

// void P.overrideDesc(str desc)
P.prototype.overrideDesc=function(desc) {
  var index=this.descs.indexOf(desc);
  if (index==-1) {
    index=this._parambuild.descs.length;
    this.descs[index]=desc;
  }
  this._desc_index=index;
}

// str P.getResetDesc()
P.prototype.getResetDesc=function() {
  return this._parambuild.descs[0];
}

// bool P.isDescOverridden()
P.prototype.isDescOverridden=function() {
  return this._desc_index!=0;
}

// arr<str> P.getDesc_alt()
P.prototype.getDesc_alt=function() {
  return this._parambuild.descs;
}

// void P.overrideParamStr(str param_str)
P.prototype.overrideParamStr=function(param_str) {
  this.param_str=param_str;
}

// str P.getResetParamStr()
P.prototype.getResetParamStr=function() {
  return this._parambuild.param_str;
}

// bool P.isParamStrOverridden()
P.prototype.isParamStrOverridden=function() {
  return this.param_str!=this._parambuild.param_str;
}

// DList P.getProps_hidden()
P.prototype.getProps_hidden=function() {
  var empty_props=new DList();
  var schema=this.parseId().schema;
  var _this=this;
  empty_props.load_array($.map(db.struct_inf.hasOwnProperty(schema) ? db.struct_inf[schema].properties : this._indp_props,function(prop_rec,abbr) {
    return _this._parambuild.props.has(abbr) || _this.props.has(abbr) ? null : new EmptyPProp(abbr,schema);
  }));
  return DList.union(DList.substract(this._parambuild.props,this.props),empty_props);
}

// DList P.getProps_reset()
P.prototype.getProps_reset=function() {
  var empty_props=new DList();
  var schema=this.parseId().schema;
  var _this=this;
  empty_props.load_array($.map(db.struct_inf.hasOwnProperty(schema) ? db.struct_inf[schema].properties : this._indp_props,function(prop_rec,abbr) {
    return _this._parambuild.props.has(abbr) ? null : (_this.getProps().has(abbr) ? _this.getProps().get(abbr) : _this.getProps_hidden().get(abbr));
  }));
  return DList.union(this._parambuild.props,empty_props)
}

// bool P.isPropOverridden(abbr abbr)
P.prototype.isPropOverridden=function(abbr) {
  return !this.parseId().is_R275_product ? !this.props.get(abbr).equals(this.getProps_reset().get(abbr)) : false;
}

// bool P.arePropsReset()
P.prototype.arePropsReset=function() {
  var _this=this;
  return !this.props.find(function(prop) {
    return _this.isPropOverridden(prop.getPropId());
  }) && (this.isKitParent() ? $.grep(this.kit_references,function(kit_reference_id) {
    return !my_pool.get(kit_reference_id).arePropsReset();
  }).length==0 : true);
}

// arr<PProp> P.getProp_alt(abbr abbr)
P.prototype.getProp_alt=function(abbr) {
  var schema=this.parseId().schema;
  return (db.struct_inf.hasOwnProperty(schema) ? db.struct_inf[schema].properties : this._indp_props)[abbr].values;
}

// DList P.getExts_hidden()
P.prototype.getExts_hidden=function() {
  return DList.substract(this._parambuild.exts,this.exts);
}

// DList P.getExts_reset()
P.prototype.getExts_reset=function() {
  return this._parambuild.exts;
}

// bool P.isExtOverridden(int id)
P.prototype.isExtOverridden=function(id) {
  return !this.exts.get(id).equals(this._parambuild.exts.get(id));
}

// arr<PExt> P.getExt_alt(int id)
P.prototype.getExt_alt=function(id) {
  var ext=this._parambuild.exts.get(id);
  var ext_alt_false=deep_copy(ext);
  var ext_alt_true=deep_copy(ext);
  ext_alt_false.alt=true;
  return new Array(ext_alt_true,ext_alt_false);
}

// DList P.getInstrs_hidden()
P.prototype.getInstrs_hidden=function() {
  return DList.substract(this._parambuild.instrs,this.instrs);
}

// DList P.getInstrs_reset()
P.prototype.getInstrs_reset=function() {
  return this._parambuild.instrs;
}

// bool P.isInstrOverridden(int id)
P.prototype.isInstrOverridden=function(id) {
  return !this.instrs.get(id).equals(this._parambuild.instrs.get(id));
}

// arr<PInstr> P.getInstr_alt(int id)
P.prototype.getInstr_alt=function(id) {
  var instr=this._parambuild.instrs.get(id);
  return new Array(deep_copy(instr));
}

// PDContainer P.resolveRefP(str id)
P.prototype.resolveRefP=function(id) {
  return this._original._refp_pool[id];
}

// bool P.isKitParent()
P.prototype.isKitParent=function() {
  return this.hasOwnProperty('kit_references');
}

// bool P.isKitChild()
P.prototype.isKitChild=function() {
  return this.hasOwnProperty('kit_parent');
}

// bool P.isAnyKit()
P.prototype.isAnyKit=function() {
  return this.isKitParent() || this.isKitChild();
}

// void P.parent_all(bool reverse,? func(str,P*) func)
P.prototype.parent_all=function(reverse,func) {
  var kit_content=[this.getId()];
  if (this.isKitParent()) {
    $.merge(kit_content,this.kit_references);
  }
  if (reverse) {
    kit_content.reverse();
  }
  $.each(kit_content,function(index,kcpid) {
    func(kcpid,my_pool.get(kcpid));
  });
}

// str P.getKf()
P.prototype.getKf=function() {
  return this._original._kf;
}

// arr<{str id, str name, bool render_as_category, bool editable_if_empty}> P.getAdditionals(str add_position_id, bool extended)
P.prototype.getAdditionals=function(add_position_id,extended) {
  if (this._original._additional_schema_ext.hasOwnProperty(add_position_id)) {
    var _this=this;
    var add_arr=$.map(this._original._additional_schema_ext[add_position_id],function(field_rec) {
      if (isAdditionalRecCategory(field_rec)) {
        return field_rec;
      } else {
        var value=_this.getAdditionalValue(field_rec.id);
        if (value || (extended ? _this._parambuild.getAdditionalValue(field_rec.id) || field_rec.editable_if_empty : false)) {
          return $.extend({value: value},field_rec);
        }
      }
    });
    return $.grep(add_arr,function(field_rec,index) {
      return isAdditionalRecCategory(field_rec) ? index==0 || (index!=add_arr.length-1 && !isAdditionalRecCategory(add_arr[index-1])) : true;
    });
  } else {
    return new Array();
  }
}

// bool isAdditionalRecCategory({str id, str name, bool render_as_category, bool editable_if_empty} field_rec)
function isAdditionalRecCategory(field_rec) {
  return field_rec.render_as_category && field_rec.name;
}

// -- class TitlePg

var titlepg_id='titlepg';

// TitlePg()
function TitlePg() {
  this.title=this.getResetTitle();
  this.stitle=this.getResetSubtitle();
  this.f_state='';
  this.f_id='';
  this.f_author=user_data.is_guest ? 'vendég' : user_data.username;
}

// str TitlePg.getId()
TitlePg.prototype.getId=function() {
  return titlepg_id;
}

// str TitlePg.getTitle()
TitlePg.prototype.getTitle=function() {
  return this.title;
}

// void TitlePg overrideTitle(str title)
TitlePg.prototype.overrideTitle=function(title) {
  this.title=title;
}

// str TitlePg.getResetTitle()
TitlePg.prototype.getResetTitle=function() {
  return '{{TITLE}}';
}

// bool TitlePg.isTitleOverridden() {
TitlePg.prototype.isTitleOverridden=function() {
  return this.getTitle()!=this.getResetTitle();
}

// str TitlePg.getSubtitle()
TitlePg.prototype.getSubtitle=function() {
  return this.stitle;
}

// void TitlePg overrideSubtitle(str stitle)
TitlePg.prototype.overrideSubtitle=function(stitle) {
  this.stitle=stitle;
}

// str TitlePg.getResetSubtitle()
TitlePg.prototype.getResetSubtitle=function() {
  return 'Teljesítménykiírás';
}

// str TitlePg.getResetSubtitle_alt()
TitlePg.prototype.getResetSubtitle_alt=function() {
  return 'Termékkiírás';
}

// bool TitlePg.isSubtitleOverridden() {
TitlePg.prototype.isSubtitleOverridden=function() {
  return this.getSubtitle()!=this.getResetSubtitle();
}

// map<str -> str> TitlePg.getFooterKeys()
TitlePg.prototype.getFooterKeys=function() {
  return {
    'state': 'Terv állapot',
    'id': 'Terv azonosító',
    'author': 'Készítette'
  };
}

// str TitlePg.getFooter(str key)
TitlePg.prototype.getFooter=function(key) {
  return this['f_'+key];
}

// void TitlePg overrideFooter(str key, str fstr)
TitlePg.prototype.overrideFooter=function(key,fstr) {
  this['f_'+key]=fstr;
}

// -- class Doc : DList

// Doc(Doc inherit_from)
function Doc(inherit_from) {
  DList.call(this);
  if (inherit_from) {
    this.union(inherit_from.filter(function(i) {
      return i instanceof TitlePg;
    }));
  }
  this.settings=inherit_from ? inherit_from.settings : new Object();
  this.label_oam=new Object();
}

Doc.prototype=Object.create(DList.prototype);
Doc.prototype.constructor=Doc;

// DList Doc.ps()
Doc.prototype.ps=function() {
  return this.filter(function(e) {
    return e instanceof P;
  });
}

// DList Doc.ps2()
Doc.prototype.ps2=function() {
  return this.ps().filter(function(e) {
    return !e.isKitChild();
  });
}

// bool Doc.has_titlepg()
Doc.prototype.has_titlepg=function() {
  return this.count()>0 && this.at(0) instanceof TitlePg;
}

// void Doc.sset(str key, <any> value)
Doc.prototype.sset=function(key,value) {
  this.settings[key]=value;
}

// <any> Doc.sget(str key)
Doc.prototype.sget=function(key) {
  if (this.settings.hasOwnProperty(key)) {
    return this.settings[key];
  }
}

// bool Doc.label_oam_isset(str id)
Doc.prototype.label_oam_isset=function(id) {
  return this.label_oam.hasOwnProperty(id);
}

// void Doc.label_oam_add(str id)
Doc.prototype.label_oam_add=function(id) {
  this.label_oam[id]=true;
}

// void Doc.label_oam_remove(str id)
Doc.prototype.label_oam_remove=function(id) {
  delete this.label_oam[id];
}