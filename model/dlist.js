// -- class DList
// @template(K keytype, T itemtype)

// -- interface T

// K T.getId()

// DList()
function DList() {
  this.clear();
}

// static arr<constructor() Error> exceptions
DList.exceptions=new Object();
$.each({
  KEY_RESERVED:             'Key already reserved',
  REFERENCE_KEY_NOT_FOUND:  'Reference key is not reserved',
  KEY_NOT_FOUND:            'Key is not reserved',
  INDEX_OUT_OF_BOUNDS:      'Index is out of bounds'
},function(id,msg) {
  DList.exceptions[id]=function() {
    throw Object.create(DList.exceptions[id].prototype);
  }
  DList.exceptions[id].prototype.toString=function() {
    return 'DList exception: '+msg;
  }
});

// undocumented void DList._apply_wsarr(arr<*> wsarr, void func(T) apply_func)
DList.prototype._apply_wsarr=function(wsarr,apply_func) {
  var _this=this;
  $.each(wsarr,function(index,i) {
    apply_func.call(_this,i);
  });
};
// undocumented void DList._apply_wsarr_append(arr<T> wsarr)
DList.prototype._apply_wsarr_append=function(wsarr) {
  return this._apply_wsarr(wsarr,function(item) {this.append(item);});
}
// undocumented void DList._apply_wsarr_append_or_replace(arr<T> wsarr)
DList.prototype._apply_wsarr_append_or_replace=function(wsarr) {
  return this._apply_wsarr(wsarr,function(item) {this.append_or_replace(item);});
}
// undocumented void DList._apply_wsarr_remove(arr<K> wsarr)
DList.prototype._apply_wsarr_remove=function(wsarr) {
  return this._apply_wsarr(wsarr,function(key) {this.remove(key);});
}

// K DList.key_func(T item)
DList.prototype.key_func=function(item) {
  return item.getId();
}

// K DList.key_func_direct(T item)
DList.prototype.key_func_direct=function(item) {
  return item.toString();
}

// void DList.load_array(arr<T> items)
// @throws KEY_RESERVED
DList.prototype.load_array=function(items) {
  var ws=[];
  $.each(items,function(i,item) {
    ws.push(item);
  });
  this._apply_wsarr_append(ws);
}

// T DList.get(K key)
// @throws KEY_NOT_FOUND
DList.prototype.get=function(key) {
  if (!this.has(key))
    DList.exceptions.KEY_NOT_FOUND();
  return this.pool[key];
}

// bool DList.has(K key)
DList.prototype.has=function(key) {
  return this.order.indexOf(key)!=-1;
}

// bool DList.has_poolcheck(K key)
DList.prototype.has_poolcheck=function(key) {
  return this.has(key) && this.pool.hasOwnProperty(key);
}

// void DList.append(T item)
// @throws KEY_RESERVED
DList.prototype.append=function(item) {
  var key=this.key_func(item);
  if (this.has(key))
    DList.exceptions.KEY_RESERVED();
  this.pool[key]=item;
  this.order.push(key);
}

// void DList.insert_after(K key_after, T item)
// @throws KEY_RESERVED
// @throws REFERENCE_KEY_NOT_FOUND
DList.prototype.insert_after=function(key_after,item) {
  var key=this.key_func(item);
  if (this.has(key))
    DList.exceptions.KEY_RESERVED();
  if (!this.has(key_after))
    DList.exceptions.REFERENCE_KEY_NOT_FOUND();
  this.pool[key]=item;
  this.order.splice(this.index(key_after)+1,0,key);
}

// void DList.insert_before(K key_before, T item)
// @throws KEY_RESERVED
// @throws REFERENCE_KEY_NOT_FOUND
DList.prototype.insert_before=function(key_before,item) {
  var key=this.key_func(item);
  if (this.has(key))
    DList.exceptions.KEY_RESERVED();
  if (!this.has(key_before))
    DList.exceptions.REFERENCE_KEY_NOT_FOUND();
  this.pool[key]=item;
  this.order.splice(this.index(key_before),0,key);
}

// void DList.insert_absolute(T item, int index)
// @throws KEY_RESERVED
// @throws INDEX_OUT_OF_BOUNDS
DList.prototype.insert_absolute=function(item,index) {
  var key=this.key_func(item);
  if (this.has(key))
    DList.exceptions.KEY_RESERVED();
  if (!(index>=0 && index<=this.count()))
    DList.exceptions.INDEX_OUT_OF_BOUNDS();
  this.pool[key]=item;
  this.order.splice(index,0,key);
}

// void DList.replace(K key_replace, T item)
// @throws KEY_RESERVED
// @throws REFERENCE_KEY_NOT_FOUND
DList.prototype.replace=function(key_replace,item) {
  var key=this.key_func(item);
  var key_eq=key==key_replace;
  if (!key_eq && this.has(key))
    DList.exceptions.KEY_RESERVED();
  if (!this.has(key_replace))
    DList.exceptions.REFERENCE_KEY_NOT_FOUND();
  if (key_eq) {
    this.pool[key_replace]=item;
  } else {
    delete this.pool[key_replace];
    this.pool[key]=item;
    this.order.splice(this.index(key_replace),1,key);
  }
}

// void DList.append_or_replace(T item)
DList.prototype.append_or_replace=function(item) {
  try {
    this.append(item);
  } catch (err) {
    if (err instanceof DList.exceptions.KEY_RESERVED) {
      this.replace(this.key_func(item),item);
    } else {
      throw err;
    }
  }
}

// void DLIst.remove(K key)
// @throws KEY_NOT_FOUND
DList.prototype.remove=function(key) {
  if (!this.has(key))
    DList.exceptions.KEY_NOT_FOUND();
  delete this.pool[key];
  this.order.splice(this.index(key),1);
}

// void DList.clear()
DList.prototype.clear=function() {
  this.pool=new Object();
  this.order=new Array();
}

// int DList.count()
DList.prototype.count=function() {
  return this.order.length;
}

// bool DList.empty()
DList.prototype.empty=function() {
  return this.count()==0;
}

// int DList.index(K key)
// @throws KEY_NOT_FOUND
DList.prototype.index=function(key) {
  if (!this.has(key))
    DList.exceptions.KEY_NOT_FOUND();
  return this.order.indexOf(key);
}

// T DList.at(int index)
// @throws INDEX_OUT_OF_BOUNDS
DList.prototype.at=function(index) {
  if (!(index>=0 && index<this.count()))
    DList.exceptions.INDEX_OUT_OF_BOUNDS();
  return this.get(this.order[index]);
}

// int DList.test(bool func(T,int) test_func, optional int limit)
DList.prototype.test=function(test_func,limit) {
  limit=limit || Infinity;
  var hit=0;
  if (limit>0) {
    var _this=this;
    $.each(this.order,function(index,key) {
      if (test_func(_this.pool[key],index)===true) {
        return ++hit!=limit;
      }
    });
  }
  return hit;
}

// bool DList.find(bool func(T,int) test_func)
DList.prototype.find=function(test_func) {
  return this.test(test_func,1)==1;
}

// void DList.move_absolute(K key, int index, optional bool throw_out_of_bounds=false)
// @throws KEY_NOT_FOUND
// @throws INDEX_OUT_OF_BOUNDS
DList.prototype.move_absolute=function(key,index,throw_out_of_bounds) {
  throw_out_of_bounds=throw_out_of_bounds || false;
  if (!this.has(key))
    DList.exceptions.KEY_NOT_FOUND();
  if (throw_out_of_bounds) {
    if (!(index>=0 && index<this.count()))
      DList.exceptions.INDEX_OUT_OF_BOUNDS();
  } else {
    index=Math.max(Math.min(index,this.count()-1),0);
  }
  var key_index=this.index(key);
  if (key_index!=index) {
    this.order.splice(key_index,1);
    this.order.splice(index,0,key);
  }
}

// void DList.move_relative(K key, int diff, optional bool throw_out_of_bounds=false)
// @throws KEY_NOT_FOUND
// @throws INDEX_OUT_OF_BOUNDS
DList.prototype.move_relative=function(key,diff,throw_out_of_bounds) {
  throw_out_of_bounds=throw_out_of_bounds || false;
  var key_index=this.index(key);
  var index=key_index+diff;
  if (!this.has(key))
    DList.exceptions.KEY_NOT_FOUND();
  if (throw_out_of_bounds) {
    if (!(index>=0 && index<this.count()))
      DList.exceptions.INDEX_OUT_OF_BOUNDS();
  } else {
    index=Math.max(Math.min(index,this.count()-1),0);
  }
  if (diff!=0) {
    this.order.splice(key_index,1);
    this.order.splice(index,0,key);
  }
}

// void DList.sort(int func(T,int) sort_func)
// NOTE: the smaller value sort_func returns, the more lower index the item gets
DList.prototype.sort=function(sort_func) {
  var _this=this;
  var new_order=$.merge(new Array(),this.order);
  var original_order=this.order;
  new_order.sort(function(key_a,key_b) {
    var s=sort_func(_this.pool[key_a],_this.index(key_a))-sort_func(_this.pool[key_b],_this.index(key_b));
    return s==0 ? original_order.indexOf(key_a)-original_order.indexOf(key_b) : s;
  });
  this.order=new_order;
}

// void DList.all(? func(T,int) func)
DList.prototype.all=function(func) {
  var _this=this;
  $.each(this.order,function(index,key) {
    func(_this.pool[key],index);
  });
}

// @template(A array_type)
// arr<A> DList.map(A func(T,int) func)
DList.prototype.map=function(func) {
  var arr=new Array();
  var _this=this;
  $.each(this.order,function(index,key) {
    var arr_val=func(_this.pool[key],index);
    if (arr_val) {
      arr.push(arr_val);
    }
  });
  return arr;
}

// DList DList.copy()
DList.prototype.copy=function() {
  var copy=new DList();
  if (this.hasOwnProperty('key_func')) {
    copy.key_func=this.key_func;
  }
  $.extend(copy.pool,this.pool);
  $.merge(copy.order,this.order);
  return copy;
}

// DList DList.filter(bool func(T,int) filter_func)
DList.prototype.filter=function(filter_func) {
  var filtered=new DList();
  var _this=this;
  $.each(this.order,function(index,key) {
    if (filter_func(_this.pool[key],index)===true) {
      filtered.append(_this.pool[key]);
    }
  });
  return filtered;
}

// undocumented static DList func(DList,DList) _execute_apart(void func(DList) execute_func)
DList._execute_apart=function(execute_func) {
  return function(dlist1,dlist2) {
    var result_dlist=dlist1.copy();
    execute_func.call(result_dlist,dlist2)
    return result_dlist;
  }
}

// void DList.union(DList dlist)
// @throws KEY_RESERVED
DList.prototype.union=function(dlist) {
  var ws=[];
  dlist.all(function(item) {
    ws.push(item);
  });
  this._apply_wsarr_append(ws);
}

// static DList DList.union(DList dlist1, DList dlist2)
// @throws KEY_RESERVED
DList.union=DList._execute_apart(DList.prototype.union);

// void DList.union_replace(DList dlist)
DList.prototype.union_replace=function(dlist) {
  var ws=[];
  dlist.all(function(item) {
    ws.push(item);
  });
  this._apply_wsarr_append_or_replace(ws);
}

// static DList DList.union_replace(DList dlist1, DList dlist2)
DList.union_replace=DList._execute_apart(DList.prototype.union_replace);

// void DList.intersect(DList dlist)
DList.prototype.intersect=function(dlist) {
  var ws=[];
  this.all(function(item) {
    var key=dlist.key_func(item);
    if (!dlist.has(key)) {
      ws.push(key);
    }
  });
  this._apply_wsarr_remove(ws);
}

// static DList DList.intersect(DList dlist1, DList dlist2)
DList.intersect=DList._execute_apart(DList.prototype.intersect);

// void DList.substract(DList dlist)
DList.prototype.substract=function(dlist) {
  var ws=[];
  this.all(function(item) {
    var key=dlist.key_func(item);
    if (dlist.has(key)) {
      ws.push(key);
    }
  });
  this._apply_wsarr_remove(ws);
}

// static DList DList.substract(DList dlist1, DList dlist2)
DList.substract=DList._execute_apart(DList.prototype.substract);