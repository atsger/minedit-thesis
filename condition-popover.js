$(document).ready(function() {
  
  $('.form-label[for="condition"] .popover').find('.popover-container').append(
    $('<div class="card">').append(
      $('<div class="card-header">').append(
        $('<div class="card-title h6">').text('A szűrési feltétel megfogalmazása')
      ),
      $('<div class="card-body">').append(
        $('<small>').append(
          $('<p>').text('A szűrési feltétel egy logikai kifejezés, ami alapján a kiválasztott szerkezeti elem csoporthoz tartozó termékeket szűrni lehet.'),
          $('<p>').text('Támogatott operátorok (precedencia szerint):'),
          $('<ul class="conventional-operators">').append(
            $('<li>').text('Halmaz elemvizsgálat').prepend($('<code>').text('in (...)')),
            $('<li>').text('Logikai NEM').prepend($('<code>').text('!')),
            $('<li>').text('Relációs jelek').prepend($('<code>').text('<= < >= >')),
            $('<li>').text('Egyenlőség/egyenlőtlenség').prepend($('<code>').text('== !=')),
            $('<li>').text('Logikai ÉS').prepend($('<code>').text('&&')),
            $('<li>').text('Logikai VAGY').prepend($('<code>').text('||')),
          ),
          $('<p>').text('Konstansok megadhatóak számként, illetve karakterláncként, pl.: ').append($('<code>').text('42 3.14 "example"')),
          $('<p>').text('A termék teljesítményjellemzőire behelyettesíthető változó beszúrásához használja a feltétel alatt megjelenített teljesítményjellemző listát!'),
          $('<p>').text('A termék sajátosságai vizsgálhatóak az alábbi kifejezésekkel:'),
          $('<ul class="pdc-operators">').append(
            $('<li>').text('$1 van megadott teljesítmény, $2 paraméterezhető termék'),
            $('<li>').text('$3 készlet, $4 készlet alá sorolt termék')
          )
        )
      )
    )
  );
  
  $('form .form-label[for="condition"] .popover .popover-container .conventional-operators').find('code').after(' ');
  
  function replaceArgument(substr,replacement) {
    var last_txtnode=this.contents().get().pop();
    var substr_txtnode=last_txtnode.splitText(last_txtnode.textContent.indexOf(substr));
    substr_txtnode.splitText(substr.length);
    $(substr_txtnode).replaceWith(replacement);
  }
  
  var pdc_operators_ul=$('form .form-label[for="condition"] .popover .popover-container .pdc-operators');
  replaceArgument.call(pdc_operators_ul.find('li').eq(0),'$1',$('<code>').text('has any prop'));
  replaceArgument.call(pdc_operators_ul.find('li').eq(0),'$2',$('<code>').text('has any param'));
  replaceArgument.call(pdc_operators_ul.find('li').eq(1),'$3',$('<code>').text('is kit'));
  replaceArgument.call(pdc_operators_ul.find('li').eq(1),'$4',$('<code>').text('is kit child'));
  
});
