<?php

$schema_id=$rest->getPathArg('schema');

require_once 'mysqli.php';

$db=mnSqlConnect();

if (
  ($stmt_schema_info=$db->prepare('SELECT *'.
    ' FROM SCHEMA_POOL'.
    ' WHERE SCHEMA_ID=?')) &&
  $stmt_schema_info->bind_param('s',$schema_id) &&
  $stmt_schema_info->execute() &&
  
  $result_schema_info=$stmt_schema_info->get_result()
) {
  
  if ($result_schema_info->num_rows==0) {
    header('HTTP/1.0 400 Bad Request');
    return;
  }
  
  $stmt_schema_info->close();
  unset($stmt_schema_info);
  
  $schema_row=$result_schema_info->fetch_assoc();
  $result_schema_info->free();
  unset($result_schema_info);
} else
  trigger_error('Preparing statement failed, @checking schema id',E_USER_ERROR);

if (
  ($stmt_props=$db->prepare('SELECT PROPERTY_ID, NAME, DEFAULT_UNIT'.
    ' FROM PROPERTY_POOL'.
    ' WHERE SCHEMA_ID=?'.
    ' ORDER BY NAME')) &&
  $stmt_props->bind_param('s',$schema_id) &&
  $stmt_props->bind_result($prop_id,$prop_name,$default_unit) &&
  $stmt_props->execute()
) {
  
  $properties=[];
  
  while ($stmt_props->fetch()) {
    $properties[$prop_id]=['name' => $prop_name, 'default_unit' => is_null($default_unit) ? false : $default_unit];
  }
  $stmt_props->close();
  unset($stmt_props);
  
  unset($prop_id);
  unset($prop_name);
  unset($default_unit);
  
} else
  trigger_error('Preparing statement failed, @collecting properties',E_USER_ERROR);

if (
  ($stmt_kit_funcs=$db->prepare('SELECT FUNCTION_ID, NAME'.
    ' FROM KIT_FUNCTIONS'.
    ' WHERE KIT_SCHEMA_ID=?'.
    ' ORDER BY SORT_INDEX')) &&
  $stmt_kit_funcs->bind_param('s',$schema_id) &&
  $stmt_kit_funcs->bind_result($func_id,$func_name) &&
  $stmt_kit_funcs->execute()
) {
  
  $kit_funcs=[];
  
  while ($stmt_kit_funcs->fetch()) {
    $kit_funcs[]=['id' => $func_id, 'name' => $func_name];
  }
  $stmt_kit_funcs->close();
  unset($stmt_kit_funcs);
  
  unset($func_name);
  
  if (!empty($kit_funcs)) {
    
    $kit_func_ids=array_column($kit_funcs,'id');
    
    if (
      ($stmt_kf_content=$db->prepare('SELECT KIT_VENDOR, KIT_FUNCTION_ID, CONCAT(SCHEMA_ID,".",PRODUCT_ID)'.
        ' FROM KF_CONTENT NATURAL JOIN (SELECT PRODUCT_ID, CONCAT(VENDOR," ",PRODUCT_NAME) AS ORDER_EXPR FROM PRODUCT_POOL) AS ORDER_JOIN'.
        ' WHERE KIT_SCHEMA_ID=?'.
        ' ORDER BY ORDER_EXPR')) &&
      $stmt_kf_content->bind_param('s',$schema_id) &&
      $stmt_kf_content->bind_result($kf_id,$func_id,$child_ref_id) &&
      $stmt_kf_content->execute()
    ) {
      
      $kf_map=[];
      
      while ($stmt_kf_content->fetch()) {
        
        if (!array_key_exists($kf_id,$kf_map)) {
          $kf_map[$kf_id]=[];
          foreach ($kit_func_ids as $kit_func_id) {
            $kf_map[$kf_id][$kit_func_id]=[];
          }
        }
        
        $kf_map[$kf_id][$func_id][]=$child_ref_id;
      }
      $stmt_kf_content->close();
      unset($stmt_kf_content);
      
      unset($kf_id);
      unset($func_id);
      unset($child_ref_id);
      
      unset($kit_func_id);
      
    } else
      trigger_error('Preparing statement failed, @collecting kf content',E_USER_ERROR);
    
    unset($kit_func_ids);
    
  } else {
    $kf_map=[];
  }
  
  unset($func_id);
  
} else
  trigger_error('Preparing statement failed, @collecting kit functions',E_USER_ERROR);

if (
  ($stmt_params=$db->prepare('SELECT CONCAT(SCHEMA_ID,".",PRODUCT_ID) AS PARAM_P_REF, PARAM_ID, PARAM_NAME, PARAM_STR_FORMAT, DEFAULT_OPT_ID, PARAMOPT_ID, PARAMOPT_NAME'.
    ' FROM DOP_PARAMS NATURAL JOIN PARAM_POOL NATURAL JOIN PARAMOPT_POOL'.
    ' WHERE SCHEMA_ID=?'.
    ' ORDER BY PRODUCT_ID, SORT_INDEX, PARAMOPT_NAME')) &&
  $stmt_params->bind_param('s',$schema_id) &&
  $stmt_params->bind_result($param_p_ref,$param_id,$param_name,$param_str_format,$default_option_id,$option_id,$option_name) &&
  $stmt_params->execute()
) {
  
  $param_inf=[];
  
  while ($stmt_params->fetch()) {
    
    if (!array_key_exists($param_p_ref,$param_inf)) {
      $param_inf[$param_p_ref]=['params' => [], 'params_order' => []];
    }
    
    if (!array_key_exists($param_id,$param_inf[$param_p_ref]['params'])) {
      $param_inf[$param_p_ref]['params'][$param_id]=['name' => $param_name, 'str_format' => $param_str_format, 'options' => [], 'default_opt' => $default_option_id];
      $param_inf[$param_p_ref]['params_order'][]=$param_id;
    }
    
    $param_inf[$param_p_ref]['params'][$param_id]['options'][]=['value' => $option_name, 'paramopt_id' => $option_id];
  }
  $stmt_params->close();
  unset($stmt_params);
  
  unset($param_p_ref);
  unset($param_id);
  unset($param_name);
  unset($param_str_format);
  unset($default_option_id);
  unset($option_id);
  unset($option_name);
  
} else
  trigger_error('Preparing statement failed, @collecting params and options',E_USER_ERROR);

header('Content-Type: application/json; charset=utf-8');
echo json_encode([
  'name'                  =>  $schema_row['NAME'],
  'properties'            =>  $properties,
  'kit_functions'         =>  $kit_funcs,
  'kf_map'                =>  $kf_map,
  'param_inf'             =>  $param_inf
],JSON_NUMERIC_CHECK);

?>
