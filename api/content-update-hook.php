<?php

restore_error_handler();

ini_set('implicit_flush',1);
ob_implicit_flush(1);
ob_start();

function flush_chunk() { flush(); ob_flush(); }

define('COLLECT_TYPE_LEN',12); // characters
define('COLLECT_NUM_LEN',5); // characters

echo '<pre>';

echo <<<EOH
/---------------------------------------\
|          CONTENT UPDATE HOOK          |
|          import new db build          |
|   (may take a while to complete...)   |
\---------------------------------------/

EOH;

ob_end_flush();
flush();
ob_flush();

define('EVLOG',fopen('last-update-hook-output.log','w'));
function echo_ev_log($s) {
  echo $s;
  fwrite(EVLOG,$s);
}

echo_ev_log('[Current time is: '.date('r').']'."\n");
echo_ev_log('[Initiated by '.$_SERVER['PHP_AUTH_USER'].']'."\n");
flush_chunk();

register_shutdown_function(function() {
  
  global $db_conn, $buffer_import, $rollback_hnd;
  
  if (!is_null($db_conn)) {
    $db_conn->close();
  }

  unset($db_conn);
  unset($buffer_import);
  unset($rollback_hnd);
  
  if (defined('ERRMSG')) {
    echo_ev_log("\n".ERRMSG);
  }
  fclose(EVLOG);
  
  echo '</pre>';
  
});

require_once 'mysqli.php';

echo_ev_log('[Collecting sql files]'."\n");
flush_chunk();

class FileBasenameMatchFilter extends FilterIterator {
  private $match_pattern;
  public function setPattern($match_pattern) {
    $this->match_pattern=$match_pattern;
  }
  public function accept() {
    return is_file(parent::current()) && fnmatch($this->match_pattern,basename(parent::current()));
  }
}

$sql_files=array();
$db_upload_path='./../db_src/';
$fn_iterator=new FileBasenameMatchFilter(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($db_upload_path ? $db_upload_path : '.',FilesystemIterator::CURRENT_AS_PATHNAME)));
unset($db_upload_path);

$echo_collect_stat_line=(function($type,$count) {
  echo_ev_log($type.':'.str_repeat(' ',(COLLECT_TYPE_LEN-strlen($type))+(COLLECT_NUM_LEN-strlen($count))).$count.' file'.($count>1 ? 's' : '')."\n");
});
$count_param_files=0;
foreach (array('_schema','schema','vendor','product','kf','kit') as $prefix) {
  
  $fn_iterator->setPattern($prefix.'*.sql');
  
  $prefix_sqlfiles=array();
  foreach ($fn_iterator as $fn) {
    $prefix_sqlfiles[]=$fn;
  }
  
  if ($prefix=='product' || $prefix=='kit') {
    $count_prefix_param_files=count(array_filter($prefix_sqlfiles,function($fn) {
      return fnmatch('*_param.sql',$fn);
    }));
    $echo_collect_stat_line($prefix,count($prefix_sqlfiles)-$count_prefix_param_files);
    $count_param_files+=$count_prefix_param_files;
    unset($count_prefix_param_files);
    if ($prefix=='kit') {
      $echo_collect_stat_line('param',$count_param_files);
      unset($count_param_files);
    }
  } else {
    $echo_collect_stat_line($prefix,count($prefix_sqlfiles));
  }
  flush_chunk();
  
  if (natcasesort($prefix_sqlfiles)) {
    $sql_files=array_merge($sql_files,array_values($prefix_sqlfiles));
  } else {
    define('ERRMSG','Could not execute natcasesort() on file array');
    die;
  }
  
}

echo_ev_log(str_repeat('-',1+COLLECT_TYPE_LEN+COLLECT_NUM_LEN+strlen(' files'))."\n");
$echo_collect_stat_line('Sum',count($sql_files));
unset($echo_collect_stat_line);

echo_ev_log('[Initializing import environment]'."\n");
flush_chunk();

unset($fn_iterator);
unset($prefix);
unset($prefix_sqlfiles);
unset($fn);
$varmap=array();
$sql_buffer;
import_connect_to_db:
$db_conn=mnSqlConnect();

$buffer_import=(function(&$buffer) {
  global $db_conn;
  if ($db_conn->multi_query($buffer)) {
    do {
      $result=$db_conn->use_result();
      if ($result) {
        $result->free();
      } else {
        if ($db_conn->errno!=0) {
          return false;
        }
      }
      if (!$db_conn->more_results()) {
        return true;
      }
      $db_conn->next_result();
    } while (true);
  } else {
    return false;
  }
});

echo_ev_log('[Importing]'."\n");
flush_chunk();

set_time_limit(60*8);

$unknown_var_dirty=false;
$unknown_var_dirty_hnd=(function() {
  global $unknown_var_dirty;
  if ($unknown_var_dirty) {
    echo_ev_log(str_repeat('-',12)."\n");
    echo_ev_log('Any "Unknown variable" message is just a warning -- these variables are possibly in a comment.'."\n");
    echo_ev_log('If not, and that also causes a fatal error, a database error would let you know.'."\n");
    flush_chunk();
  }
});

$rollback_hnd=(function() {});

foreach ($sql_files as $idx => $sql_file) {
  
  echo_ev_log($sql_file."\n");
  flush_chunk();
  
  $content=file_get_contents($sql_file);
  if ($content===false) {
    call_user_func($rollback_hnd);
    call_user_func($unknown_var_dirty_hnd);
    define('ERRMSG','Could not read file "'.$sql_file.'"');
    die;
  }
  $content=str_replace("\xef\xbb\xbf",'',substr($content,0,3)).substr($content,3);
  $sql_buffer='';
  
  $sql_file_varmap=array();
  if (preg_match_all('/^SET (@[A-Z]+_[^= ]+).*/m',$content,$preg_matches,PREG_SET_ORDER)!==false) {
    foreach ($preg_matches as $preg_match) {
      $sql_file_varmap[$preg_match[1]]=$preg_match[0];
    }
  } else {
    call_user_func($rollback_hnd);
    call_user_func($unknown_var_dirty_hnd);
    define('ERRMSG','Could not execute varmap regex in file "'.$sql_file.'"');
    die;
  }
  
  $sql_file_import_varmap=array();
  if (preg_match_all('/@[A-Z]+_[^,;\(\)=\+ ]+/',$content,$preg_matches,PREG_SET_ORDER | PREG_OFFSET_CAPTURE)!==false) {
    foreach ($preg_matches as $preg_match) {
      $varname=$preg_match[0][0];
      if (!array_key_exists($varname,$sql_file_varmap)) {
        if (array_key_exists($varname,$varmap)) {
          if (array_key_exists($varname,$sql_file_import_varmap)) {
            $sql_file_import_varmap[$varname]++;
          } else {
            $sql_file_import_varmap[$varname]=1;
          }
        } else {
          $unknown_var_dirty=true;
          echo_ev_log('Unknown variable '.$preg_match[0][0].' in file "'.$sql_file.'", line '.(1+substr_count(substr($content,0,$preg_match[0][1]),"\n"))."\n");
          flush_chunk();
        }
      }
    }
    foreach (array_keys($sql_file_import_varmap) as $import_varname) {
      $sql_buffer.=$varmap[$import_varname];
    }
  } else {
    call_user_func($rollback_hnd);
    call_user_func($unknown_var_dirty_hnd);
    define('ERRMSG','Could not execute var regex in file "'.$sql_file.'"');
    die;
  }
  
  $sql_buffer.=$content;
  
  if (!$buffer_import($sql_buffer)) {
    call_user_func($rollback_hnd);
    call_user_func($unknown_var_dirty_hnd);
    define('ERRMSG','Could not import file "'.$sql_file.'"; ['.$db_conn->errno.'] '.$db_conn->error);
    die;
  }
  
  $varmap=array_merge($varmap,$sql_file_varmap);
  
}

$unknown_var_dirty_hnd();
unset($unknown_var_dirty_hnd);
unset($unknown_var_dirty);

unset($sql_file);
unset($idx);
unset($content);
unset($sql_buffer);
unset($preg_matches);
unset($preg_match);
unset($sql_file_varmap);
unset($sql_file_import_varmap);
unset($varname);
unset($import_varname);

$mem_before_free=memory_get_usage();
unset($sql_files);
unset($varmap);

echo_ev_log('[Imported; destructing freed up '.ceil(($mem_before_free-memory_get_usage())/1024).' KB of unused memory]'."\n");
flush_chunk();
unset($mem_before_free);

define('IMPORT_SUCCESS',true);

echo_ev_log('[Done]'."\n");
flush_chunk();

?>
