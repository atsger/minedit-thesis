<?php

$schema_id=$rest->getPathArg('schema');

require_once 'mysqli.php';

$params=[];
$join_tables=[];

if ($_SERVER['CONTENT_LENGTH']>0) {
  
  require_once 'parser.php';
    
  if (defined('TEST_ENVIRONMENT') && class_exists('Parser2',false))
    goto skip_Parser2_class_definition;
  class Parser2 extends Parser {
    
    protected function get_prop_row($prop_label) {
      
      global $schema_id;
      
      $db=mnSqlConnect();
      
      $tbrw_='[^a-z0-9]';
      $name_regex='^'.$tbrw_.'*'.str_replace(
        ['a','e','i','o','u','_'],['[aá]','[eé]','[ií]','[őöóo]','[űüúu]',$tbrw_.'+'],$prop_label
      ).$tbrw_.'*$';
      
      if (
        ($stmt_resolve=$db->prepare('SELECT PROPERTY_ID, DEFAULT_UNIT'.
          ' FROM PROPERTY_POOL'.
          ' WHERE SCHEMA_ID=? AND LOWER(NAME) REGEXP ?')) &&
        $stmt_resolve->bind_param('ss',$schema_id,$name_regex) &&
        $stmt_resolve->execute() &&
        
        $result_resolve=$stmt_resolve->get_result()
      ) {
        
        $stmt_resolve->close();
        unset($stmt_resolve);
        
        $prop_row=$result_resolve->fetch_assoc();
        
        $result_resolve->free();
        unset($result_resolve);
        
        return $prop_row;
        
      } else
        trigger_error('Preparing statement failed, @resolving property label "'.$prop_label.'"',E_USER_ERROR);
      
    }
    
  }
  skip_Parser2_class_definition:
  
  $parser=new Parser2($rest->getBody());
  $cond_ptree=$parser->parse();
  
  if (defined('TEST_ENVIRONMENT')) {
    return $cond_ptree;
    die;
  }
  
  if (is_int(array_key_first($cond_ptree))) {
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($cond_ptree,JSON_NUMERIC_CHECK);
    die;
  }
  
  function parse_ptree_node(&$node) {
    $in_bracket=array_key_exists('in_bracket',$node) ? $node['in_bracket'] : false;
    return ($in_bracket ? '(' : '')._parse_ptree_node($node).($in_bracket ? ')' : '');
  }
  
  function _parse_ptree_node(&$node) {
    
    global $params, $join_tables;
    
    static $binary_sql_operators=[
      Parser::TOKEN_LESS_OR_EQUALS => '<=',
      Parser::TOKEN_LESS_THAN => '<',
      Parser::TOKEN_GREATER_OR_EQUALS => '>=',
      Parser::TOKEN_GREATER_THAN => '>',
      Parser::TOKEN_EQUALS => '=',
      Parser::TOKEN_NOT_EQUALS => '<>',
      Parser::TOKEN_AND => 'AND',
      Parser::TOKEN_OR => 'OR'
    ];
    static $attr_map=[
      Parser::TOKEN_PROP => ['need_table' => 'NATURAL LEFT JOIN DOP_PROPERTIES', 'expr' => 'PROPERTY_ID'],
      Parser::TOKEN_PARAM => ['need_table' => 'NATURAL LEFT JOIN DOP_PARAMS', 'expr' => 'PARAM_ID'],
      Parser::TOKEN_KIT => ['need_table' => 'NATURAL LEFT JOIN (SELECT PARAM_HASH AS KCP_PARAM_HASH, PARAM_OP AS KCP_PARAM_OP, KIT_SCHEMA_ID AS SCHEMA_ID, KIT_PRODUCT_ID AS PRODUCT_ID, KIT_FUNCTION_ID AS KCP_KIT_FUNCTION_ID, SCHEMA_ID AS CHILD_SCHEMA_ID, PRODUCT_ID AS CHILD_PRODUCT_ID FROM KIT_CONTENT) AS KIT_CONTENT_AS_PARENT', 'expr' => 'KIT_CONTENT_AS_PARENT.KCP_KIT_FUNCTION_ID'],
      Parser::TOKEN_CHILD => ['need_table' => 'NATURAL LEFT JOIN KF_CONTENT', 'expr' => 'KF_CONTENT.KIT_FUNCTION_ID']
    ];
    static $prop_table='NATURAL LEFT JOIN DOP_PROPERTIES';
    
    if (array_key_exists('operator',$node) && array_key_exists($node['operator'],$binary_sql_operators) && $node['operator']<Parser::TOKEN_AND) {
      if ($node['operands'][0]['type']!=Parser::TYPE_REF) {
        static $operator_swap=[
          Parser::TOKEN_LESS_OR_EQUALS => Parser::TOKEN_GREATER_OR_EQUALS,
          Parser::TOKEN_LESS_THAN => Parser::TOKEN_GREATER_THAN,
          Parser::TOKEN_GREATER_OR_EQUALS => Parser::TOKEN_LESS_OR_EQUALS,
          Parser::TOKEN_GREATER_THAN => Parser::TOKEN_LESS_THAN
        ];
        if (array_key_exists($node['operator'],$operator_swap)) {
          $node['operator']=$operator_swap[$node['operator']];
        }
        $node['operands']=array_reverse($node['operands']);
      }
      return 'SUM(IF('.parse_ptree_node($node['operands'][0]).($node['operands'][1]['type']==Parser::TYPE_NUMERIC ? ' AND !ISNULL(UNIT_MULTIPLIER) AND UNIT_MULTIPLIER*' : ' AND ').'VALUE'.$binary_sql_operators[$node['operator']].parse_ptree_node($node['operands'][1]).',1,0))>0';
      
    } elseif (array_key_exists('operator',$node) && array_key_exists($node['operator'],$binary_sql_operators) && $node['operator']>=Parser::TOKEN_AND) {
      return parse_ptree_node($node['operands'][0]).' '.$binary_sql_operators[$node['operator']].' '.parse_ptree_node($node['operands'][1]);
      
    } elseif (array_key_exists('operator',$node) && $node['operator']==Parser::TOKEN_NOT) {
      return '!'.parse_ptree_node($node['operands'][0]);
      
    } elseif (array_key_exists('operator',$node) && $node['operator']==Parser::TOKEN_HAS) {
      if (!in_array($attr_map[$node['attr']]['need_table'],$join_tables)) {
        $join_tables[]=$attr_map[$node['attr']]['need_table'];
      }
      return 'COUNT('.$attr_map[$node['attr']]['expr'].')>0';
      
    } elseif (array_key_exists('operator',$node) && $node['operator']===Parser::TOKEN_IN) {
      $set_string=[];
      $set_numeric=[];
      foreach ($node['operands'][1]['set'] as $i => $l) {
        if (is_string($l)) {
          $set_string[]=$i;
        } else {
          $set_numeric[]=$i;
        }
      }
      $params[]=['s' => $node['operands'][0]['prop_row']['PROPERTY_ID']];
      foreach ($set_string as $i) {
        $params[]=['s' => $node['operands'][1]['set'][$i]];
      }
      foreach ($set_numeric as $i) {
        $params[]=[(is_int($node['operands'][1]['set'][$i]) ? 'i' : 'd') => $node['operands'][1]['set'][$i]];
      }
      if (!in_array($prop_table,$join_tables)) {
        $join_tables[]=$prop_table;
      }
      return 'SUM(IF(PROPERTY_ID=? AND !ISNULL(VALUE) AND ('.(!empty($set_string) ? 'VALUE IN ('.implode(',',array_fill(0,count($set_string),'?')).')'.(!empty($set_numeric) ? ' OR (' : '') : '').(!empty($set_numeric) ? '!ISNULL(UNIT_MULTIPLIER) AND UNIT_MULTIPLIER*VALUE IN ('.implode(',',array_fill(0,count($set_numeric),'?')).')'.(!empty($set_string) ? ')' : '') : '').'),1,0))>0';
      
    } elseif ($node['type']==Parser::TYPE_REF) {
      
      $params[]=['s' => $node['prop_row']['PROPERTY_ID']];
      if (!in_array($prop_table,$join_tables)) {
        $join_tables[]=$prop_table;
      }
      return 'PROPERTY_ID=? AND !ISNULL(VALUE)';
      
    } elseif (array_key_exists('literal',$node)) {
      $params[]=[(is_string($node['literal']) ? 's' : (is_int($node['literal']) ? 'i' : 'd')) => $node['literal']];
      return '?';
      
    } else
      trigger_error('Expression compiling error',E_USER_ERROR);
    
  }
  
  $having=parse_ptree_node($cond_ptree);
  
}

$db=mnSqlConnect();
if (
  ($stmt_hits=$db->prepare('SELECT DISTINCT DOP.SCHEMA_ID, DOP.PRODUCT_ID'.
    ' FROM DOP NATURAL JOIN (SELECT PRODUCT_ID, CONCAT(VENDOR," ",PRODUCT_NAME) AS ORDER_EXPR FROM PRODUCT_POOL) AS ORDER_JOIN'.(!empty($join_tables) ? ' '.implode(' ',$join_tables) : '').
    ' WHERE DOP.SCHEMA_ID=?'.
    ' GROUP BY DOP.SCHEMA_ID, DOP.PRODUCT_ID'.
    (isset($having) ? ' HAVING '.$having : '').
    ' ORDER BY ORDER_EXPR')) &&
  $stmt_hits->bind_param(implode('',array_merge(['s'],array_map('array_key_first',$params))),...array_merge([$schema_id],array_map('reset',$params))) &&
  $stmt_hits->bind_result($hit_schema_id,$hit_product_id) &&
  $stmt_hits->execute()
) {
  
  $hit_product_ids=[];
  
  while ($stmt_hits->fetch()) {
    $hit_product_ids[]=$hit_product_id;
  }
  $stmt_hits->close();
  unset($stmt_hits);
  
  $hits=[];
  
  function get_p($schema_id,$product_id) {
    
    global $db;
    
    if (
      ($stmt_head=$db->prepare('SELECT *'.
        ' FROM DOP NATURAL JOIN PRODUCT_POOL'.
        ' WHERE SCHEMA_ID=? AND PRODUCT_ID=?')) &&
      $stmt_head->bind_param('ss',$schema_id,$product_id) &&
      $stmt_head->execute() &&
      
      $result_head=$stmt_head->get_result()
    ) {
      
      $stmt_head->close();
      unset($stmt_head);
      
      $head_row=$result_head->fetch_assoc();
      $result_head->free();
      unset($result_head);
      
    } else
      trigger_error('Preparing statement failed, @reading product head row of "'.$product_id.'"',E_USER_ERROR);
    
    if (
      ($stmt_props=$db->prepare('SELECT PROPERTY_ID, VALUE, CONCAT_PATTERN'.
        ' FROM DOP_PROPERTIES NATURAL JOIN PROPERTY_POOL'.
        ' WHERE SCHEMA_ID=? AND PRODUCT_ID=? AND PARAM_HASH=""'.
        ' ORDER BY PROPERTY_POOL.NAME')) &&
      $stmt_props->bind_param('ss',$schema_id,$product_id) &&
      $stmt_props->bind_result($prop_id,$value,$concat_pattern) &&
      $stmt_props->execute()
    ) {
      
      $props=[];
      
      while ($stmt_props->fetch()) {
        $props[$prop_id]=array(
          'value'                 =>  $value,
          'concat_pattern'        =>  $concat_pattern,
        );
      }
      $stmt_props->close();
      unset($stmt_props);
      
      unset($prop_id);
      unset($value);
      unset($concat_pattern);
      
    } else
      trigger_error('Preparing statement failed, @collecting properties of "'.$product_id.'"',E_USER_ERROR);
    
    if (
      ($stmt_instrs=$db->prepare('SELECT SORT_INDEX, INSTRUCTION'.
        ' FROM DOP_INSTRUCTIONS'.
        ' WHERE SCHEMA_ID=? AND PRODUCT_ID=? AND PARAM_HASH=""'.
        ' ORDER BY SORT_INDEX')) &&
      $stmt_instrs->bind_param('ss',$schema_id,$product_id) &&
      $stmt_instrs->bind_result($instr_idx,$instr) &&
      $stmt_instrs->execute()
    ) {
      
      $instrs=[];
      
      while ($stmt_instrs->fetch()) {
        $instrs[]=array(
          'idx'                   =>  $instr_idx,
          'instr'                 =>  $instr
        );
      }
      $stmt_instrs->close();
      unset($stmt_instrs);
      
      unset($instr_idx);
      unset($instr);
      
    } else
      trigger_error('Preparing statement failed, @collecting instructions of "'.$product_id.'"',E_USER_ERROR);
    
    static $recursion_lvl1=true;
    if ($recursion_lvl1) {
      
      if (
        ($stmt_kit_content=$db->prepare('SELECT KIT_FUNCTION_ID, CONCAT(SCHEMA_ID,".",PRODUCT_ID) AS CHILD_REF'.
          ' FROM KIT_CONTENT'.
          ' WHERE KIT_SCHEMA_ID=? AND KIT_PRODUCT_ID=? AND PARAM_HASH=""')) &&
        $stmt_kit_content->bind_param('ss',$schema_id,$product_id) &&
        $stmt_kit_content->bind_result($kit_function_id,$child_ref) &&
        $stmt_kit_content->execute()
      ) {
        
        $kit_map=[];
        
        while ($stmt_kit_content->fetch()) {
          $kit_map[$kit_function_id]=$child_ref;
        }
        $stmt_kit_content->close();
        unset($stmt_kit_content);
        
        unset($kit_function_id);
        
      } else
        trigger_error('Preparing statement failed, @collecting kit content of "'.$product_id.'"',E_USER_ERROR);
      
      static $where_to_find_refp_map=[];
      
      if (
        ($stmt_kf_content=$db->prepare('SELECT DISTINCT SCHEMA_ID, PRODUCT_ID'.
          ' FROM KF_CONTENT'.
          ' WHERE KIT_VENDOR=? AND KIT_SCHEMA_ID=?')) &&
        $stmt_kf_content->bind_param('ss',$head_row['VENDOR'],$schema_id) &&
        $stmt_kf_content->execute() &&
        
        $result_kf_content=$stmt_kf_content->get_result()
      ) {
        
        $stmt_kf_content->close();
        unset($stmt_kf_content);
        
        $refp_pool=[];
        
        while ($kf_content_row=$result_kf_content->fetch_assoc()) {
          
          $child_ref=$kf_content_row['SCHEMA_ID'].'.'.$kf_content_row['PRODUCT_ID'];
          
          if (array_key_exists($child_ref,$where_to_find_refp_map)) {
            $refp_pool[$child_ref]=$where_to_find_refp_map[$child_ref];
            
          } else {
            $recursion_lvl1=false;
            $refp_pool[$child_ref]=get_p($kf_content_row['SCHEMA_ID'],$kf_content_row['PRODUCT_ID']);
            $where_to_find_refp_map[$child_ref]=['id' => $child_ref, 'found_in' => $schema_id.'.'.$product_id];
            $recursion_lvl1=true;
            
          }
          
        }
        unset($kf_content_row);
        
        $result_kf_content->free();
        unset($result_kf_content);
        
        unset($child_ref);
        
      } else
        trigger_error('Preparing statement failed, @collecting kf content of "'.$product_id.'"',E_USER_ERROR);
      
      if (
        ($stmt_param_hashes=$db->prepare('SELECT DISTINCT PARAM_HASH'.
          ' FROM DOP_PROPERTIES'.
          ' WHERE SCHEMA_ID=? AND PRODUCT_ID=? AND PARAM_HASH<>""'.
          ' UNION DISTINCT '.            'SELECT DISTINCT PARAM_HASH'.
          ' FROM DOP_INSTRUCTIONS'.
          ' WHERE SCHEMA_ID=? AND PRODUCT_ID=? AND PARAM_HASH<>""'.
          ' UNION DISTINCT '.            'SELECT DISTINCT PARAM_HASH'.
          ' FROM KIT_CONTENT'.
          ' WHERE KIT_SCHEMA_ID=? AND KIT_PRODUCT_ID=? AND PARAM_HASH<>""')) &&
        $stmt_param_hashes->bind_param('ssssss',$schema_id,$product_id,$schema_id,$product_id,$schema_id,$product_id) &&
        $stmt_param_hashes->bind_result($param_hash) &&
        $stmt_param_hashes->execute()
      ) {
        
        $param_override_map=[];
        
        while ($stmt_param_hashes->fetch()) {
          $param_override_map[$param_hash]=[];
        }
        $stmt_param_hashes->close();
        unset($stmt_param_hashes);
        
        if (!empty($param_override_map)) {
          
          if (
            ($stmt_param_props=$db->prepare('SELECT PARAM_HASH, PARAM_OP, PROPERTY_ID, VALUE, CONCAT_PATTERN'.
              ' FROM DOP_PROPERTIES'.
              ' WHERE SCHEMA_ID=? AND PRODUCT_ID=? AND PARAM_HASH<>""')) &&
            $stmt_param_props->bind_param('ss',$schema_id,$product_id) &&
            $stmt_param_props->bind_result($param_hash,$param_op,$prop_id,$value,$concat_pattern) &&
            $stmt_param_props->execute()
          ) {
            
            while ($stmt_param_props->fetch()) {
              $param_override_map[$param_hash][]=array(
                'type'                  =>  'prop',
                'operation'             =>  $param_op,
                'prop_id'               =>  $prop_id,
                'value'                 =>  $value,
                'concat_pattern'        =>  $concat_pattern
              );
            }
            $stmt_param_props->close();
            unset($stmt_param_props);
            
            unset($prop_id);
            unset($value);
            unset($concat_pattern);
            
          } else
            trigger_error('Preparing statement failed, @collecting param-properties of "'.$product_id.'"',E_USER_ERROR);
          
          if (
            ($stmt_param_instrs=$db->prepare('SELECT PARAM_HASH, PARAM_OP, SORT_INDEX, INSTRUCTION'.
              ' FROM DOP_INSTRUCTIONS'.
              ' WHERE SCHEMA_ID=? AND PRODUCT_ID=? AND PARAM_HASH<>""'.
              ' ORDER BY SORT_INDEX')) &&
            $stmt_param_instrs->bind_param('ss',$schema_id,$product_id) &&
            $stmt_param_instrs->bind_result($param_hash,$param_op,$instr_idx,$instr) &&
            $stmt_param_instrs->execute()
          ) {
            
            while ($stmt_param_instrs->fetch()) {
              $param_override_map[$param_hash][]=array(
                'type'                  =>  'instr',
                'operation'             =>  $param_op,
                'idx'                   =>  $instr_idx,
                'instr'                 =>  $instr
              );
            }
            $stmt_param_instrs->close();
            unset($stmt_param_instrs);
            
            unset($instr_idx);
            unset($instr);
            
          } else
            trigger_error('Preparing statement failed, @collecting param-instructions of "'.$product_id.'"',E_USER_ERROR);
          
          if (
            ($stmt_param_kit_content=$db->prepare('SELECT PARAM_HASH, PARAM_OP, KIT_FUNCTION_ID, CONCAT(SCHEMA_ID,".",PRODUCT_ID) AS CHILD_REF'.
              ' FROM KIT_CONTENT'.
              ' WHERE KIT_SCHEMA_ID=? AND KIT_PRODUCT_ID=? AND PARAM_HASH<>""')) &&
            $stmt_param_kit_content->bind_param('ss',$schema_id,$product_id) &&
            $stmt_param_kit_content->bind_result($param_hash,$param_op,$kit_function_id,$child_ref) &&
            $stmt_param_kit_content->execute()
          ) {
            
            while ($stmt_param_kit_content->fetch()) {
              $param_override_map[$param_hash][]=array(
                'type'                  =>  'kit',
                'operation'             =>  $param_op,
                'function'              =>  $kit_function_id,
                'refp_id'               =>  $child_ref
              );
            }
            $stmt_param_kit_content->close();
            unset($stmt_param_kit_content);
            
            unset($kit_function_id);
            unset($child_ref);
            
          } else
            trigger_error('Preparing statement failed, @collecting kit param-content of "'.$product_id.'"',E_USER_ERROR);
          
          unset($param_op);
          
        }
        
        unset($param_hash);
        
      } else
        trigger_error('Preparing statement failed, @collecting param hashes of "'.$product_id.'"',E_USER_ERROR);
      
    } else {
      $kit_map=[];
      $refp_pool=[];
      $param_override_map=[];
    }
    
    return array(
      'id'                    =>  $schema_id.'.'.$product_id,
      'label'                 =>  $head_row['VENDOR'].' '.$head_row['PRODUCT_NAME'],
      'desc'                  =>  $head_row['DESCRIPTION'],
      'props_raw'             =>  $props,
      'instrs_raw'            =>  $instrs,
      'link'                  =>  $head_row['LINK'],
      'doc_href'              =>  $head_row['DOC_LINK'],
      'param_override_raw'    =>  $param_override_map,
      'kit'                   =>  $kit_map,
      'kf_id'                 =>  $head_row['VENDOR'],
      'refp_pool_raw'         =>  $refp_pool
    );
    
  }
  
  foreach ($hit_product_ids as $hit_product_id) {
    $hits[]=get_p($schema_id,$hit_product_id);
  }
  
} else
  trigger_error('Preparing statement failed, @collecting hit ids',E_USER_ERROR);

header('Content-Type: application/json; charset=utf-8');
echo json_encode($hits,JSON_NUMERIC_CHECK);

?>
