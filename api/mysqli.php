<?php

function mnSqlConnect() {
  static $db=null;
  if (!is_null($db)) {
    return $db;
  } else {
    
    $get_conf=(function($key, $default=null) {
      static $config=null;
      if (is_null($config)) {
        $config=json_decode(preg_replace('/# .*/','',file_get_contents('.conf')),true);
      }
      return array_key_exists($key,$config) ? $config[$key] : $default;
    });
    
    $db=new mysqli(
      $get_conf('mysqli_address'),
      $get_conf('mysqli_user'),
      $get_conf('mysqli_password'),
      $get_conf('mysqli_db')
    );
    
    if ($db->connect_error)
      trigger_error('Could not connect to MySQL server, error: '.$db->connect_errno.' '.$db->connect_error,E_USER_ERROR);
    if (!$db->set_charset('utf8'))
      trigger_error('Could not set mysqli charset, error: '.$db->errno.' '.$db->error,E_USER_ERROR);
    
    return $db;
    
  }
}

?>
