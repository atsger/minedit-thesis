<?php

abstract class Parser {
  
  public const TOKEN_ERR=-0xff;
  public const TOKEN_EOF=-0xf0;
  public const TOKEN_WHITESPACE=-0x01;
  
  public const TOKEN_BRACKET_OPEN=0x10;
  public const TOKEN_BRACKET_CLOSE=0x11;
  public const TOKEN_COMMA=0x12;
  
  public const TOKEN_NOT=0x20;
  
  public const TOKEN_LESS_OR_EQUALS=0x30;
  public const TOKEN_LESS_THAN=0x31;
  public const TOKEN_GREATER_OR_EQUALS=0x32;
  public const TOKEN_GREATER_THAN=0x33;
  public const TOKEN_EQUALS=0x40;
  public const TOKEN_NOT_EQUALS=0x41;
  public const TOKEN_IN=0x42;
  
  public const TOKEN_AND=0x50;
  public const TOKEN_OR=0x60;
  
  public const TOKEN_HAS=0x70;
  public const TOKEN_ANY=0x71;
  public const TOKEN_IS=0x72;
  public const TOKEN_PROP=0x8c;
  public const TOKEN_PARAM=0x8d;
  public const TOKEN_KIT=0x8e;
  public const TOKEN_CHILD=0x8f;
  
  public const TOKEN_SCOPE=0xf5;
  public const TOKEN_LITERAL=0xfe;
  public const TOKEN_LABEL=0xff;
  
  public const TYPE_BOOL=1;
  public const TYPE_NUMERIC=2;
  public const TYPE_STRING=3;
  public const TYPE_REF=4;
  public const TYPE_SET=5;
  
  private $errors=[];
  private $found_illegal_expression=false;
  
  private $str;
  
  private $preread_stack=[];
  private $preread_cache=[];
  
  private $pos=0;
  private $token_data;
  private $token_pos;
  
  public function __construct($str) {
    
    $this->str=$str;
    
  }
  
  protected function nextToken() {
    
    static $regex_map=[
      self::TOKEN_WHITESPACE => '\\s+',
      self::TOKEN_BRACKET_OPEN => '\\(',
      self::TOKEN_BRACKET_CLOSE => '\\)',
      self::TOKEN_COMMA => ',',
      self::TOKEN_NOT_EQUALS => '!=',
      self::TOKEN_NOT => '!',
      self::TOKEN_LESS_OR_EQUALS => '<=',
      self::TOKEN_LESS_THAN => '<',
      self::TOKEN_GREATER_OR_EQUALS => '>=',
      self::TOKEN_GREATER_THAN => '>',
      self::TOKEN_EQUALS => '==',
      self::TOKEN_IN => 'in',
      self::TOKEN_AND => '&&',
      self::TOKEN_OR => '\\|\\|',
      self::TOKEN_HAS => 'has\\b',
      self::TOKEN_ANY => 'any\\b',
      self::TOKEN_IS => 'is\\b',
      self::TOKEN_PROP => 'prop\\b',
      self::TOKEN_PARAM => 'param\\b',
      self::TOKEN_KIT => 'kit\\b',
      self::TOKEN_CHILD => 'child\\b',
      self::TOKEN_SCOPE => '::',
      self::TOKEN_LITERAL => '"([^"]*)"|((?:-?[1-9][0-9]*|0)(?:\\.[0-9]+)?)',
      self::TOKEN_LABEL => '[a-z0-9_]+'
    ];
    
    if (array_key_exists($this->pos,$this->preread_cache)) {
      $bak=$this->preread_cache[$this->pos];
      $this->pos=$bak[0];
      $this->token_data=$bak[1];
      $this->token_pos=$bak[2];
      return $bak[3];
    }
    
    if (count($this->preread_stack)>0) {
      $c_at_pos=$this->pos;
    }
    
    $poscheck_made=false;
    foreach ($regex_map as $token => $regex) {
      
      if (!$poscheck_made) {
        if ($this->pos==strlen($this->str)) {
          $this->token_data=null;
          $this->token_pos=[$this->pos,$this->pos];
          if (count($this->preread_stack)>0) {
            $this->preread_cache[$c_at_pos]=[$this->pos,$this->token_data,$this->token_pos,self::TOKEN_EOF];
          }
          return self::TOKEN_EOF;
        }
        $poscheck_made=true;
      }
      
      if (preg_match('/'.$regex.'/',$this->str,$match,PREG_OFFSET_CAPTURE,$this->pos)===1 && $match[0][1]==$this->pos) {
        
        if ($token==self::TOKEN_LITERAL) {
          $this->token_data=$match[1][1]<0 ? strpos($match[2][0],'.')===false ? intval($match[2][0]) : floatval($match[2][0]) : $match[1][0];
        } elseif ($token==self::TOKEN_LABEL) {
          $this->token_data=$match[0][0];
        } else {
          $this->token_data=null;
        }
        $this->token_pos=[$this->pos,$this->pos+=strlen($match[0][0])];
        
        if ($token==self::TOKEN_WHITESPACE) {
          $poscheck_made=false;
          continue;
        }
        if (count($this->preread_stack)>0) {
          $this->preread_cache[$c_at_pos]=[$this->pos,$this->token_data,$this->token_pos,$token];
        }
        return $token;
      }
    }
    
    $this->token_data=null;
    $this->token_pos=[$this->pos,$this->pos];
    if (count($this->preread_stack)>0) {
      $this->preread_cache[$c_at_pos]=[$this->pos,$this->token_data,$this->token_pos,self::TOKEN_ERR];
    }
    return self::TOKEN_ERR;
    
  }
  
  protected function addErrorAtPosition($e_or_pos,$e=null) {
    $this->addError($this->pos_to_string(is_null($e) ? $this->token_pos[0] : $e_or_pos).' '.(is_null($e) ? $e_or_pos : $e));
  }
  
  protected function addError($e) {
    if ($e=='Illegal expression') {
      if ($this->found_illegal_expression) {
        return;
      } else {
        $this->found_illegal_expression=true;
      }
    }
    $this->errors[]=$e;
  }
  
  protected function pos_to_string($pos) {
    $str=&$this->str;
    return ':'.(is_array($pos) ? implode('-',array_map(function($p) use ($str) {
      return mb_strlen(substr($str,0,$p));
    },$pos)) : mb_strlen(substr($this->str,0,$pos)));
  }
  
  protected function operand_type_hr(&$operand) {
    
    static $type_hr_map=[
      self::TYPE_BOOL => 'boolean',
      self::TYPE_NUMERIC => 'numeric',
      self::TYPE_STRING => 'string',
      self::TYPE_REF => 'reference',
      self::TYPE_SET => 'set'
    ];
    
    return $type_hr_map[$operand['type']].' '.$this->pos_to_string($operand['pos']);
    
  }
  
  protected function operator_hr($operator) {
    
    static $hr_map=[
      self::TOKEN_LESS_OR_EQUALS => '<=',
      self::TOKEN_LESS_THAN => '<',
      self::TOKEN_GREATER_OR_EQUALS => '>=',
      self::TOKEN_GREATER_THAN => '>',
      self::TOKEN_EQUALS => '==',
      self::TOKEN_NOT_EQUALS => '!=',
      self::TOKEN_IN => 'in',
      self::TOKEN_AND => '&&',
      self::TOKEN_OR => '||'
    ];
    
    return $hr_map[$operator];
  }
  
  protected function pushPos() {
    $this->preread_stack[]=[$this->pos,$this->token_data,$this->token_pos];
  }
  
  protected function popPos() {
    $bak=array_pop($this->preread_stack);
    $this->pos=$bak[0];
    $this->token_data=$bak[1];
    $this->token_pos=$bak[2];
  }
  
  protected function commitPos() {
    array_pop($this->preread_stack);
  }
  
  abstract protected function get_prop_row($prop_label);
  
  private function symbol_E() {
    $acc_E=$this->symbol_E_single();
    
    if (!$acc_E) {
      $this->addError('Illegal expression');
      return;
    }
    
    do {
      $this->pushPos();
      $operator=$this->nextToken();
      if ($operator<self::TOKEN_LESS_OR_EQUALS || $operator>self::TOKEN_OR) {
        break;
      }
      $operator_pos=$this->token_pos[0];
      $this->commitPos();
      $right_operand=$this->symbol_E_single();
      
      if (!$right_operand) {
        $this->addError('Illegal expression');
        return;
      }
      
      $insert_right_of=null;
      for ($left_E=&$acc_E; array_key_exists('operator',$left_E) && array_key_exists('operands',$left_E) && (($left_E['operator'] & 0xf0)>($operator & 0xf0) && !array_key_exists('in_bracket',$left_E)); $left_E=&$left_E['operands'][count($left_E['operands'])-1]) {
        $insert_right_of=&$left_E;
      }
      
      if (!is_null($insert_right_of)) {
        $insert_right_of['operands'][]=[
          'operator' => $operator,
          'operands' => [array_pop($insert_right_of['operands']),$right_operand],
          'pos_op' => $operator_pos
        ];
      } else {
        $acc_E=[
          'operator' => $operator,
          'operands' => [$acc_E,$right_operand],
          'pos_op' => $operator_pos
        ];
      }
      
      unset($insert_right_of);
    } while (true);
    $this->popPos();
    
    for ($traverse_stack=[&$acc_E]; !empty($traverse_stack);) {
      $current=&$traverse_stack[count($traverse_stack)-1];
      if (array_key_exists('operator',$current) && array_key_exists('operands',$current) && !array_key_exists('type',$current)) {
        $dirty_mark=false;
        foreach ($current['operands'] as &$operand) {
          if (!array_key_exists('type',$operand)) {
            $traverse_stack[]=&$operand;
            $dirty_mark=true;
          }
        }
        unset($operand);
        
        if (!$dirty_mark) {
          
          if (!defined('TEST_ENVIRONMENT') || defined('TEST_MODE_RETURN_PARSE_TREE_SEMANTICS')) {
            if ($current['operator']>=self::TOKEN_LESS_OR_EQUALS && $current['operator']<=self::TOKEN_NOT_EQUALS
                && count(array_filter($current['operands'],function($operand) { return $operand['type']==self::TYPE_REF; }))==0) {
              $this->addErrorAtPosition($current['pos_op'],'Relational operator '."'".$this->operator_hr($current['operator'])."'".' requires one of the operands to be reference type (got '.$this->operand_type_hr($current['operands'][0]).' and '.$this->operand_type_hr($current['operands'][1]).')');
              return;
            }
            if ($current['operator']>=self::TOKEN_LESS_OR_EQUALS && $current['operator']<=self::TOKEN_GREATER_THAN
                && count(array_filter($current['operands'],function($operand) { return $operand['type']==self::TYPE_NUMERIC; }))==0) {
              $this->addErrorAtPosition($current['pos_op'],'Relational operator '."'".$this->operator_hr($current['operator'])."'".' requires one of the operands to be numeric type (got '.$this->operand_type_hr($current['operands'][0]).' and '.$this->operand_type_hr($current['operands'][1]).')');
              return;
            }
            if ($current['operator']>=self::TOKEN_EQUALS && $current['operator']<=self::TOKEN_NOT_EQUALS
                && count(array_filter($current['operands'],function($operand) { return $operand['type']==self::TYPE_NUMERIC || $operand['type']==self::TYPE_STRING; }))==0) {
              $this->addErrorAtPosition($current['pos_op'],'Relational operator '."'".$this->operator_hr($current['operator'])."'".' requires one of the operands to be numeric or string type (got '.$this->operand_type_hr($current['operands'][0]).' and '.$this->operand_type_hr($current['operands'][1]).')');
              return;
            }
            if ($current['operator']>=self::TOKEN_LESS_OR_EQUALS && $current['operator']<=self::TOKEN_NOT_EQUALS
                && ($current['operator']<self::TOKEN_EQUALS || count(array_filter($current['operands'],function($operand) { return $operand['type']==self::TYPE_NUMERIC; }))>0) && count(array_filter($current['operands'],function($operand) { return $operand['type']==self::TYPE_REF && !is_null($operand['prop_row']['DEFAULT_UNIT']); }))==0) {
              $this->addErrorAtPosition($current['pos_op'],'Relational operator '."'".$this->operator_hr($current['operator'])."'".' requires its reference type operand '.$this->pos_to_string($current['operands'][$current['operands'][0]['type']==self::TYPE_REF ? 0 : 1]['pos']).' to have default unit if it'."'s".' to be compared to the numeric type other operand');
              return;
            }
            if ($current['operator']==self::TOKEN_IN
                && $current['operands'][0]['type']!=self::TYPE_REF) {
              $this->addErrorAtPosition($current['pos_op'],'Set membership operator '."'".$this->operator_hr($current['operator'])."'".' requires its left operand to be reference type (got '.$this->operand_type_hr($current['operands'][0]).')');
              return;
            }
            if ($current['operator']==self::TOKEN_IN
                && $current['operands'][1]['type']!=self::TYPE_SET) {
              if (array_key_exists('literal',$current['operands'][1]) && array_key_exists('in_bracket',$current['operands'][1])) {
                $current['operands'][1]=array(
                  'type' => self::TYPE_SET,
                  'pos' => $current['operands'][1]['pos'],
                  'set' => [$current['operands'][1]['literal']]
                );
              } else {
                $this->addErrorAtPosition($current['pos_op'],'Set membership operator '."'".$this->operator_hr($current['operator'])."'".' requires its right operand to be set type (got '.$this->operand_type_hr($current['operands'][1]).')');
                return;
              }
            }
            if ($current['operator']==self::TOKEN_IN
                && count(array_filter($current['operands'][1]['set'],function($set_literal) { return is_int($set_literal) || is_float($set_literal); }))>0 && is_null($current['operands'][0]['prop_row']['DEFAULT_UNIT'])) {
              $this->addErrorAtPosition($current['pos_op'],'Set membership operator '."'".$this->operator_hr($current['operator'])."'".' requires its reference type operand '.$this->pos_to_string($current['operands'][0]['pos']).' to have default unit if it'."'s".' to be compared to any numeric type literal in the set');
              return;
            }
            if (($current['operator']==self::TOKEN_AND || $current['operator']==self::TOKEN_OR)
                && count(array_filter($current['operands'],function($operand) { return $operand['type']!=self::TYPE_BOOL; }))>0) {
              $this->addErrorAtPosition($current['pos_op'],'Logical operator '."'".$this->operator_hr($current['operator'])."'".' requires both of the operands to be boolean type (got '.$this->operand_type_hr($current['operands'][0]).' and '.$this->operand_type_hr($current['operands'][1]).')');
              return;
            }
          }
          
          $current['type']=self::TYPE_BOOL;
          $current['pos']=[$current['operands'][0]['pos'][0],$current['operands'][1]['pos'][1]];
          array_pop($traverse_stack);
        }
      } else {
        array_pop($traverse_stack);
      }
    }
    
    return $acc_E;
    
  }
  
  private function symbol_E_single() {
    $token=$this->nextToken();
    
    if ($token==self::TOKEN_BRACKET_OPEN) {
      $bracket_open_pos=$this->token_pos[0];
      
      $this->pushPos();
      if ($this->nextToken()==self::TOKEN_LITERAL && (($first_literal_token_data=$this->token_data) || true) && $this->nextToken()==self::TOKEN_COMMA) {
        $this->commitPos();
        
        $literals=[$first_literal_token_data];
        unset($first_literal_token_data);
        
        do {
          
          if ($this->nextToken()!=self::TOKEN_LITERAL) {
            $this->addErrorAtPosition('Expected literal not found');
            return;
          }
          
          $literals[]=$this->token_data;
          $after_literal=$this->nextToken();
        } while ($after_literal==self::TOKEN_COMMA);
        
        if ($after_literal!=self::TOKEN_BRACKET_CLOSE) {
          $this->addErrorAtPosition('Set is neither closed nor continued with new members');
          return;
        }
        
        return [
          'type' => self::TYPE_SET,
          'pos' => [$bracket_open_pos,$this->token_pos[1]],
          'set' => $literals
        ];
        
      } else {
        $this->popPos();
        $inside_E=$this->symbol_E();
        
        if (!$inside_E) {
          return;
        }
        if ($this->nextToken()!=self::TOKEN_BRACKET_CLOSE) {
          $this->addErrorAtPosition('Unclosed parenthesis');
          return;
        }
        
        $inside_E['pos'][0]=$bracket_open_pos;
        $inside_E['pos'][1]=$this->token_pos[1];
        $inside_E['in_bracket']=true;
        
        return $inside_E;
        
      }
      
    } elseif ($token==self::TOKEN_NOT) {
      $not_pos=$this->token_pos[0];
      $operand=$this->symbol_E_single();
      
      if (!$operand) {
        return;
      }
      if ((!defined('TEST_ENVIRONMENT') || defined('TEST_MODE_RETURN_PARSE_TREE_SEMANTICS')) && $operand['type']!=self::TYPE_BOOL) {
        $this->addErrorAtPosition($not_pos,'Logical operator '."'!'".' requires its operand to be boolean type (got '.$this->operand_type_hr($operand).')');
        return;
      }
      
      return [
        'type' => self::TYPE_BOOL,
        'pos' => [$not_pos,$operand['pos'][1]],
        'operator' => self::TOKEN_NOT,
        'operands' => [$operand]
      ];
      
    } elseif ($token==self::TOKEN_HAS) {
      $has_pos=$this->token_pos[0];
      
      if ($this->nextToken()!=self::TOKEN_ANY) {
        $this->addErrorAtPosition('Expected '."'any'".' not found');
        return;
      }
      $attr_token=$this->nextToken();
      if ($attr_token!=self::TOKEN_PROP && $attr_token!=self::TOKEN_PARAM) {
        $this->addErrorAtPosition('Expected attribute '."'prop' or 'param'".' not found');
        return;
      }
      
      return [
        'type' => self::TYPE_BOOL,
        'pos' => [$has_pos,$this->token_pos[1]],
        'operator' => self::TOKEN_HAS,
        'attr' => $attr_token
      ];
      
    } elseif ($token==self::TOKEN_IS) {
      $is_pos=$this->token_pos[0];
      
      if ($this->nextToken()!=self::TOKEN_KIT) {
        $this->addErrorAtPosition('Expected attribute '."'kit'".' not found');
        return;
      }
      
      $this->pushPos();
      if ($this->nextToken()==self::TOKEN_CHILD) {
        $this->commitPos();
        
        return [
          'type' => self::TYPE_BOOL,
          'pos' => [$is_pos,$this->token_pos[1]],
          'operator' => self::TOKEN_HAS,
          'attr' => self::TOKEN_CHILD
        ];
        
      } else {
        $this->popPos();
        
        return [
          'type' => self::TYPE_BOOL,
          'pos' => [$is_pos,$this->token_pos[1]],
          'operator' => self::TOKEN_HAS,
          'attr' => self::TOKEN_KIT
        ];
        
      }
      
    } elseif ($token==self::TOKEN_PROP) {
      $prop_pos=$this->token_pos[0];
      
      if ($this->nextToken()!=self::TOKEN_SCOPE) {
        $this->addErrorAtPosition('Expected scope token '."'::'".' not found');
        return;
      }
      if ($this->nextToken()!=self::TOKEN_LABEL) {
        $this->addErrorAtPosition('Expected property label not found');
        return;
      }
      
      $prop_row=(!defined('TEST_ENVIRONMENT') || defined('TEST_MODE_RETURN_PARSE_TREE_SEMANTICS')) ? $this->get_prop_row($this->token_data) : '&'.$this->token_data;
      if (!$prop_row) {
        $this->addErrorAtPosition('Property label '."'".$this->token_data."'".' could not be resolved');
        return;
      }
      
      return [
        'type' => self::TYPE_REF,
        'pos' => [$prop_pos,$this->token_pos[1]],
        'prop_row' => $prop_row
      ];
      
    } elseif ($token==self::TOKEN_LITERAL) {
      
      if (!is_string($this->token_data) && !is_int($this->token_data) && !is_float($this->token_data)) {
        return;
      }
      
      return [
        'type' => is_string($this->token_data) ? self::TYPE_STRING : self::TYPE_NUMERIC,
        'pos' => $this->token_pos,
        'literal' => $this->token_data
      ];
      
    }
    
  }
  
  public function parse() {
    
    if (defined('TEST_MODE_RETURN_TOKEN_ARRAY') && !defined('TEST_MODE_RETURN_PARSE_TREE_SYNTAX')) {
      $tokens=[];
      do {
        $tokens[]=($token=$this->nextToken());
      } while ($token>0);
      return $tokens;
    }
    
    $root=$this->symbol_E();
    if (!$root) { } elseif ($this->nextToken()!=self::TOKEN_EOF) {
      $this->addErrorAtPosition('Unexpected token, root expression ends prematurely');
    } elseif ((!defined('TEST_ENVIRONMENT') || defined('TEST_MODE_RETURN_PARSE_TREE_SEMANTICS')) && $root['type']!=self::TYPE_BOOL) {
      $this->addError('Root expression must be boolean type (got '.$this->operand_type_hr($root).')');
    }
    
    if (count($this->errors)>0) {
      return $this->errors;
    } else {
      return $root;
    }
    
  }
  
}

?>
