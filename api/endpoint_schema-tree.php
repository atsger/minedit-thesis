<?php

require_once 'mysqli.php';

$db=mnSqlConnect();

function tree_walk($node_schema_id=null) {
  $node_is_root=is_null($node_schema_id);
  
  global $db;
  
  if (
    ($stmt_children=$db->prepare('SELECT SCHEMA_ID, NAME'.
      ' FROM SCHEMA_POOL'.
      ' WHERE '.($node_is_root ? 'PARENT_SCHEMA IS NULL' : 'PARENT_SCHEMA=?').
      ' ORDER BY NAME;')) &&
    
    (!$node_is_root
      ? $stmt_children->bind_param('s',$node_schema_id)
      : true) &&
    
    $stmt_children->bind_result($schema_id,$name) &&
    $stmt_children->execute()
  ) {
    
    $children=[];
    
    while ($stmt_children->fetch()) {
      $children[]=['id' => $schema_id, 'name' => $name];
    }
    $stmt_children->close();
    unset($stmt_children);
    
    foreach ($children as &$child) {
      $child['children']=tree_walk($child['id']);
      if (count($child['children'])==0) {
        unset($child['children']);
      }
    }
    
  } else
    trigger_error('Preparing statement failed, @collecting child nodes of '.($node_is_root ? 'root' : '"'.$node_schema_id.'"'),E_USER_ERROR);
  
  return $children;
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode(tree_walk(),JSON_NUMERIC_CHECK);

?>
