<?php

class RestProxy {
  
  public function __construct($base='/') {
    $this->base=$base;
  }
  
  private $base;
  
  private function pattern_match($method_pattern,$path_pattern) {
    
    $this->args=array();
    
    if ($_SERVER['REQUEST_METHOD']!=$method_pattern) {
      return false;
    }
    
    $args=array(0);
    if (preg_match('/^'.preg_replace_callback('/\{([^\}]+)\}/',function($matches) use (&$args) {
      if (substr($matches[1],-1)=='?') {
        $args[]=substr($matches[1],0,-1);
        return '([^\/]*)';
      } else {
        $args[]=$matches[1];
        return '([^\/]+)';
      }
    },str_replace('*','.*',str_replace(array('/','.'),array('\/','\.'),$this->base.$path_pattern))).'$/',$_SERVER['REDIRECT_URL'],$matches)==1) {
      $this->args=array_combine($args,$matches);
      return true;
    } else {
      return false;
    }
    
  }
  
  public function post($path_pattern) {
    return $this->pattern_match('POST',$path_pattern);
  }
  
  public function get($path_pattern) {
    return $this->pattern_match('GET',$path_pattern);
  }
  
  private $args;
  
  public function getPathArg($k) {
    return array_key_exists($k,$this->args) ? $this->args[$k] : null;
  }
  
  public function getBody() {
    return file_get_contents('php://input');
  }
  
  public function requestAuth() {
    
    $accept_if_in=
      array_map(function($line) {
        return substr($line,0,28);
      },array_filter(file('.admin-access',FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES),function($line) {
        return preg_match('/^[A-Za-z0-9+\\/]{27}=\\[(\\d+)?-(\\d+)?\\]$/',$line,$matches)===1
          && (array_key_exists(1,$matches) ? time()>=$matches[1] : true)
          && (array_key_exists(2,$matches) ? time()<$matches[2] : true);
      }));
    
    if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW']) || $accept_if_in===false || !in_array(base64_encode(sha1($_SERVER['PHP_AUTH_PW'],true)),$accept_if_in)) {
      
      header('WWW-Authenticate: Basic realm="Authentication required"');
      header($_SERVER['SERVER_PROTOCOL'].' 401 Unauthorized');
      
      echo '<h1>Unauthorized access denied</h1>';
      return false;
      
    } else {
      return true;
      
    }
    
  }
  
  public function response_invalid($message='Invalid request',$status_code=400,$body='') {
    header($_SERVER['SERVER_PROTOCOL'].' '.$status_code.' '.$message);
    if (strlen($body)>0) {
      echo $body;
    }
  }
  
}

$rest=new RestProxy(pathinfo($_SERVER['PHP_SELF'],PATHINFO_DIRNAME));
set_error_handler(function() {
  
  header($_SERVER['SERVER_PROTOCOL'].' 500 Internal Server Error');
  return false;
  
},E_USER_ERROR);

if ($rest->get('/schema/tree')) {
  
  require 'endpoint_schema-tree.php';
  
} else

if ($rest->get('/schema/{schema}') &&
  preg_match('/^[0-9a-f]{40}$/',$rest->getPathArg('schema'))==1) {
  
  require 'endpoint_schema.php';
  
} else

if ($rest->post('/schema/{schema}/query') &&
  preg_match('/^[0-9a-f]{40}$/',$rest->getPathArg('schema'))==1) {
  
  require 'endpoint_query.php';
  
} else

if ($rest->get('/update-hook')) {
  
  if ($rest->requestAuth()) {
    require 'content-update-hook.php';
  }
  
} else

if ($rest->get('/test-parser')) {
  
  if ($rest->requestAuth()) {
    require 'endpoint_test_parser.php';
  }
  
} else

if ($rest->get('/test-sql-transcriptor')) {
  
  if ($rest->requestAuth()) {
    require 'endpoint_test_sql_transcriptor.html';
  }
  
} else

{
  
  $rest->response_invalid('Resource unknown or method not allowed',404,'<h1>Resource unknown or method not allowed</h1>');
  
}

?>
