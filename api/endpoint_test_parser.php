<?php

define('TEST_ENVIRONMENT',null);
define('SCHEMA_ALATETHEJAZAT','c622a21996a791b001fb1265e88958164a6f86ff');

class RestProxyMockup {
  
  private $expression;
  
  public function setExpression($expression) {
    $this->expression=$expression;
    $_SERVER['CONTENT_LENGTH']=strlen($expression);
  }
  
  public function getPathArg($k) {
    return $k=='schema' ? SCHEMA_ALATETHEJAZAT : null;
  }
  
  public function getBody() {
    return $this->expression;
  }
  
}

$rest=new RestProxyMockup();

function runTest($expression) {
  
  global $rest, $schema_id, $params, $join_tables, $db;
  
  $rest->setExpression($expression);
  return require 'endpoint_query.php';
  
}

function displayAssertion($message,$bool) {
  
  echo '<input type="checkbox" disabled'.($bool ? ' checked' : ' style="box-shadow:0 0 0 3px crimson;"').'/> '.$message.'<br/>';
  
}

require_once 'parser.php';

?><h3>Lexical analysis</h3><?php
define('TEST_MODE_RETURN_TOKEN_ARRAY',null);

displayAssertion('Accepting property reference',
  runTest('prop::paraatereszto_kepesseg')==[Parser::TOKEN_PROP,Parser::TOKEN_SCOPE,Parser::TOKEN_LABEL,Parser::TOKEN_EOF]);

displayAssertion('Accepting literals, integer',
  runTest('0')==[Parser::TOKEN_LITERAL,Parser::TOKEN_EOF] && runTest('42')==[Parser::TOKEN_LITERAL,Parser::TOKEN_EOF] && runTest('-1')==[Parser::TOKEN_LITERAL,Parser::TOKEN_EOF]);
displayAssertion('Accepting literals, float',
  runTest('3.14')==[Parser::TOKEN_LITERAL,Parser::TOKEN_EOF] && runTest('0.0001')==[Parser::TOKEN_LITERAL,Parser::TOKEN_EOF]);
displayAssertion('Accepting literals, string',
  runTest('"example"')==[Parser::TOKEN_LITERAL,Parser::TOKEN_EOF] && runTest('""')==[Parser::TOKEN_LITERAL,Parser::TOKEN_EOF]);

displayAssertion('Accepting product specificities, related property existence',
  runTest('has any prop')==[Parser::TOKEN_HAS,Parser::TOKEN_ANY,Parser::TOKEN_PROP,Parser::TOKEN_EOF]);
displayAssertion('Accepting product specificities, related parameter existence',
  runTest('has any param')==[Parser::TOKEN_HAS,Parser::TOKEN_ANY,Parser::TOKEN_PARAM,Parser::TOKEN_EOF]);
displayAssertion('Accepting product specificities, being kit',
  runTest('is kit')==[Parser::TOKEN_IS,Parser::TOKEN_KIT,Parser::TOKEN_EOF]);
displayAssertion('Accepting product specificities, being kit child',
  runTest('is kit child')==[Parser::TOKEN_IS,Parser::TOKEN_KIT,Parser::TOKEN_CHILD,Parser::TOKEN_EOF]);

displayAssertion('Accepting parentheses',
  runTest('(is kit)')==[Parser::TOKEN_BRACKET_OPEN,Parser::TOKEN_IS,Parser::TOKEN_KIT,Parser::TOKEN_BRACKET_CLOSE,Parser::TOKEN_EOF]);

displayAssertion('Accepting operators, negation',
  runTest('!is kit')==[Parser::TOKEN_NOT,Parser::TOKEN_IS,Parser::TOKEN_KIT,Parser::TOKEN_EOF]);

displayAssertion('Accepting operators, <i>less or equals</i> relation',
  runTest('prop::paraatereszto_kepesseg<=0.15')==[Parser::TOKEN_PROP,Parser::TOKEN_SCOPE,Parser::TOKEN_LABEL,Parser::TOKEN_LESS_OR_EQUALS,Parser::TOKEN_LITERAL,Parser::TOKEN_EOF]);
displayAssertion('Accepting operators, <i>less than</i> relation',
  runTest('prop::paraatereszto_kepesseg<0.15')==[Parser::TOKEN_PROP,Parser::TOKEN_SCOPE,Parser::TOKEN_LABEL,Parser::TOKEN_LESS_THAN,Parser::TOKEN_LITERAL,Parser::TOKEN_EOF]);
displayAssertion('Accepting operators, <i>greater or equals</i> relation',
  runTest('prop::paraatereszto_kepesseg>=0.15')==[Parser::TOKEN_PROP,Parser::TOKEN_SCOPE,Parser::TOKEN_LABEL,Parser::TOKEN_GREATER_OR_EQUALS,Parser::TOKEN_LITERAL,Parser::TOKEN_EOF]);
displayAssertion('Accepting operators, <i>greater than</i> relation',
  runTest('prop::paraatereszto_kepesseg>0.15')==[Parser::TOKEN_PROP,Parser::TOKEN_SCOPE,Parser::TOKEN_LABEL,Parser::TOKEN_GREATER_THAN,Parser::TOKEN_LITERAL,Parser::TOKEN_EOF]);

displayAssertion('Accepting operators, equality',
  runTest('prop::paraatereszto_kepesseg==0.15')==[Parser::TOKEN_PROP,Parser::TOKEN_SCOPE,Parser::TOKEN_LABEL,Parser::TOKEN_EQUALS,Parser::TOKEN_LITERAL,Parser::TOKEN_EOF]);
displayAssertion('Accepting operators, inequality (distinguishing from negation)',
  runTest('prop::paraatereszto_kepesseg!=0.15')==[Parser::TOKEN_PROP,Parser::TOKEN_SCOPE,Parser::TOKEN_LABEL,Parser::TOKEN_NOT_EQUALS,Parser::TOKEN_LITERAL,Parser::TOKEN_EOF]);
displayAssertion('Accepting operators, set membership (cardinality of 1)',
  runTest('prop::eghetoseg in ("E")')==[Parser::TOKEN_PROP,Parser::TOKEN_SCOPE,Parser::TOKEN_LABEL,Parser::TOKEN_IN,Parser::TOKEN_BRACKET_OPEN,
    Parser::TOKEN_LITERAL,
  Parser::TOKEN_BRACKET_CLOSE,Parser::TOKEN_EOF]);
displayAssertion('Accepting operators, set membership (cardinality of n)',
  runTest('prop::eghetoseg in ("B","C","D","E")')==[Parser::TOKEN_PROP,Parser::TOKEN_SCOPE,Parser::TOKEN_LABEL,Parser::TOKEN_IN,Parser::TOKEN_BRACKET_OPEN,
    Parser::TOKEN_LITERAL,Parser::TOKEN_COMMA,
    Parser::TOKEN_LITERAL,Parser::TOKEN_COMMA,
    Parser::TOKEN_LITERAL,Parser::TOKEN_COMMA,
    Parser::TOKEN_LITERAL,
  Parser::TOKEN_BRACKET_CLOSE,Parser::TOKEN_EOF]);

displayAssertion('Accepting operators, conjunction',
  runTest('is kit && has any prop')==[Parser::TOKEN_IS,Parser::TOKEN_KIT,Parser::TOKEN_AND,Parser::TOKEN_HAS,Parser::TOKEN_ANY,Parser::TOKEN_PROP,Parser::TOKEN_EOF]);
displayAssertion('Accepting operators, disjunction',
  runTest('is kit || has any prop')==[Parser::TOKEN_IS,Parser::TOKEN_KIT,Parser::TOKEN_OR,Parser::TOKEN_HAS,Parser::TOKEN_ANY,Parser::TOKEN_PROP,Parser::TOKEN_EOF]);

displayAssertion('Ignoring whitespace',
  runTest('!is kit')==runTest("\t\n!\v\f\ris kit "));

displayAssertion('Enforcing word boundaries of reserved words',
  runTest('hasanyprop')!=[Parser::TOKEN_HAS,Parser::TOKEN_ANY,Parser::TOKEN_PROP,Parser::TOKEN_EOF] &&
  runTest('hasanyparam')!=[Parser::TOKEN_HAS,Parser::TOKEN_ANY,Parser::TOKEN_PARAM,Parser::TOKEN_EOF] &&
  runTest('iskit')!=[Parser::TOKEN_IS,Parser::TOKEN_KIT,Parser::TOKEN_EOF] &&
  runTest('iskitchild')!=[Parser::TOKEN_IS,Parser::TOKEN_KIT,Parser::TOKEN_CHILD,Parser::TOKEN_EOF]);
displayAssertion('Being permissive about the contextual word boundary of keyword "<i>in</i>"',
  runTest('prop::eghetoseg in("E")')==[Parser::TOKEN_PROP,Parser::TOKEN_SCOPE,Parser::TOKEN_LABEL,Parser::TOKEN_IN,Parser::TOKEN_BRACKET_OPEN,
    Parser::TOKEN_LITERAL,
  Parser::TOKEN_BRACKET_CLOSE,Parser::TOKEN_EOF]);

displayAssertion('Rejecting invalid input',
  ($e=runTest('❤️😅👋')) && $e[count($e)-1]==Parser::TOKEN_ERR);
displayAssertion('Accepting any misspelled keyword as label',
  runTest('pop::paraatereszto_kepesseg==0.15')==[Parser::TOKEN_LABEL,Parser::TOKEN_SCOPE,Parser::TOKEN_LABEL,Parser::TOKEN_EQUALS,Parser::TOKEN_LITERAL,Parser::TOKEN_EOF]);

?><h3>Syntax analysis</h3><?php
define('TEST_MODE_RETURN_PARSE_TREE_SYNTAX',null);

displayAssertion('Production rules, property reference',
  ($e=runTest('prop::paraatereszto_kepesseg')) && array_key_exists('prop_row',$e) && $e['prop_row']=='&paraatereszto_kepesseg');

displayAssertion('Production rules, literals',
  ($e=runTest('42')) && array_key_exists('literal',$e) && $e['literal']===42 &&
  ($e=runTest('3.14')) && array_key_exists('literal',$e) && $e['literal']===3.14 &&
  ($e=runTest('"example"')) && array_key_exists('literal',$e) && $e['literal']==='example');

displayAssertion('Production rules, product specificities',
  ($e=runTest('has any prop')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_HAS && array_key_exists('attr',$e) && $e['attr']==Parser::TOKEN_PROP &&
  ($e=runTest('has any param')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_HAS && array_key_exists('attr',$e) && $e['attr']==Parser::TOKEN_PARAM &&
  ($e=runTest('is kit')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_HAS && array_key_exists('attr',$e) && $e['attr']==Parser::TOKEN_KIT &&
  ($e=runTest('is kit child')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_HAS && array_key_exists('attr',$e) && $e['attr']==Parser::TOKEN_CHILD);

displayAssertion('Production rules, set with cardinality of n',
  ($e=runTest('("B","C","D","E")')) && array_key_exists('set',$e) && $e['set']===['B','C','D','E']);

displayAssertion('Production rules, parenthesized expression/set with cardinality of 1',
  ($e=runTest('(is kit)')) && array_key_exists('in_bracket',$e) && !!$e['in_bracket'] &&
  ($e=runTest('("E")')) && array_key_exists('in_bracket',$e) && !!$e['in_bracket']);

displayAssertion('Production rules, unary operator(s)',
  ($e=runTest('!is kit')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_NOT);

displayAssertion('Production rules, binary operator(s)',
  ($e=runTest('prop::paraatereszto_kepesseg<=0.15')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_LESS_OR_EQUALS &&
  ($e=runTest('prop::paraatereszto_kepesseg<0.15')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_LESS_THAN &&
  ($e=runTest('prop::paraatereszto_kepesseg>=0.15')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_GREATER_OR_EQUALS &&
  ($e=runTest('prop::paraatereszto_kepesseg>0.15')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_GREATER_THAN &&
  ($e=runTest('prop::paraatereszto_kepesseg==0.15')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_EQUALS &&
  ($e=runTest('prop::paraatereszto_kepesseg!=0.15')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_NOT_EQUALS &&
  ($e=runTest('prop::eghetoseg in ("B","C","D","E")')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_IN &&
  ($e=runTest('is kit && has any prop')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_AND &&
  ($e=runTest('is kit || has any prop')) && array_key_exists('operator',$e) && $e['operator']==Parser::TOKEN_OR);

displayAssertion('Indicating syntax errors, input deviates from production rule of property reference',
  in_array('Illegal expression',runTest('::paraatereszto_kepesseg')) &&
  in_array('Illegal expression',runTest('propparaatereszto_kepesseg')) &&
  in_array('Illegal expression',runTest('prop::')));
displayAssertion('Indicating syntax errors, input deviates from production rules of product specificities',
  in_array('Illegal expression',runTest('any prop')) &&
  in_array('Illegal expression',runTest('any param')) &&
  in_array('Illegal expression',runTest('has prop')) &&
  in_array('Illegal expression',runTest('has param')) &&
  in_array('Illegal expression',runTest('has any')) &&
  in_array('Illegal expression',runTest('kit')) &&
  in_array('Illegal expression',runTest('is')) &&
  in_array('Illegal expression',runTest('is child')));
displayAssertion('Indicating syntax errors, input deviates from production rules of set with cardinality of n and of parenthesized expression/set with cardinality of 1',
  in_array(':3 Unexpected token, root expression ends prematurely',runTest('"B","C","D","E")')) &&
  in_array('Illegal expression',runTest('()')) &&
  in_array('Illegal expression',runTest('("B""C""D""E")')) &&
  in_array('Illegal expression',runTest('("B","C","D","E"')));
displayAssertion('Indicating syntax errors, input deviates from production rule of set with cardinality of n by supplying a non-literal',
  in_array('Illegal expression',runTest('(prop::paraatereszto_kepesseg,"E")')) && in_array('Illegal expression',runTest('(is kit,"E")')) && in_array('Illegal expression',runTest('(("B"),"C","D","E")')) &&
  in_array('Illegal expression',runTest('("E",prop::paraatereszto_kepesseg)')) && in_array('Illegal expression',runTest('("E", is kit)')) && in_array('Illegal expression',runTest('("B",("C"),"D","E")')));
displayAssertion('Indicating syntax errors, input deviates from production rule(s) of unary operator(s)',
  in_array('Illegal expression',runTest('!')));
displayAssertion('Indicating syntax errors, input deviates from production rule(s) of binary operator(s)',
  in_array('Illegal expression',runTest('in ("B","C","D","E")')) &&
  in_array('Illegal expression',runTest('prop::eghetoseg in')) &&
  in_array('Illegal expression',runTest('&& has any prop')) &&
  in_array('Illegal expression',runTest('is kit &&')) &&
  in_array('Illegal expression',runTest('|| has any prop')) &&
  in_array('Illegal expression',runTest('is kit ||')) &&
  in_array('Illegal expression',runTest('<=0.15')) &&
  in_array('Illegal expression',runTest('prop::paraatereszto_kepesseg<=')) &&
  in_array('Illegal expression',runTest('<0.15')) &&
  in_array('Illegal expression',runTest('prop::paraatereszto_kepesseg<')) &&
  in_array('Illegal expression',runTest('>=0.15')) &&
  in_array('Illegal expression',runTest('prop::paraatereszto_kepesseg>=')) &&
  in_array('Illegal expression',runTest('>0.15')) &&
  in_array('Illegal expression',runTest('prop::paraatereszto_kepesseg>')) &&
  in_array('Illegal expression',runTest('==0.15')) &&
  in_array('Illegal expression',runTest('prop::paraatereszto_kepesseg==')) &&
  in_array('Illegal expression',runTest('!=0.15')) &&
  in_array('Illegal expression',runTest('prop::paraatereszto_kepesseg!=')));

displayAssertion('Operator precedence',
  ($e=runTest('!is kit && has any prop')) && $e['operator']==Parser::TOKEN_AND && $e['operands'][0]['operator']==Parser::TOKEN_NOT &&
  ($e=runTest('prop::paraatereszto_kepesseg<=0.15 && prop::paraatereszto_kepesseg>0.15')) && $e['operator']==Parser::TOKEN_AND && $e['operands'][0]['operator']==Parser::TOKEN_LESS_OR_EQUALS && $e['operands'][1]['operator']==Parser::TOKEN_GREATER_THAN &&
  ($e=runTest('prop::paraatereszto_kepesseg<0.15 && prop::paraatereszto_kepesseg>=0.15')) && $e['operator']==Parser::TOKEN_AND && $e['operands'][0]['operator']==Parser::TOKEN_LESS_THAN && $e['operands'][1]['operator']==Parser::TOKEN_GREATER_OR_EQUALS &&
  ($e=runTest('prop::paraatereszto_kepesseg==0.15 && prop::paraatereszto_kepesseg!=0.15')) && $e['operator']==Parser::TOKEN_AND && $e['operands'][0]['operator']==Parser::TOKEN_EQUALS && $e['operands'][1]['operator']==Parser::TOKEN_NOT_EQUALS &&
  ($e=runTest('prop::eghetoseg in ("B","C","D") && prop::eghetoseg in ("E")')) && $e['operator']==Parser::TOKEN_AND && $e['operands'][0]['operator']==Parser::TOKEN_IN && $e['operands'][1]['operator']==Parser::TOKEN_IN &&
  ($e=runTest('!is kit || has any prop')) && $e['operator']==Parser::TOKEN_OR && $e['operands'][0]['operator']==Parser::TOKEN_NOT &&
  ($e=runTest('prop::paraatereszto_kepesseg<=0.15 || prop::paraatereszto_kepesseg>0.15')) && $e['operator']==Parser::TOKEN_OR && $e['operands'][0]['operator']==Parser::TOKEN_LESS_OR_EQUALS && $e['operands'][1]['operator']==Parser::TOKEN_GREATER_THAN &&
  ($e=runTest('prop::paraatereszto_kepesseg<0.15 || prop::paraatereszto_kepesseg>=0.15')) && $e['operator']==Parser::TOKEN_OR && $e['operands'][0]['operator']==Parser::TOKEN_LESS_THAN && $e['operands'][1]['operator']==Parser::TOKEN_GREATER_OR_EQUALS &&
  ($e=runTest('prop::paraatereszto_kepesseg==0.15 || prop::paraatereszto_kepesseg!=0.15')) && $e['operator']==Parser::TOKEN_OR && $e['operands'][0]['operator']==Parser::TOKEN_EQUALS && $e['operands'][1]['operator']==Parser::TOKEN_NOT_EQUALS &&
  ($e=runTest('prop::eghetoseg in ("B","C","D") || prop::eghetoseg in ("E")')) && $e['operator']==Parser::TOKEN_OR && $e['operands'][0]['operator']==Parser::TOKEN_IN && $e['operands'][1]['operator']==Parser::TOKEN_IN &&
  ($e=runTest('is kit child || is kit && has any prop')) && $e['operator']==Parser::TOKEN_OR && $e['operands'][1]['operator']==Parser::TOKEN_AND);
displayAssertion('Parentheses override precedence',
  ($e=runTest('(is kit child || is kit) && has any prop')) && $e['operator']==Parser::TOKEN_AND && $e['operands'][0]['operator']==Parser::TOKEN_OR);
displayAssertion('Operator associativity',
  ($e=runTest('is kit && is kit child && has any prop')) && $e['operator']==Parser::TOKEN_AND && $e['operands'][0]['operator']==Parser::TOKEN_AND && $e['operands'][1]['operator']==Parser::TOKEN_HAS && $e['operands'][1]['attr']==Parser::TOKEN_PROP);

?><h3>Semantic analysis</h3><?php

displayAssertion('Type check, production rule of property reference',
  runTest('prop::paraatereszto_kepesseg')['type']==Parser::TYPE_REF);

displayAssertion('Type check, production rules of literals',
  runTest('42')['type']==Parser::TYPE_NUMERIC &&
  runTest('3.14')['type']==Parser::TYPE_NUMERIC &&
  runTest('"example"')['type']==Parser::TYPE_STRING);

displayAssertion('Type check, production rules of product specificities',
  runTest('has any prop')['type']==Parser::TYPE_BOOL &&
  runTest('has any param')['type']==Parser::TYPE_BOOL &&
  runTest('is kit')['type']==Parser::TYPE_BOOL &&
  runTest('is kit child')['type']==Parser::TYPE_BOOL);

displayAssertion('Type check, production rule of set with cardinality of n',
  runTest('("B","C","D","E")')['type']==Parser::TYPE_SET);

displayAssertion('Type check, production rule of parenthesized expression/set with cardinality of 1',
  ($e=runTest('("E")')) && $e['type']==Parser::TYPE_STRING && array_key_exists('literal',$e) && array_key_exists('in_bracket',$e));

displayAssertion('Type check, production rule(s) of unary operator(s)',
  runTest('!is kit')['type']==Parser::TYPE_BOOL);

displayAssertion('Type check, production rule(s) of binary operator(s)',
  runTest('prop::paraatereszto_kepesseg<=0.15')['type']==Parser::TYPE_BOOL &&
  runTest('prop::paraatereszto_kepesseg<0.15')['type']==Parser::TYPE_BOOL &&
  runTest('prop::paraatereszto_kepesseg>=0.15')['type']==Parser::TYPE_BOOL &&
  runTest('prop::paraatereszto_kepesseg>0.15')['type']==Parser::TYPE_BOOL &&
  runTest('prop::paraatereszto_kepesseg==0.15')['type']==Parser::TYPE_BOOL &&
  runTest('prop::paraatereszto_kepesseg!=0.15')['type']==Parser::TYPE_BOOL &&
  runTest('prop::eghetoseg in ("B","C","D","E")')['type']==Parser::TYPE_BOOL &&
  runTest('is kit && has any prop')['type']==Parser::TYPE_BOOL &&
  runTest('is kit || has any prop')['type']==Parser::TYPE_BOOL);

define('TEST_MODE_RETURN_PARSE_TREE_SEMANTICS',null);

displayAssertion('Type check, operand type requirements, basic',
  ($t_reference='prop::paraatereszto_kepesseg') && ($t_numeric='1') && ($t_string='"example"') && ($t_boolean='is kit') && ($t_set='("B","C","D","E")') &&
  in_array(':0 Logical operator '."'!'".' requires its operand to be boolean type (got reference :1-'.(1+strlen($t_reference)).')',runTest('!'.$t_reference)) &&
  in_array(':0 Logical operator '."'!'".' requires its operand to be boolean type (got numeric :1-'.(1+strlen($t_numeric)).')',runTest('!'.$t_numeric)) &&
  in_array(':0 Logical operator '."'!'".' requires its operand to be boolean type (got string :1-'.(1+strlen($t_string)).')',runTest('!'.$t_string)) &&
  in_array(':0 Logical operator '."'!'".' requires its operand to be boolean type (got set :1-'.(1+strlen($t_set)).')',runTest('!'.$t_set)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'<='".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and reference :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_reference)).')',runTest($t_reference.'<='.$t_reference)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'<'".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and reference :'.(strlen($t_reference)+1).'-'.(strlen($t_reference)+1+strlen($t_reference)).')',runTest($t_reference.'<'.$t_reference)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'>='".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and reference :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_reference)).')',runTest($t_reference.'>='.$t_reference)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'>'".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and reference :'.(strlen($t_reference)+1).'-'.(strlen($t_reference)+1+strlen($t_reference)).')',runTest($t_reference.'>'.$t_reference)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'<='".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and string :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_string)).')',runTest($t_reference.'<='.$t_string)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'<'".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and string :'.(strlen($t_reference)+1).'-'.(strlen($t_reference)+1+strlen($t_string)).')',runTest($t_reference.'<'.$t_string)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'>='".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and string :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_string)).')',runTest($t_reference.'>='.$t_string)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'>'".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and string :'.(strlen($t_reference)+1).'-'.(strlen($t_reference)+1+strlen($t_string)).')',runTest($t_reference.'>'.$t_string)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'<='".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and boolean :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_boolean)).')',runTest($t_reference.'<='.$t_boolean)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'<'".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and boolean :'.(strlen($t_reference)+1).'-'.(strlen($t_reference)+1+strlen($t_boolean)).')',runTest($t_reference.'<'.$t_boolean)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'>='".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and boolean :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_boolean)).')',runTest($t_reference.'>='.$t_boolean)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'>'".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and boolean :'.(strlen($t_reference)+1).'-'.(strlen($t_reference)+1+strlen($t_boolean)).')',runTest($t_reference.'>'.$t_boolean)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'<='".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and set :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_set)).')',runTest($t_reference.'<='.$t_set)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'<'".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and set :'.(strlen($t_reference)+1).'-'.(strlen($t_reference)+1+strlen($t_set)).')',runTest($t_reference.'<'.$t_set)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'>='".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and set :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_set)).')',runTest($t_reference.'>='.$t_set)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'>'".' requires one of the operands to be numeric type (got reference :0-'.strlen($t_reference).' and set :'.(strlen($t_reference)+1).'-'.(strlen($t_reference)+1+strlen($t_set)).')',runTest($t_reference.'>'.$t_set)) &&
  in_array(':'.strlen($t_string).' Relational operator '."'<='".' requires one of the operands to be numeric type (got string :0-'.strlen($t_string).' and reference :'.(strlen($t_string)+2).'-'.(strlen($t_string)+2+strlen($t_reference)).')',runTest($t_string.'<='.$t_reference)) &&
  in_array(':'.strlen($t_string).' Relational operator '."'<'".' requires one of the operands to be numeric type (got string :0-'.strlen($t_string).' and reference :'.(strlen($t_string)+1).'-'.(strlen($t_string)+1+strlen($t_reference)).')',runTest($t_string.'<'.$t_reference)) &&
  in_array(':'.strlen($t_string).' Relational operator '."'>='".' requires one of the operands to be numeric type (got string :0-'.strlen($t_string).' and reference :'.(strlen($t_string)+2).'-'.(strlen($t_string)+2+strlen($t_reference)).')',runTest($t_string.'>='.$t_reference)) &&
  in_array(':'.strlen($t_string).' Relational operator '."'>'".' requires one of the operands to be numeric type (got string :0-'.strlen($t_string).' and reference :'.(strlen($t_string)+1).'-'.(strlen($t_string)+1+strlen($t_reference)).')',runTest($t_string.'>'.$t_reference)) &&
  in_array(':'.strlen($t_boolean).' Relational operator '."'<='".' requires one of the operands to be numeric type (got boolean :0-'.strlen($t_boolean).' and reference :'.(strlen($t_boolean)+2).'-'.(strlen($t_boolean)+2+strlen($t_reference)).')',runTest($t_boolean.'<='.$t_reference)) &&
  in_array(':'.strlen($t_boolean).' Relational operator '."'<'".' requires one of the operands to be numeric type (got boolean :0-'.strlen($t_boolean).' and reference :'.(strlen($t_boolean)+1).'-'.(strlen($t_boolean)+1+strlen($t_reference)).')',runTest($t_boolean.'<'.$t_reference)) &&
  in_array(':'.strlen($t_boolean).' Relational operator '."'>='".' requires one of the operands to be numeric type (got boolean :0-'.strlen($t_boolean).' and reference :'.(strlen($t_boolean)+2).'-'.(strlen($t_boolean)+2+strlen($t_reference)).')',runTest($t_boolean.'>='.$t_reference)) &&
  in_array(':'.strlen($t_boolean).' Relational operator '."'>'".' requires one of the operands to be numeric type (got boolean :0-'.strlen($t_boolean).' and reference :'.(strlen($t_boolean)+1).'-'.(strlen($t_boolean)+1+strlen($t_reference)).')',runTest($t_boolean.'>'.$t_reference)) &&
  in_array(':'.strlen($t_set).' Relational operator '."'<='".' requires one of the operands to be numeric type (got set :0-'.strlen($t_set).' and reference :'.(strlen($t_set)+2).'-'.(strlen($t_set)+2+strlen($t_reference)).')',runTest($t_set.'<='.$t_reference)) &&
  in_array(':'.strlen($t_set).' Relational operator '."'<'".' requires one of the operands to be numeric type (got set :0-'.strlen($t_set).' and reference :'.(strlen($t_set)+1).'-'.(strlen($t_set)+1+strlen($t_reference)).')',runTest($t_set.'<'.$t_reference)) &&
  in_array(':'.strlen($t_set).' Relational operator '."'>='".' requires one of the operands to be numeric type (got set :0-'.strlen($t_set).' and reference :'.(strlen($t_set)+2).'-'.(strlen($t_set)+2+strlen($t_reference)).')',runTest($t_set.'>='.$t_reference)) &&
  in_array(':'.strlen($t_set).' Relational operator '."'>'".' requires one of the operands to be numeric type (got set :0-'.strlen($t_set).' and reference :'.(strlen($t_set)+1).'-'.(strlen($t_set)+1+strlen($t_reference)).')',runTest($t_set.'>'.$t_reference)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'=='".' requires one of the operands to be numeric or string type (got reference :0-'.strlen($t_reference).' and reference :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_reference)).')',runTest($t_reference.'=='.$t_reference)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'!='".' requires one of the operands to be numeric or string type (got reference :0-'.strlen($t_reference).' and reference :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_reference)).')',runTest($t_reference.'!='.$t_reference)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'=='".' requires one of the operands to be numeric or string type (got reference :0-'.strlen($t_reference).' and boolean :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_boolean)).')',runTest($t_reference.'=='.$t_boolean)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'!='".' requires one of the operands to be numeric or string type (got reference :0-'.strlen($t_reference).' and boolean :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_boolean)).')',runTest($t_reference.'!='.$t_boolean)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'=='".' requires one of the operands to be numeric or string type (got reference :0-'.strlen($t_reference).' and set :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_set)).')',runTest($t_reference.'=='.$t_set)) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'!='".' requires one of the operands to be numeric or string type (got reference :0-'.strlen($t_reference).' and set :'.(strlen($t_reference)+2).'-'.(strlen($t_reference)+2+strlen($t_set)).')',runTest($t_reference.'!='.$t_set)) &&
  in_array(':'.strlen($t_boolean).' Relational operator '."'=='".' requires one of the operands to be numeric or string type (got boolean :0-'.strlen($t_boolean).' and reference :'.(strlen($t_boolean)+2).'-'.(strlen($t_boolean)+2+strlen($t_reference)).')',runTest($t_boolean.'=='.$t_reference)) &&
  in_array(':'.strlen($t_boolean).' Relational operator '."'!='".' requires one of the operands to be numeric or string type (got boolean :0-'.strlen($t_boolean).' and reference :'.(strlen($t_boolean)+2).'-'.(strlen($t_boolean)+2+strlen($t_reference)).')',runTest($t_boolean.'!='.$t_reference)) &&
  in_array(':'.strlen($t_set).' Relational operator '."'=='".' requires one of the operands to be numeric or string type (got set :0-'.strlen($t_set).' and reference :'.(strlen($t_set)+2).'-'.(strlen($t_set)+2+strlen($t_reference)).')',runTest($t_set.'=='.$t_reference)) &&
  in_array(':'.strlen($t_set).' Relational operator '."'!='".' requires one of the operands to be numeric or string type (got set :0-'.strlen($t_set).' and reference :'.(strlen($t_set)+2).'-'.(strlen($t_set)+2+strlen($t_reference)).')',runTest($t_set.'!='.$t_reference)) &&
  in_array(':'.(strlen($t_numeric)+1).' Set membership operator '."'in'".' requires its left operand to be reference type (got numeric :0-'.strlen($t_numeric).')',runTest($t_numeric.' in (0.15)')) &&
  in_array(':'.(strlen($t_string)+1).' Set membership operator '."'in'".' requires its left operand to be reference type (got string :0-'.strlen($t_string).')',runTest($t_string.' in (0.15)')) &&
  in_array(':'.(strlen($t_boolean)+1).' Set membership operator '."'in'".' requires its left operand to be reference type (got boolean :0-'.strlen($t_boolean).')',runTest($t_boolean.' in (0.15)')) &&
  in_array(':'.(strlen($t_set)+1).' Set membership operator '."'in'".' requires its left operand to be reference type (got set :0-'.strlen($t_set).')',runTest($t_set.' in (0.15)')) &&
  in_array(':'.(strlen($t_reference)+1).' Set membership operator '."'in'".' requires its right operand to be set type (got reference :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_reference)).')',runTest($t_reference.' in '.$t_reference)) &&
  in_array(':'.(strlen($t_reference)+1).' Set membership operator '."'in'".' requires its right operand to be set type (got numeric :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_numeric)).')',runTest($t_reference.' in '.$t_numeric)) &&
  in_array(':'.(strlen($t_reference)+1).' Set membership operator '."'in'".' requires its right operand to be set type (got string :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_string)).')',runTest($t_reference.' in '.$t_string)) &&
  in_array(':'.(strlen($t_reference)+1).' Set membership operator '."'in'".' requires its right operand to be set type (got boolean :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_boolean)).')',runTest($t_reference.' in '.$t_boolean)) &&
  in_array(':'.(strlen($t_reference)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got reference :0-'.strlen($t_reference).' and reference :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_reference)).')',runTest($t_reference.' && '.$t_reference)) &&
  in_array(':'.(strlen($t_reference)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got reference :0-'.strlen($t_reference).' and reference :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_reference)).')',runTest($t_reference.' || '.$t_reference)) &&
  in_array(':'.(strlen($t_reference)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got reference :0-'.strlen($t_reference).' and numeric :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_numeric)).')',runTest($t_reference.' && '.$t_numeric)) &&
  in_array(':'.(strlen($t_reference)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got reference :0-'.strlen($t_reference).' and numeric :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_numeric)).')',runTest($t_reference.' || '.$t_numeric)) &&
  in_array(':'.(strlen($t_reference)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got reference :0-'.strlen($t_reference).' and string :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_string)).')',runTest($t_reference.' && '.$t_string)) &&
  in_array(':'.(strlen($t_reference)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got reference :0-'.strlen($t_reference).' and string :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_string)).')',runTest($t_reference.' || '.$t_string)) &&
  in_array(':'.(strlen($t_reference)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got reference :0-'.strlen($t_reference).' and boolean :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_boolean)).')',runTest($t_reference.' && '.$t_boolean)) &&
  in_array(':'.(strlen($t_reference)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got reference :0-'.strlen($t_reference).' and boolean :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_boolean)).')',runTest($t_reference.' || '.$t_boolean)) &&
  in_array(':'.(strlen($t_reference)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got reference :0-'.strlen($t_reference).' and set :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_set)).')',runTest($t_reference.' && '.$t_set)) &&
  in_array(':'.(strlen($t_reference)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got reference :0-'.strlen($t_reference).' and set :'.(strlen($t_reference)+4).'-'.(strlen($t_reference)+4+strlen($t_set)).')',runTest($t_reference.' || '.$t_set)) &&
  in_array(':'.(strlen($t_numeric)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got numeric :0-'.strlen($t_numeric).' and reference :'.(strlen($t_numeric)+4).'-'.(strlen($t_numeric)+4+strlen($t_reference)).')',runTest($t_numeric.' && '.$t_reference)) &&
  in_array(':'.(strlen($t_numeric)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got numeric :0-'.strlen($t_numeric).' and reference :'.(strlen($t_numeric)+4).'-'.(strlen($t_numeric)+4+strlen($t_reference)).')',runTest($t_numeric.' || '.$t_reference)) &&
  in_array(':'.(strlen($t_numeric)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got numeric :0-'.strlen($t_numeric).' and numeric :'.(strlen($t_numeric)+4).'-'.(strlen($t_numeric)+4+strlen($t_numeric)).')',runTest($t_numeric.' && '.$t_numeric)) &&
  in_array(':'.(strlen($t_numeric)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got numeric :0-'.strlen($t_numeric).' and numeric :'.(strlen($t_numeric)+4).'-'.(strlen($t_numeric)+4+strlen($t_numeric)).')',runTest($t_numeric.' || '.$t_numeric)) &&
  in_array(':'.(strlen($t_numeric)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got numeric :0-'.strlen($t_numeric).' and string :'.(strlen($t_numeric)+4).'-'.(strlen($t_numeric)+4+strlen($t_string)).')',runTest($t_numeric.' && '.$t_string)) &&
  in_array(':'.(strlen($t_numeric)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got numeric :0-'.strlen($t_numeric).' and string :'.(strlen($t_numeric)+4).'-'.(strlen($t_numeric)+4+strlen($t_string)).')',runTest($t_numeric.' || '.$t_string)) &&
  in_array(':'.(strlen($t_numeric)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got numeric :0-'.strlen($t_numeric).' and boolean :'.(strlen($t_numeric)+4).'-'.(strlen($t_numeric)+4+strlen($t_boolean)).')',runTest($t_numeric.' && '.$t_boolean)) &&
  in_array(':'.(strlen($t_numeric)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got numeric :0-'.strlen($t_numeric).' and boolean :'.(strlen($t_numeric)+4).'-'.(strlen($t_numeric)+4+strlen($t_boolean)).')',runTest($t_numeric.' || '.$t_boolean)) &&
  in_array(':'.(strlen($t_numeric)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got numeric :0-'.strlen($t_numeric).' and set :'.(strlen($t_numeric)+4).'-'.(strlen($t_numeric)+4+strlen($t_set)).')',runTest($t_numeric.' && '.$t_set)) &&
  in_array(':'.(strlen($t_numeric)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got numeric :0-'.strlen($t_numeric).' and set :'.(strlen($t_numeric)+4).'-'.(strlen($t_numeric)+4+strlen($t_set)).')',runTest($t_numeric.' || '.$t_set)) &&
  in_array(':'.(strlen($t_string)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got string :0-'.strlen($t_string).' and reference :'.(strlen($t_string)+4).'-'.(strlen($t_string)+4+strlen($t_reference)).')',runTest($t_string.' && '.$t_reference)) &&
  in_array(':'.(strlen($t_string)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got string :0-'.strlen($t_string).' and reference :'.(strlen($t_string)+4).'-'.(strlen($t_string)+4+strlen($t_reference)).')',runTest($t_string.' || '.$t_reference)) &&
  in_array(':'.(strlen($t_string)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got string :0-'.strlen($t_string).' and numeric :'.(strlen($t_string)+4).'-'.(strlen($t_string)+4+strlen($t_numeric)).')',runTest($t_string.' && '.$t_numeric)) &&
  in_array(':'.(strlen($t_string)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got string :0-'.strlen($t_string).' and numeric :'.(strlen($t_string)+4).'-'.(strlen($t_string)+4+strlen($t_numeric)).')',runTest($t_string.' || '.$t_numeric)) &&
  in_array(':'.(strlen($t_string)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got string :0-'.strlen($t_string).' and string :'.(strlen($t_string)+4).'-'.(strlen($t_string)+4+strlen($t_string)).')',runTest($t_string.' && '.$t_string)) &&
  in_array(':'.(strlen($t_string)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got string :0-'.strlen($t_string).' and string :'.(strlen($t_string)+4).'-'.(strlen($t_string)+4+strlen($t_string)).')',runTest($t_string.' || '.$t_string)) &&
  in_array(':'.(strlen($t_string)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got string :0-'.strlen($t_string).' and boolean :'.(strlen($t_string)+4).'-'.(strlen($t_string)+4+strlen($t_boolean)).')',runTest($t_string.' && '.$t_boolean)) &&
  in_array(':'.(strlen($t_string)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got string :0-'.strlen($t_string).' and boolean :'.(strlen($t_string)+4).'-'.(strlen($t_string)+4+strlen($t_boolean)).')',runTest($t_string.' || '.$t_boolean)) &&
  in_array(':'.(strlen($t_string)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got string :0-'.strlen($t_string).' and set :'.(strlen($t_string)+4).'-'.(strlen($t_string)+4+strlen($t_set)).')',runTest($t_string.' && '.$t_set)) &&
  in_array(':'.(strlen($t_string)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got string :0-'.strlen($t_string).' and set :'.(strlen($t_string)+4).'-'.(strlen($t_string)+4+strlen($t_set)).')',runTest($t_string.' || '.$t_set)) &&
  in_array(':'.(strlen($t_boolean)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got boolean :0-'.strlen($t_boolean).' and reference :'.(strlen($t_boolean)+4).'-'.(strlen($t_boolean)+4+strlen($t_reference)).')',runTest($t_boolean.' && '.$t_reference)) &&
  in_array(':'.(strlen($t_boolean)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got boolean :0-'.strlen($t_boolean).' and reference :'.(strlen($t_boolean)+4).'-'.(strlen($t_boolean)+4+strlen($t_reference)).')',runTest($t_boolean.' || '.$t_reference)) &&
  in_array(':'.(strlen($t_boolean)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got boolean :0-'.strlen($t_boolean).' and numeric :'.(strlen($t_boolean)+4).'-'.(strlen($t_boolean)+4+strlen($t_numeric)).')',runTest($t_boolean.' && '.$t_numeric)) &&
  in_array(':'.(strlen($t_boolean)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got boolean :0-'.strlen($t_boolean).' and numeric :'.(strlen($t_boolean)+4).'-'.(strlen($t_boolean)+4+strlen($t_numeric)).')',runTest($t_boolean.' || '.$t_numeric)) &&
  in_array(':'.(strlen($t_boolean)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got boolean :0-'.strlen($t_boolean).' and string :'.(strlen($t_boolean)+4).'-'.(strlen($t_boolean)+4+strlen($t_string)).')',runTest($t_boolean.' && '.$t_string)) &&
  in_array(':'.(strlen($t_boolean)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got boolean :0-'.strlen($t_boolean).' and string :'.(strlen($t_boolean)+4).'-'.(strlen($t_boolean)+4+strlen($t_string)).')',runTest($t_boolean.' || '.$t_string)) &&
  in_array(':'.(strlen($t_boolean)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got boolean :0-'.strlen($t_boolean).' and set :'.(strlen($t_boolean)+4).'-'.(strlen($t_boolean)+4+strlen($t_set)).')',runTest($t_boolean.' && '.$t_set)) &&
  in_array(':'.(strlen($t_boolean)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got boolean :0-'.strlen($t_boolean).' and set :'.(strlen($t_boolean)+4).'-'.(strlen($t_boolean)+4+strlen($t_set)).')',runTest($t_boolean.' || '.$t_set)) &&
  in_array(':'.(strlen($t_set)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got set :0-'.strlen($t_set).' and reference :'.(strlen($t_set)+4).'-'.(strlen($t_set)+4+strlen($t_reference)).')',runTest($t_set.' && '.$t_reference)) &&
  in_array(':'.(strlen($t_set)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got set :0-'.strlen($t_set).' and reference :'.(strlen($t_set)+4).'-'.(strlen($t_set)+4+strlen($t_reference)).')',runTest($t_set.' || '.$t_reference)) &&
  in_array(':'.(strlen($t_set)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got set :0-'.strlen($t_set).' and numeric :'.(strlen($t_set)+4).'-'.(strlen($t_set)+4+strlen($t_numeric)).')',runTest($t_set.' && '.$t_numeric)) &&
  in_array(':'.(strlen($t_set)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got set :0-'.strlen($t_set).' and numeric :'.(strlen($t_set)+4).'-'.(strlen($t_set)+4+strlen($t_numeric)).')',runTest($t_set.' || '.$t_numeric)) &&
  in_array(':'.(strlen($t_set)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got set :0-'.strlen($t_set).' and string :'.(strlen($t_set)+4).'-'.(strlen($t_set)+4+strlen($t_string)).')',runTest($t_set.' && '.$t_string)) &&
  in_array(':'.(strlen($t_set)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got set :0-'.strlen($t_set).' and string :'.(strlen($t_set)+4).'-'.(strlen($t_set)+4+strlen($t_string)).')',runTest($t_set.' || '.$t_string)) &&
  in_array(':'.(strlen($t_set)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got set :0-'.strlen($t_set).' and boolean :'.(strlen($t_set)+4).'-'.(strlen($t_set)+4+strlen($t_boolean)).')',runTest($t_set.' && '.$t_boolean)) &&
  in_array(':'.(strlen($t_set)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got set :0-'.strlen($t_set).' and boolean :'.(strlen($t_set)+4).'-'.(strlen($t_set)+4+strlen($t_boolean)).')',runTest($t_set.' || '.$t_boolean)) &&
  in_array(':'.(strlen($t_set)+1).' Logical operator '."'&&'".' requires both of the operands to be boolean type (got set :0-'.strlen($t_set).' and set :'.(strlen($t_set)+4).'-'.(strlen($t_set)+4+strlen($t_set)).')',runTest($t_set.' && '.$t_set)) &&
  in_array(':'.(strlen($t_set)+1).' Logical operator '."'||'".' requires both of the operands to be boolean type (got set :0-'.strlen($t_set).' and set :'.(strlen($t_set)+4).'-'.(strlen($t_set)+4+strlen($t_set)).')',runTest($t_set.' || '.$t_set)));
unset($t_reference);
unset($t_numeric);
unset($t_string);
unset($t_boolean);
unset($t_set);

displayAssertion('Validating property reference',
  is_null(runTest('prop::eghetoseg in ("E")')['operands'][0]['prop_row']['DEFAULT_UNIT']) &&
  !is_null(runTest('prop::paraatereszto_kepesseg==0.15')['operands'][0]['prop_row']['DEFAULT_UNIT']) &&
  in_array(':6 Property label '."'definitely_invalid_reference'".' could not be resolved',runTest('prop::definitely_invalid_reference in ("E")')));

displayAssertion('Type check, operand type requirements, rejecting relational expressions without property reference',
  in_array(':1 Relational operator '."'<='".' requires one of the operands to be reference type (got numeric :0-1 and numeric :3-4)',runTest('1<=2')) &&
  in_array(':1 Relational operator '."'<'".' requires one of the operands to be reference type (got numeric :0-1 and numeric :2-3)',runTest('1<2')) &&
  in_array(':1 Relational operator '."'>='".' requires one of the operands to be reference type (got numeric :0-1 and numeric :3-4)',runTest('2>=1')) &&
  in_array(':1 Relational operator '."'>'".' requires one of the operands to be reference type (got numeric :0-1 and numeric :2-3)',runTest('2>1')) &&
  in_array(':1 Relational operator '."'=='".' requires one of the operands to be reference type (got numeric :0-1 and numeric :3-4)',runTest('1==1')) &&
  in_array(':3 Relational operator '."'=='".' requires one of the operands to be reference type (got string :0-3 and string :5-8)',runTest('"a"=="a"')) &&
  in_array(':1 Relational operator '."'!='".' requires one of the operands to be reference type (got numeric :0-1 and numeric :3-4)',runTest('1!=1')) &&
  in_array(':3 Relational operator '."'!='".' requires one of the operands to be reference type (got string :0-3 and string :5-8)',runTest('"a"!="a"')));

displayAssertion('Type check, operand type requirements, rejecting relational expressions (including set membership) between a numeric operand and a property reference that has no default unit to relate to',
  ($t_reference='prop::eghetoseg') &&
  in_array(':'.strlen($t_reference).' Relational operator '."'<='".' requires its reference type operand :0-'.strlen($t_reference).' to have default unit if it'."'".'s to be compared to the numeric type other operand',runTest($t_reference.'<=0')) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'<'".' requires its reference type operand :0-'.strlen($t_reference).' to have default unit if it'."'".'s to be compared to the numeric type other operand',runTest($t_reference.'<0')) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'>='".' requires its reference type operand :0-'.strlen($t_reference).' to have default unit if it'."'".'s to be compared to the numeric type other operand',runTest($t_reference.'>=0')) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'>'".' requires its reference type operand :0-'.strlen($t_reference).' to have default unit if it'."'".'s to be compared to the numeric type other operand',runTest($t_reference.'>0')) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'=='".' requires its reference type operand :0-'.strlen($t_reference).' to have default unit if it'."'".'s to be compared to the numeric type other operand',runTest($t_reference.'==0')) &&
  in_array(':'.strlen($t_reference).' Relational operator '."'!='".' requires its reference type operand :0-'.strlen($t_reference).' to have default unit if it'."'".'s to be compared to the numeric type other operand',runTest($t_reference.'!=0')) &&
  in_array(':'.(strlen($t_reference)+1).' Set membership operator '."'in'".' requires its reference type operand :0-'.strlen($t_reference).' to have default unit if it'."'".'s to be compared to any numeric type literal in the set',runTest($t_reference.' in (0)')));
unset($t_reference);

displayAssertion('Enforcing root expression to be boolean type',
  ($s='42') && in_array('Root expression must be boolean type (got numeric :0-'.strlen($s).')',runTest($s)) &&
  ($s='3.14') && in_array('Root expression must be boolean type (got numeric :0-'.strlen($s).')',runTest($s)) &&
  ($s='"example"') && in_array('Root expression must be boolean type (got string :0-'.strlen($s).')',runTest($s)) &&
  ($s='("B","C","D","E")') && in_array('Root expression must be boolean type (got set :0-'.strlen($s).')',runTest($s)));

?>
