$(document).ready(function() {
  
  /* Hit
   */
  function Hit(p) {
    var hit_card=$('<div class="card">');
    
    var header=$('<div class="card-header">').appendTo(hit_card);
    $('<button class="btn btn-primary float-right expand-collapse">').append('<i class="icon icon-arrow-down" aria-hidden="true">').appendTo(header);
    
    var customize_group=$('<div class="btn-group float-right">').appendTo(header);
    if (db.param_inf.hasOwnProperty(p.parseId().schema+'.'+p.parseId().db_id)) {
      $('<button class="btn customize-param">').text('Paraméterezés').prop('disabled',p instanceof P && !p.isNaturallyParameterized()).appendTo(customize_group);
    }
    if (p.isKit()) {
      $('<button class="btn customize-kit">').text('Rendszer-szerkesztő').appendTo(customize_group);
    }
    if (customize_group.children().length==0) {
      customize_group.remove();
    } else {
      customize_group.children().eq(0).detach().appendTo(customize_group);
    }
    
    $('<div class="card-title h5">').text(p.getLabel()).appendTo(header);
    $('<div class="card-subtitle text-gray">').text(p.getDesc()+(p.hasParamStr() ? ' - '+p.getParamStr() : '')).appendTo(header);
    
    if (p.isKit()) {
      $('<span class="label label-secondary">').text('rendszer '+Object.keys(p.getKit()).length+' termékből').appendTo(header.find('.card-title').text(header.find('.card-title').text()+' '));
    }
    
    var body=$('<div class="card-body">').appendTo(hit_card);
    
    if (!p.getProps().empty()) {
      $('<table class="table table-striped props">').append($('<thead>').append($('<tr>').append($('<th>').text('Jellemző megnevezése'),$('<th>').text('Teljesítmény'))),$('<tbody>').append(p.getProps().map(function(prop) {
        return $('<tr>').append(
          $('<td>').text(db.struct_inf[prop.getSchemaId()].properties[prop.getPropId()].name),
          $('<td>').text(prop.toString())
        );
      }))).appendTo(body);
    }
    if (!p.getInstrs().empty()) {
      $('<ul class="instrs">').append($('<strong>').text('Beépítési utasítások')).append(p.getInstrs().map(function(instr) {
        return $('<li>').text(instr.toString());
      })).appendTo(body);
    }
    if (p.isKit()) {
      $('<table class="table table-striped kit">').append($('<thead>').append($('<tr>').append($('<th>').text('Rendeltetés'),$('<th colspan="2">').text('Termék'))),$('<tbody>').append(db.struct_inf[p.parseId().schema].kit_functions.map(function(kit_function_data) {
        var ref_p=p.getKit().hasOwnProperty(kit_function_data.id) ? p.resolveRefP(p.getKit()[kit_function_data.id]) : undefined;
        return $('<tr>').append( //ref_p
          $('<td>').text(kit_function_data.name),
          $('<td>').text(ref_p ? ref_p.getLabel() : '(nincs termék)'),
          $('<td>').text(ref_p ? ref_p.getAltLabel() : '')
        );
      }))).appendTo(body);
    }
    
    hit_card.addClass('collapsed');
    body.hide();
    return hit_card;
  }
  
  function replace_Hit(new_p) {
    var new_hit_card=Hit(new_p);
    
    hits.replace(hits_resolve_map.get(this.get(0)),new_p);
    this.replaceWith(new_hit_card);
    hits_resolve_map.delete(this.get(0));
    hits_resolve_map.set(new_hit_card.get(0),new_p.getId());
    
    if (this.hasClass('expanded')) {
      new_hit_card.find('.expand-collapse').click();
    }
  }
  
  /* dialog support functions
   */
  function TwoStepDialogChain(options,defer) {
    return TwoStepDialogChain.RootDialog(options,defer);
  }
  
  TwoStepDialogChain.RootDialog=(function(dc,defer) {
    var modal_host=$('<div class="lvl-root">');
    
    var header=$('<div class="modal-header">').appendTo(modal_host);
    $('<div class="modal-title h5">').text(dc.title).appendTo(header);
    
    var body=$('<div class="modal-body">').appendTo(modal_host);
    var grid=$('<div class="columns">').appendTo(body);
    
    for (var i=0; i<dc.root_order.length; i++) {
      var option_card=$('<div class="card c-hand">').appendTo(grid).wrap('<div class="column col-4">');
      
      var option_header=$('<div class="card-header">').appendTo(option_card);
      $('<div class="card-title h6">').text(dc.root_map[dc.root_order[i]]).appendTo(option_header);
      $('<div class="card-subtitle text-gray">').text(dc.leaf_maps[dc.root_order[i]][dc.corrs[dc.root_order[i]]]).appendTo(option_header);
      
    }
    
    var footer=$('<div class="modal-footer">').appendTo(modal_host);
    $('<button class="btn btn-primary">').text('Alkalmaz').appendTo(footer);
    $('<button class="btn btn-link">').text('Mégse').appendTo(footer);
    
    var init_corrs=$.extend({},dc.corrs);
    
    grid.on('click','.card',function() {
      var option_card=$(this);
      var selected_root_option=dc.root_order[grid.children().index(option_card.parent())];
      
      var leaf_defer=$.Deferred();
      leaf_defer.done(function(selected_leaf_id) {
        
        dc.corrs[selected_root_option]=selected_leaf_id;
        dc.handleEvent('updateCorrs',modal_host,dc.corrs,selected_root_option);
        
        if (option_card.hasClass('pending-modifications')!=(dc.corrs[selected_root_option]!=init_corrs[selected_root_option])) {
          option_card.toggleClass('pending-modifications');
        }
        
        option_card.find('.card-subtitle').text(dc.leaf_maps[selected_root_option][selected_leaf_id]);
        
        display_modal(modal_host);
        
      }).fail(function() {
        
        display_modal(modal_host);
        
      });
      
      dc.handleEvent('clickRootOption',modal_host,option_card,selected_root_option);
      display_modal(TwoStepDialogChain.LeafDialog(dc,selected_root_option,leaf_defer));
      
    });
    
    footer.on('click','.btn',function() {
      
      if ($(this).hasClass('btn-primary')) {
        defer.resolve(dc.corrs);
        hide_modal(modal_host);
        
      } else {
        defer.reject();
        hide_modal(modal_host);
        
      }
      
    });
    
    dc.setCorrs=(function(new_corrs) {
      $.extend(dc.corrs,new_corrs);
      
      for (var i=0; i<dc.root_order.length; i++) {
        var option_card=grid.children().eq(i).find('.card');
        
        if (option_card.hasClass('pending-modifications')!=(new_corrs[dc.root_order[i]]!=init_corrs[dc.root_order[i]])) {
          option_card.toggleClass('pending-modifications');
        }
        
        option_card.find('.card-subtitle').text(dc.leaf_maps[dc.root_order[i]][new_corrs[dc.root_order[i]]]);
        
      }
      
    });
    
    dc.handleEvent('createRootDialog',modal_host,dc,defer);
    
    return modal_host;
  });
  
  TwoStepDialogChain.LeafDialog=(function(dc,selected_root_option,leaf_defer) {
    var modal_host=$('<div class="lvl-leaf">');
    
    var header=$('<div class="modal-header">').appendTo(modal_host);
    $('<div class="modal-title h5">').text(dc.title).appendTo(header);
    
    var body=$('<div class="modal-body">').appendTo(modal_host);
    $('<p>').text('Kiválasztott '+dc.root_noun+': ').append($('<span class="label">').text(dc.root_map[selected_root_option])).appendTo(body);
    var grid=$('<div class="columns">').appendTo(body);
    
    for (var i=0; i<dc.leaf_orders[selected_root_option].length; i++) {
      var option_card=$('<div class="card c-hand">').appendTo(grid).wrap('<div class="column col-4">');
      
      var option_header=$('<div class="card-header">').appendTo(option_card);
      $('<div class="card-title h6">').text(dc.leaf_maps[selected_root_option][dc.leaf_orders[selected_root_option][i]]).appendTo(option_header);
      
      if (dc.leaf_orders[selected_root_option][i]==dc.defaults[selected_root_option]) {
        $('<div class="card-subtitle text-gray">').text('alapértelmezett').appendTo(option_header);
      }
      
    }
    
    if (dc.corrs[selected_root_option]) {
      $('<small class="label label-primary float-right">').append('<i class="icon icon-check" aria-hidden="true">').prependTo(grid.find('.card').eq(dc.leaf_orders[selected_root_option].indexOf(dc.corrs[selected_root_option])).find('.card-header'));
    }
    
    var footer=$('<div class="modal-footer">').appendTo(modal_host);
    $('<button class="btn btn-link">').text('Másik '+dc.root_noun+' választása').appendTo(footer);
    
    grid.on('click','.card',function() {
      var option_card=$(this);
      var option_id=dc.leaf_orders[selected_root_option][grid.children().index(option_card.parent())];
      
      dc.handleEvent('clickLeafOption',modal_host,option_card,selected_root_option,option_id);
      leaf_defer.resolve(option_id);
      
    });
    
    footer.on('click','.btn',function() {
      
      leaf_defer.reject();
      
    });
    
    dc.handleEvent('createLeafDialog',modal_host,dc,selected_root_option,leaf_defer);
    
    return modal_host;
  });
  
  function display_modal(dialog) {
    var modals_container=$(document.body).children('.modals').last();
    if (modals_container.length==0) {
      modals_container=$('<div class="modals">').appendTo(document.body);
    }
    
    modals_container.children('.modal.active').removeClass('active');
    
    if (dialog.parent().length>0 && modals_container.children().filter(dialog.parent()).length>0) {
      dialog.parent().addClass('active');
      
    } else {
      dialog.addClass('modal-container').appendTo(modals_container);
      dialog.wrap('<div class="modal">').before('<div class="modal-overlay">').parent().addClass('active');
      
    }
  }
  function hide_modal(dialog) {
    dialog.parent().removeClass('active');
  }
  
  /* Param dialog
   */
  function ParamDialogChain(p,defer) {
    var param_inf_key=p.parseId().schema+'.'+p.parseId().db_id;
    
    var root_map={};
    var leaf_orders={};
    var leaf_maps={};
    
    var defaults={};
    var init_corrs={};
    
    for (var i=0; i<db.param_inf[param_inf_key].params_order.length; i++) {
      var param_id=db.param_inf[param_inf_key].params_order[i];
      
      root_map[param_id]=db.param_inf[param_inf_key].params[param_id].name;
      
      leaf_maps[param_id]={};
      leaf_orders[param_id]=db.param_inf[param_inf_key].params[param_id].options.map(function(paramopt) {
        return paramopt.paramopt_id;
      });
      
      if (!db.param_inf[param_inf_key].params[param_id].default_opt) {
        leaf_maps[param_id]['__undefined']='(nincs megadva)';
        leaf_orders[param_id].unshift('__undefined');
      }
      
      for (var j=0; j<db.param_inf[param_inf_key].params[param_id].options.length; j++) {
        var paramopt=db.param_inf[param_inf_key].params[param_id].options[j];
        leaf_maps[param_id][paramopt.paramopt_id]=paramopt.value;
      }
      
      defaults[param_id]=db.param_inf[param_inf_key].params[param_id].default_opt || '__undefined';
      init_corrs[param_id]=p instanceof P ? p._param.hasOwnProperty(param_id) ? p._param[param_id] : '__undefined' : '__undefined';
      
    }
    
    var dc_defer=$.Deferred();
    dc_defer.done(function(new_corrs) {
      
      for (var i=0; i<db.param_inf[param_inf_key].params_order.length; i++) {
        if (new_corrs[db.param_inf[param_inf_key].params_order[i]]=='__undefined') {
          delete new_corrs[db.param_inf[param_inf_key].params_order[i]];
        }
      }
      
      defer.resolve($.extend(new ParamStruct(),new_corrs));
      
    }).fail(function() {
      defer.reject();
      
    });
    
    return TwoStepDialogChain({
      title: p.getLabel()+' paraméterezés',
      root_noun: 'paraméter',
      
      root_map: root_map,
      root_order: db.param_inf[param_inf_key].params_order,
      
      leaf_maps: leaf_maps,
      leaf_orders: leaf_orders,
      
      defaults: defaults,
      corrs: init_corrs,
      
      handleEvent: (function(ev_name) {})
    },dc_defer);
    
  }
  
  /* Kit dialog
   */
  function CustomizeKit(p,defer) {
    var schema=p.parseId().schema;
    
    var root_map={};
    var leaf_orders={};
    var leaf_maps={};
    
    var defaults={};
    var init_corrs={};
    
    var default_parambuild;
    if (p instanceof P && !p.isNaturallyParameterized()) {
      default_parambuild=new P(p._original,p.getDefaultParamStruct());
    }
    
    for (var i=0; i<db.struct_inf[schema].kit_functions.length; i++) {
      var kit_function_data=db.struct_inf[schema].kit_functions[i];
      
      root_map[kit_function_data.id]=kit_function_data.name;
      
      leaf_maps[kit_function_data.id]={};
      leaf_orders[kit_function_data.id]=$.merge([],db.struct_inf[schema].kf_map[p.getKf()][kit_function_data.id]);
      
      leaf_maps[kit_function_data.id]['__undefined']='(nincs termék)';
      leaf_orders[kit_function_data.id].unshift('__undefined');
      
      for (var j=0; j<db.struct_inf[schema].kf_map[p.getKf()][kit_function_data.id].length; j++) {
        var refp_id=db.struct_inf[schema].kf_map[p.getKf()][kit_function_data.id][j];
        leaf_maps[kit_function_data.id][refp_id]=p.resolveRefP(refp_id).getLabel();
      }
      
      defaults[kit_function_data.id]=(p instanceof P ? p.isNaturallyParameterized() ? p._parambuild : default_parambuild : p).getKit()[kit_function_data.id] || '__undefined';
      init_corrs[kit_function_data.id]=(p instanceof P ? p._parambuild : p).getKit()[kit_function_data.id] || '__undefined';
      
    }
    if (default_parambuild) {
      default_parambuild=undefined;
    }
    
    var dc_defer=$.Deferred();
    dc_defer.done(function(new_corrs) {
      
      if (root_dialog.find('.form-switch input[type="checkbox"]').prop('checked')) {
        if (p instanceof P && !p.isNaturallyParameterized()) {
          defer.resolve(p.getDefaultParamStruct());
        }
        
      } else {
        
        var modifications=[];
        
        for (var i=0; i<db.struct_inf[schema].kit_functions.length; i++) {
          var kit_function_data=db.struct_inf[schema].kit_functions[i];
          
          if (((p instanceof P ? p._parambuild : p).getKit()[kit_function_data.id] || '__undefined')!=new_corrs[kit_function_data.id]) {
            modifications.push(new ParamOverride2({
              type: 'kit',
              operation: 'AUTO',
              function: kit_function_data.id,
              refp_id: new_corrs[kit_function_data.id]!='__undefined' ? new_corrs[kit_function_data.id] : ''
            }));
          }
          
        }
        
        if (modifications.length>0) {
          var prop_ids=Object.keys(db.struct_inf[schema].properties);
          for (var i=0; i<prop_ids.length; i++) {
            modifications.push(new ParamOverride2({
              type: 'prop',
              operation: 'REMOVE',
              prop_id: prop_ids[i],
              schema_id: schema
            }));
          }
        }
        
        if (modifications.length>0) {
          defer.resolve(modifications);
        } else {
          defer.reject();
        }
        
      }
      
    }).fail(function() {
      defer.reject();
      
    });
    
    var root_dialog;
    return (root_dialog=TwoStepDialogChain({
      title: p.getLabel()+' rendszerelemek',
      root_noun: 'rendszerelem',
      
      root_map: root_map,
      root_order: db.struct_inf[schema].kit_functions.map(function(kit_function_data) {
        return kit_function_data.id;
      }),
      
      leaf_maps: leaf_maps,
      leaf_orders: leaf_orders,
      
      defaults: defaults,
      corrs: init_corrs,
      
      handleEvent: (function(ev_name) {
        
        if (ev_name=='createRootDialog') {
          var modal_host=arguments[1];
          var dc=arguments[2];
          var lock_checkbox=$('<div class="form-group">').append(
            $('<label class="form-switch">').append(
              $('<input type="checkbox">').prop('checked',p instanceof P ? p.isNaturallyParameterized() : true),
              '<i class="form-icon">','<strong>','<br>'
            )
          ).prependTo(modal_host.find('.modal-body')).find('input');
          
          modal_host.find('.columns .card').on('click',(function() {
            if (lock_checkbox.prop('checked')) {
              return false;
            }
          }))
          
          lock_checkbox.on('change',function() {
            var form_switch=lock_checkbox.parent();
            
            var from_switch_children=form_switch.children(':not(br~em)').detach();
            from_switch_children.filter('strong').text($(this).prop('checked')
              ? 'A rendszer jelenleg zárolt'
              : 'Módosítások zárolása feloldva'
            );
            form_switch.text($(this).prop('checked')
              ? 'A gyártó által javasolt kombinációk között Paraméterezés útján választhat VAGY oldja fel a zárolást a csúszkával!'
              : 'Egyedi termékrendszer megalkotásához kérje ki a gyártó/forgalmazó további tájékoztatását!'
            );
            if ($(this).prop('checked')) {
              var text_node=form_switch.contents().get(0);
              var param_text=text_node.splitText(text_node.textContent.indexOf('Paraméterezés'));
              param_text.splitText('Paraméterezés'.length);
              $(param_text).wrap('<em>');
            }
            form_switch.prepend(from_switch_children);
            
            modal_host.find('.card').toggleClass('disabled c-hand');
            
            if ($(this).prop('checked')) {
              dc.setCorrs(defaults);
              
              modal_host.find('.btn-primary').prop('disabled',!(p instanceof P) || p.isNaturallyParameterized());
              
            } else {
              
              modal_host.find('.btn-primary').prop('disabled',false);
              
            }
            
          }).change();
          
        } else if (ev_name=='updateCorrs') {
          var modal_host=arguments[1];
          var corrs=arguments[2];
          
          var apply_button=modal_host.find('.btn-primary');
          if (db.struct_inf[schema].kit_functions.some(function(kit_function_data) {
            return corrs[kit_function_data.id]!='__undefined';
          })!=!apply_button.prop('disabled')) {
            apply_button.prop('disabled',!apply_button.prop('disabled'));
          }
          
        }
        
      })
    },dc_defer));
    
  }
  
  var hits=new DList();
  var hits_resolve_map;
  
  window.mnLoadHits=(function(hits_arr) {
    
    mnParseHitsArray.call(hits,hits_arr);
    hits_resolve_map=new WeakMap();
    
    if($('form+.divider+*:not(script)').length>0) {
      $('form+.divider~*:not(script)').remove();
    }
    
    if (hits.empty()) {
      
      if (history.state) {
        $('form+.divider~script').eq(0).before(
          $('<div class="empty">').append(
            $('<div class="empty-icon">').append('<i class="icon icon-3x icon-search" aria-hidden="true">'),
            $('<p class="empty-title h5">').text('A keresés eredménye üres'),
            $('<p class="empty-subtitle">').text('A szerkezeti elem csoporthoz nem tartozik termék vagy a szűrési feltétel mindegyiket kiszűrte.')
          )
        );
        
      }
      
      return;
    }
    
    hits.filter(function(p) {
      return db.param_inf.hasOwnProperty(p.getId()) && p.hasAnyParamWDefault();
    }).all(function(p) {
      hits.replace(p.getId(),new P(p,p.getDefaultParamStruct()))
    });
    
    hits.all(function(p) {
      var hit_card_element=Hit(p).get(0);
      $('form+.divider~script').eq(0).before(hit_card_element);
      if ($('form .expand-collapse-all .icon').hasClass('icon-arrow-up')) {
        $(hit_card_element).find('.expand-collapse').click();
      }
      hits_resolve_map.set(hit_card_element,p.getId());
    });
    
  });
  
  $(document.body).on('click','form+.divider~*:not(script) .expand-collapse',function() {
    $(this).parents('.card-header').next().toggle();
    $(this).parents('.card').toggleClass('collapsed expanded');
    $(this).find('.icon').toggleClass('icon-arrow-down icon-arrow-up');
  });
  $('form .expand-collapse-all').on('click',function() {
    $('form+.divider~*:not(script)').filter(':not(.'+($(this).find('.icon').hasClass('icon-arrow-down') ? 'expanded' : 'collapsed')+')').find('.expand-collapse').click();
    $(this).find('.icon').toggleClass('icon-arrow-down icon-arrow-up');
    return false;
  });
  
  $(document.body).on('click','form+.divider~*:not(script) .customize-param',function() {
    var hit_card=$(this).parents('.card');
    var p=hits.get(hits_resolve_map.get(hit_card.get(0)));
    
    var modal_defer=$.Deferred();
    display_modal(ParamDialogChain(p,modal_defer));
    
    modal_defer.done(function(new_param) {
      replace_Hit.call(hit_card,new P(p instanceof P ? p._original : p,new_param));
    });
    
  });
  
  $(document.body).on('click','form+.divider~*:not(script) .customize-kit',function() {
    var hit_card=$(this).parents('.card');
    var p=hits.get(hits_resolve_map.get(hit_card.get(0)));
    
    var modal_defer=$.Deferred();
    display_modal(CustomizeKit(p,modal_defer));
    
    modal_defer.done(function(new_param) {
      replace_Hit.call(hit_card,new P(p instanceof P ? p._original : p,new_param));
    });
    
  });
  
  $(document.body).on('click','.modal.active>.modal-overlay',function() {
    hide_modal($(this).next('.modal-container'));
    return false;
  });
  $(document.body).on('keyup',function() {
    if (arguments[0].which==27 && $('.modal.active').length>0) {
      $('.modal.active>.modal-overlay').click();
    }
  });
  
});
