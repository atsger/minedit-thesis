-- burkolat típusa

SET @PARAM_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA=SHA1('botament melegburkolati rendszer burkolat típusa');
INSERT INTO PARAM_POOL(PARAM_ID,PARAM_NAME,PARAM_STR_FORMAT) VALUES
(@PARAM_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA,'Burkolat típusa','[v] burkolathoz');

SET @PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_TEXTIL=SHA1('botament melegburkolati rendszer burkolat típusa textil');
INSERT INTO PARAMOPT_POOL(PARAM_ID,PARAMOPT_ID,PARAMOPT_NAME) VALUES
(@PARAM_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA,@PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_TEXTIL,'textil');

SET @PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_PVC_GUMI=SHA1('botament melegburkolati rendszer burkolat típusa pvc/gumi');
INSERT INTO PARAMOPT_POOL(PARAM_ID,PARAMOPT_ID,PARAMOPT_NAME) VALUES
(@PARAM_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA,@PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_PVC_GUMI,'PVC/gumi');

SET @PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_LINÓLEUM=SHA1('botament melegburkolati rendszer burkolat típusa linóleum');
INSERT INTO PARAMOPT_POOL(PARAM_ID,PARAMOPT_ID,PARAMOPT_NAME) VALUES
(@PARAM_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA,@PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_LINÓLEUM,'linóleum');

SET @PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_PARKETTA=SHA1('botament melegburkolati rendszer burkolat típusa parketta');
INSERT INTO PARAMOPT_POOL(PARAM_ID,PARAMOPT_ID,PARAMOPT_NAME) VALUES
(@PARAM_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA,@PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_PARKETTA,'parketta');

INSERT INTO DOP_PARAMS(SCHEMA_ID,PRODUCT_ID,PARAM_ID,SORT_INDEX,DEFAULT_OPT_ID) VALUES
(@SCHEMA_MELEGBURKOLAT,@PRODUCT_BOTAMENT_MELEGBURKOLAT,@PARAM_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA,0,@PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_TEXTIL);

SET @PARAM_HASH_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_TEXTIL=SHA1(CONCAT(@PARAM_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA,'=',@PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_TEXTIL));
INSERT INTO KIT_CONTENT(PARAM_HASH,KIT_SCHEMA_ID,KIT_PRODUCT_ID,KIT_FUNCTION_ID,SCHEMA_ID,PRODUCT_ID) VALUES
(CONCAT(@RULE_SIMPLE,@PARAM_HASH_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_TEXTIL),
 @SCHEMA_MELEGBURKOLAT,@PRODUCT_BOTAMENT_MELEGBURKOLAT,@SCHEMA_MELEGBURKOLAT_KIT_FUNCTION_RAGASZTÓ,
 @SCHEMA_RAGASZTÓ,@PRODUCT_T350);

SET @PARAM_HASH_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_PVC_GUMI=SHA1(CONCAT(@PARAM_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA,'=',@PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_PVC_GUMI));
INSERT INTO KIT_CONTENT(PARAM_HASH,KIT_SCHEMA_ID,KIT_PRODUCT_ID,KIT_FUNCTION_ID,SCHEMA_ID,PRODUCT_ID) VALUES
(CONCAT(@RULE_SIMPLE,@PARAM_HASH_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_PVC_GUMI),
 @SCHEMA_MELEGBURKOLAT,@PRODUCT_BOTAMENT_MELEGBURKOLAT,@SCHEMA_MELEGBURKOLAT_KIT_FUNCTION_RAGASZTÓ,
 @SCHEMA_RAGASZTÓ,@PRODUCT_K530);

SET @PARAM_HASH_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_LINÓLEUM=SHA1(CONCAT(@PARAM_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA,'=',@PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_LINÓLEUM));
INSERT INTO KIT_CONTENT(PARAM_HASH,KIT_SCHEMA_ID,KIT_PRODUCT_ID,KIT_FUNCTION_ID,SCHEMA_ID,PRODUCT_ID) VALUES
(CONCAT(@RULE_SIMPLE,@PARAM_HASH_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_LINÓLEUM),
 @SCHEMA_MELEGBURKOLAT,@PRODUCT_BOTAMENT_MELEGBURKOLAT,@SCHEMA_MELEGBURKOLAT_KIT_FUNCTION_RAGASZTÓ,
 @SCHEMA_RAGASZTÓ,@PRODUCT_L610);

SET @PARAM_HASH_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_PARKETTA=SHA1(CONCAT(@PARAM_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA,'=',@PARAMOPT_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_PARKETTA));
INSERT INTO KIT_CONTENT(PARAM_HASH,KIT_SCHEMA_ID,KIT_PRODUCT_ID,KIT_FUNCTION_ID,SCHEMA_ID,PRODUCT_ID) VALUES
(CONCAT(@RULE_SIMPLE,@PARAM_HASH_BOTAMENT_MELEGBURKOLAT_BURKOLAT_TÍPUSA_PARKETTA),
 @SCHEMA_MELEGBURKOLAT,@PRODUCT_BOTAMENT_MELEGBURKOLAT,@SCHEMA_MELEGBURKOLAT_KIT_FUNCTION_RAGASZTÓ,
 @SCHEMA_RAGASZTÓ,@PRODUCT_P450);
