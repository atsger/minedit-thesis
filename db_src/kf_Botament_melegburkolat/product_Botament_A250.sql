SET @PRODUCT_A250=SHA1('a250');
INSERT INTO PRODUCT_POOL(VENDOR,PRODUCT_ID,PRODUCT_NAME,LINK) VALUES
(@VENDOR_BOTAMENT,@PRODUCT_A250,'A 250',NULL);

INSERT INTO DOP(SCHEMA_ID,PRODUCT_ID,DESCRIPTION,DOC_LINK) VALUES
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A250,'Cementkötésű vastag önterülő kiegyenlítő','https://botament.com/fileadmin/user_upload/pdfs+buttons/Declaration_of_performance/de/Leistungserklaerung_BOTAMENT_A250.pdf');
INSERT INTO DOP_PROPERTIES(SCHEMA_ID,PRODUCT_ID,PROPERTY_ID,VALUE,UNIT_MULTIPLIER,CONCAT_PATTERN) VALUES
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A250,@SCHEMA_FELÜLETKIEGYENLÍTŐ_RETE,         40,     1,'[v] mm'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A250,@SCHEMA_FELÜLETKIEGYENLÍTŐ_EGHE,  'A2fl-s1',  NULL,'[v], EN 13813:2002'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A250,@SCHEMA_FELÜLETKIEGYENLÍTŐ_KORR,       'Ct',  NULL,'[v], EN 13813:2002'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A250,@SCHEMA_FELÜLETKIEGYENLÍTŐ_NYOM,      'C35',  NULL,'[v], EN 13813:2002'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A250,@SCHEMA_FELÜLETKIEGYENLÍTŐ_HAJL,       'F7',  NULL,'[v], EN 13813:2002'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A250,@SCHEMA_FELÜLETKIEGYENLÍTŐ_SZIL,       NULL,  NULL,'~50-90 perc után, ~80-120 percig, EN 196');

INSERT INTO DOP_INSTRUCTIONS(SCHEMA_ID,PRODUCT_ID,SORT_INDEX,INSTRUCTION) VALUES
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A250,0,'Keverési arány: ~5.0 l víz / 25 kg, anyagszükséglet: ~1.7 kg/m2/mm'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A250,1,'Feldolgozható ~30 percig, ~2-3 óra múlva járható, 20 mm rétegvastagságig ~24 óra múlva / 20 mm rétegvastagság fölött ~96 óra múlva burkolható parkettával, 20 mm rétegvastagságig ~24 óra múlva / 20 mm rétegvastagság fölött ~48 óra múlva burkolható textillel vagy egyéb rugalmas burkolattal, ~7 nap múlva teljesen teherbíró');
