SET @PRODUCT_A220=SHA1('a220');
INSERT INTO PRODUCT_POOL(VENDOR,PRODUCT_ID,PRODUCT_NAME,LINK) VALUES
(@VENDOR_BOTAMENT,@PRODUCT_A220,'A 220',NULL);

INSERT INTO DOP(SCHEMA_ID,PRODUCT_ID,DESCRIPTION,DOC_LINK) VALUES
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A220,'Cementkötésű önterülő kiegyenlítő','https://botament.com/fileadmin/user_upload/pdfs+buttons/Declaration_of_performance/de/Leistungserklaerung_BOTAMENT_A220.pdf');
INSERT INTO DOP_PROPERTIES(SCHEMA_ID,PRODUCT_ID,PROPERTY_ID,VALUE,UNIT_MULTIPLIER,CONCAT_PATTERN) VALUES
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A220,@SCHEMA_FELÜLETKIEGYENLÍTŐ_RETE,         20,     1,'[v] mm'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A220,@SCHEMA_FELÜLETKIEGYENLÍTŐ_EGHE,  'A2fl-s1',  NULL,'[v], EN 13813:2002'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A220,@SCHEMA_FELÜLETKIEGYENLÍTŐ_KORR,       'Ct',  NULL,'[v], EN 13813:2002'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A220,@SCHEMA_FELÜLETKIEGYENLÍTŐ_NYOM,      'C35',  NULL,'[v], EN 13813:2002'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A220,@SCHEMA_FELÜLETKIEGYENLÍTŐ_HAJL,       'F7',  NULL,'[v], EN 13813:2002'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A220,@SCHEMA_FELÜLETKIEGYENLÍTŐ_SZIL,       NULL,  NULL,'~40-70 perc után, ~50-100 percig, EN 196');

INSERT INTO DOP_INSTRUCTIONS(SCHEMA_ID,PRODUCT_ID,SORT_INDEX,INSTRUCTION) VALUES
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A220,0,'Keverési arány: ~6.5 l víz / 25 kg, anyagszükséglet: ~1.5 kg/m2/mm'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A220,1,'10 mm rétegvastagság fölött 30%-os kvarchomokkal (0-2 mm szemcseméret) terítendő, anyagszükséglet: kb. 7.5 kg homok / 25 kg'),
(@SCHEMA_FELÜLETKIEGYENLÍTŐ,@PRODUCT_A220,2,'Feldolgozható ~20-40 percig, ~3 óra múlva járható, 5 mm rétegvastagságig ~24 óra múlva / 5 mm rétegvastagság fölött ~48 óra múlva burkolható parkettával, 10 mm rétegvastagságig ~24 óra múlva / 10 mm rétegvastagság fölött ~48 óra múlva burkolható textillel vagy egyéb rugalmas burkolattal, ~7 nap múlva teljesen teherbíró');
