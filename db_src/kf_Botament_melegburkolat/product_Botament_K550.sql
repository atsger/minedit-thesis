SET @PRODUCT_K550=SHA1('k550');
INSERT INTO PRODUCT_POOL(VENDOR,PRODUCT_ID,PRODUCT_NAME,LINK) VALUES
(@VENDOR_BOTAMENT,@PRODUCT_K550,'K 550',NULL);

INSERT INTO DOP(SCHEMA_ID,PRODUCT_ID,DESCRIPTION,DOC_LINK) VALUES
(@SCHEMA_RAGASZTÓ,@PRODUCT_K550,'PVC burkolat nedves- és kontaktragasztó',NULL);

INSERT INTO DOP_INSTRUCTIONS(SCHEMA_ID,PRODUCT_ID,SORT_INDEX,INSTRUCTION) VALUES
(@SCHEMA_RAGASZTÓ,@PRODUCT_K550,0,'Anyagszükséglet: A2 kenőlappal ~280 g/m2, A3 kenőlappal ~300 g/m2, rövid bárányszőrű hengerrel ~200 g/m2'),
(@SCHEMA_RAGASZTÓ,@PRODUCT_K550,1,'Nedves ragasztás esetén ~10-20 perc alatt szárad (~40 perc nyitási idő), tapadó- és kontaktragasztás esetén ~30-60 perc alatt szárad (~2 óra nyitási idő), ~24 óra múlva járható, ~72 óra múlva éri el a végső kötési szilárdságot');
