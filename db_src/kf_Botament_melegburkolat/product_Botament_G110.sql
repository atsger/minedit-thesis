SET @PRODUCT_G110=SHA1('g110');
INSERT INTO PRODUCT_POOL(VENDOR,PRODUCT_ID,PRODUCT_NAME,LINK) VALUES
(@VENDOR_BOTAMENT,@PRODUCT_G110,'G 110',NULL);

INSERT INTO DOP(SCHEMA_ID,PRODUCT_ID,DESCRIPTION,DOC_LINK) VALUES
(@SCHEMA_ALAPOZÓ,@PRODUCT_G110,'Univerzális alapozó',NULL);

INSERT INTO DOP_INSTRUCTIONS(SCHEMA_ID,PRODUCT_ID,SORT_INDEX,INSTRUCTION) VALUES
(@SCHEMA_ALAPOZÓ,@PRODUCT_G110,0,'Porózus/erősen nedvszívó felületek kezeléséhez max. két rétegben 1:4 arányban vízzel hígítandó, anyagszükséglet rétegenként: ~30-40 g/m2'),
(@SCHEMA_ALAPOZÓ,@PRODUCT_G110,1,'Közepesen nedvszívó felületek kezeléséhez egy rétegben, 1:2 arányban vízzel hígítandó, anyagszükséglet: ~40-50 g/m2'),
(@SCHEMA_ALAPOZÓ,@PRODUCT_G110,2,'Gyengén nedvszívó felületek kezeléséhez egy rétegben, 1:1 arányban vízzel hígítandó, anyagszükséglet: ~60-70 g/m2'),
(@SCHEMA_ALAPOZÓ,@PRODUCT_G110,3,'Várakozási idő a felületkiegyenlítő felhordása előtt cementesztrich/beton felületen: ~30 perc, kalcium-szulfát kötőanyagú esztrich felületen: >=24 óra, fa/szárazesztrich felületen: ~24 óra');

