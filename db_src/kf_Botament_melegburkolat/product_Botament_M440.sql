SET @PRODUCT_M440=SHA1('m440');
INSERT INTO PRODUCT_POOL(VENDOR,PRODUCT_ID,PRODUCT_NAME,LINK) VALUES
(@VENDOR_BOTAMENT,@PRODUCT_M440,'M 440',NULL);

INSERT INTO DOP(SCHEMA_ID,PRODUCT_ID,DESCRIPTION,DOC_LINK) VALUES
(@SCHEMA_RAGASZTÓ,@PRODUCT_M440,'Polyurethan ragasztó',NULL);

INSERT INTO DOP_INSTRUCTIONS(SCHEMA_ID,PRODUCT_ID,SORT_INDEX,INSTRUCTION) VALUES
(@SCHEMA_RAGASZTÓ,@PRODUCT_M440,0,'Keverési arány: 9 (A) : 1 (B), anyagszükséglet: A2 kenőlappal ~300 g/m2, A3 kenőlappal ~600 g/m2, B3 kenőlappal ~800-1100 g/m2, B11 kenőlappal ~1000-1300 g/m2'),
(@SCHEMA_RAGASZTÓ,@PRODUCT_M440,1,'~30 perc nyitási idő, parketta esetén ~24 óra múlva / egyéb burkolat esetén ~8 óra múlva járható, a burkolat >=24 óra múlva felületkezelhető/csiszolható, ~72 óra múlva éri el a végső kötési szilárdságot');
