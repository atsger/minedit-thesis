SET @PRODUCT_K572=SHA1('k572');
INSERT INTO PRODUCT_POOL(VENDOR,PRODUCT_ID,PRODUCT_NAME,LINK) VALUES
(@VENDOR_BOTAMENT,@PRODUCT_K572,'K 572',NULL);

INSERT INTO DOP(SCHEMA_ID,PRODUCT_ID,DESCRIPTION,DOC_LINK) VALUES
(@SCHEMA_RAGASZTÓ,@PRODUCT_K572,'Hengerelhető ragasztó PVC dizájn burkolathoz',NULL);

INSERT INTO DOP_INSTRUCTIONS(SCHEMA_ID,PRODUCT_ID,SORT_INDEX,INSTRUCTION) VALUES
(@SCHEMA_RAGASZTÓ,@PRODUCT_K572,0,'Anyagszükséglet: ~200 g/m2 (műanyag hengerrel)'),
(@SCHEMA_RAGASZTÓ,@PRODUCT_K572,1,'~30 perc alatt szárad, <=24 óra szerelési idő, ragasztás után azonnal járható és igénybe vehető'),
(@SCHEMA_RAGASZTÓ,@PRODUCT_K572,2,'Alkalmazható fűtött felületeken 28 °C-ig, és EN 685 szerint meghatározott 23, 32, 41 osztályú padlóburkolatokhoz');
