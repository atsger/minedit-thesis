SET @PRODUCT_M365=SHA1('m365');
INSERT INTO PRODUCT_POOL(VENDOR,PRODUCT_ID,PRODUCT_NAME,LINK) VALUES
(@VENDOR_BOTAMENT,@PRODUCT_M365,'M 365',NULL);

INSERT INTO DOP(SCHEMA_ID,PRODUCT_ID,DESCRIPTION,DOC_LINK) VALUES
(@SCHEMA_RAGASZTÓ,@PRODUCT_M365,'Multi ragasztó',NULL);

INSERT INTO DOP_INSTRUCTIONS(SCHEMA_ID,PRODUCT_ID,SORT_INDEX,INSTRUCTION) VALUES
(@SCHEMA_RAGASZTÓ,@PRODUCT_M365,0,'Anyagszükséglet: A1 kenőlappal ~320 g/m2, A2 kenőlappal ~300 g/m2, B1 kenőlappal ~400 g/m2, B2 kenőlappal ~550 g/m2'),
(@SCHEMA_RAGASZTÓ,@PRODUCT_M365,1,'Páraáteresztő burkolat esetén ~5-10 perc alatt / párazáró burkolat esetén ~10-15 perc alatt szárad (~30 perc nyitási idő), ~24 óra múlva járható, ~72 óra múlva éri el a végső kötési szilárdságot');
