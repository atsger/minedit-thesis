SET @SCHEMA_ALÁTÉTHÉJAZAT=SHA1('alátéthéjazat');
INSERT INTO SCHEMA_POOL(PARENT_SCHEMA,SCHEMA_ID,NAME) VALUES
(NULL,@SCHEMA_ALÁTÉTHÉJAZAT,'Alátéthéjazat');

SET @SCHEMA_ALÁTÉTHÉJAZAT_EGHE=SHA1('alátéthéjazat éghetőség');
INSERT INTO PROPERTY_POOL(SCHEMA_ID,PROPERTY_ID,NAME) VALUES
(@SCHEMA_ALÁTÉTHÉJAZAT,@SCHEMA_ALÁTÉTHÉJAZAT_EGHE,'Éghetőség');

SET @SCHEMA_ALÁTÉTHÉJAZAT_VIZZ=SHA1('alátéthéjazat vízzáróság');
INSERT INTO PROPERTY_POOL(SCHEMA_ID,PROPERTY_ID,NAME) VALUES
(@SCHEMA_ALÁTÉTHÉJAZAT,@SCHEMA_ALÁTÉTHÉJAZAT_VIZZ,'Vízzáróság');

SET @SCHEMA_ALÁTÉTHÉJAZAT_SSZH=SHA1('alátéthéjazat szakítószilárdság hosszirányban');
INSERT INTO PROPERTY_POOL(SCHEMA_ID,PROPERTY_ID,NAME,DEFAULT_UNIT) VALUES
(@SCHEMA_ALÁTÉTHÉJAZAT,@SCHEMA_ALÁTÉTHÉJAZAT_SSZH,'Szakitószilárdság hosszirányban','N/5cm');

SET @SCHEMA_ALÁTÉTHÉJAZAT_SSZK=SHA1('alátéthéjazat szakítószilárdság keresztirányban');
INSERT INTO PROPERTY_POOL(SCHEMA_ID,PROPERTY_ID,NAME,DEFAULT_UNIT) VALUES
(@SCHEMA_ALÁTÉTHÉJAZAT,@SCHEMA_ALÁTÉTHÉJAZAT_SSZK,'Szakitószilárdság keresztirányban','N/5cm');

SET @SCHEMA_ALÁTÉTHÉJAZAT_PARA=SHA1('alátéthéjazat páraáteresztő képesség');
INSERT INTO PROPERTY_POOL(SCHEMA_ID,PROPERTY_ID,NAME,DEFAULT_UNIT) VALUES
(@SCHEMA_ALÁTÉTHÉJAZAT,@SCHEMA_ALÁTÉTHÉJAZAT_PARA,'Páraáteresztő képesség','m');

SET @SCHEMA_ALÁTÉTHÉJAZAT_TOME=SHA1('alátéthéjazat tömeg');
INSERT INTO PROPERTY_POOL(SCHEMA_ID,PROPERTY_ID,NAME,DEFAULT_UNIT) VALUES
(@SCHEMA_ALÁTÉTHÉJAZAT,@SCHEMA_ALÁTÉTHÉJAZAT_TOME,'Tömeg','g/m2');
