$(document).ready(function() {
  
  function insertTreeOptions(of_arr,pre_text) {
    for (var i=0; i<of_arr.length; i++) {
      if (of_arr[i].hasOwnProperty('children')) {
        insertTreeOptions(of_arr[i].children,pre_text+' > '+of_arr[i].name);
      } else {
        $('<option>')
          .attr('value',of_arr[i].id)
          .text((pre_text+' > '+of_arr[i].name).replace(/^ > /,''))
          .appendTo($('#in-schema-group'));
      }
    }
  }
  
  var schema_defer;
  var prop_resolve_map;
  
  /* onSchemaChange hnd
   */
  $('#in-schema-group').on('change',function() {
    var schema_defer_disabled=$('.schema-props, .schema-props+button').prop('disabled',true);
    var rq=mnApi.requestSchema($(this).val());
    schema_defer=rq.when.done(function(schema) {
      prop_resolve_map={};
      $('.schema-props>option').remove();
      var prop_ids=Object.keys(schema.properties);
      
      if (prop_ids.length==0) {
        $('<option>').text('A kiválasztott csoporthoz nem tartozik jellemző').appendTo($('.schema-props'));
        schema_defer_disabled.filter('')
        return;
      }
      
      for (var i=0; i<prop_ids.length; i++) {
        var option=$('<option>')
          .attr('value',schema.properties[prop_ids[i]].name
            .toLowerCase()
            .replaceAll('á','a')
            .replaceAll('é','e')
            .replaceAll('í','i')
            .replaceAll(/[őöó]/g,'o')
            .replaceAll(/[űüú]/g,'u')
            .replaceAll(/[^a-z0-9]/g,'_')
            .replaceAll(/_{2,}/g,'_')
            .replace('^_','').replace(/_$/,'')
          )
          .text(schema.properties[prop_ids[i]].name+(schema.properties[prop_ids[i]].default_unit ? ' ('+schema.properties[prop_ids[i]].default_unit+')' : ''))
          .appendTo($('.schema-props'));
        prop_resolve_map[option.attr('value')]=prop_ids[i];
      }
      schema_defer_disabled.prop('disabled',false);
    });
  });
  
  /* insert schema tree options
   */
  var schema_tree_defer_disabled=$('#in-schema-group, form .btn-primary').prop('disabled',true);
  var schema_tree_defer=mnApi.requestSchemaTree().done(function(schema_tree) {
    insertTreeOptions(schema_tree,'');
    if (!history.state) {
      $('#in-schema-group').change();
    }
    schema_tree_defer_disabled.prop('disabled',false);
  });
  
  /* parsing error feedback
   */
  function err_log(err_msg,pos) {
    var err_container=$('#condition').next('.form-input-hint');
    if (err_container.length==0) {
      err_container=$('<p class="form-input-hint">').insertAfter($('#condition'));
    }
    var contents=err_container.contents().detach();
    err_container.text(err_msg);
    for (var replace_mark_regex=/:\d+(?:\-\d+)?/g, match; (match=replace_mark_regex.exec(err_msg))!==null; ) {
      var err_nodes=err_container.contents();
      var replace_text_node=err_nodes.get(err_nodes.length-1).splitText(match.index-(err_msg.length-err_nodes.get(err_nodes.length-1).length));
      replace_text_node.splitText(match[0].length);
      $(replace_text_node).replaceWith($('<a href="#">').text(match[0]));
    }
    err_container.prepend(
      contents,
      contents.length>0 ? '<br>' : undefined
    );
    $('#condition').addClass('is-error');
  }
  
  $('#condition').parent().on('click','.is-error+.form-input-hint a[href="#"]',function() {
    var pos=parseInt($(this).text().slice(1));
    var pos_end=parseInt($(this).text().slice(1+(''+pos).length+1));
    $('#condition').get(0).setSelectionRange(pos,!!pos_end ? pos_end : pos)
    $('#condition').get(0).focus();
    return false;
  });
  
  /* onSubmit logic and hnd
   */
  function requestAndLoadHits(history_func) {
    if (!history_func) {
      history_func=history.pushState;
    }
    
    $('#condition').removeClass('is-error').next('.form-input-hint').remove();
    
    $('form+.divider~*:not(script)').remove();
    
    var hits_defer_loading=$('form .btn-primary').prop('disabled',true).addClass('loading');
    var rq=mnApi.requestHits($('#in-schema-group').val(),$('#condition').val());
    rq.when.done(function(hits_arr) {
      if (hits_arr.length>0 && typeof(hits_arr[0])=='string') { // hits_arr contains parser errors
        for (var i=0; i<hits_arr.length; i++) {
          err_log(hits_arr[i]);
        }
      } else {
        history_func.call(history,{
          in_schema_group: $('#in-schema-group').val(),
          condition: $('#condition').val(),
          hits_key: rq.key
        },'','');
        
        mnLoadHits(hits_arr);
        
      }
      
      hits_defer_loading.prop('disabled',false).removeClass('loading');
      
    });
  }
  
  $('form .btn-primary').on('click',function() {
    requestAndLoadHits();
    return false;
  });
  
  $(window).on('popstate',function() {
    if (history.state) {
      
      $('#in-schema-group').val(history.state.in_schema_group).change();
      $('#condition').val(history.state.condition);
      
      var hits_arr=mnApi.fromCache(history.state.hits_key);
      if (hits_arr) {
        $('#condition').removeClass('is-error').next('.form-input-hint').remove();
        mnLoadHits(hits_arr);
        
      } else {
        schema_defer.done(function() {
          requestAndLoadHits(history.replaceState);
        });
        
      }
      
    } else {
      
      $('#condition').val('');
      
      mnLoadHits([]);
      
    }
  });
  setTimeout(function() {
    schema_tree_defer.done(function() {
      $(window).trigger('popstate');
    });
  },10);
  
  /* add property from dropdown list
   */
  $('.schema-props+button').on('click',function() {
    var cond_val=$('#condition').val();
    var c=$('#condition').get(0).selectionStart;
    $('#condition').val(cond_val.slice(0,c)+'prop::'+$('.schema-props').val()+cond_val.slice(c));
    return false;
  });
  
});
